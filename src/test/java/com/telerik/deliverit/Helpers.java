package com.telerik.deliverit;

import com.telerik.deliverit.models.*;
import com.telerik.deliverit.models.utils.UserRole;

import java.util.HashSet;
import java.util.Set;

public class Helpers {

    public static Country createCountry(){
        Country country = new Country();
        country.setId(1);
        country.setName("mockCountry");

        return country;
    }

    public static City createCity(){
        City city = new City();
        city.setId(1);
        city.setName("mockCity");
        city.setCountry(createCountry());

        return city;
    }

    public static Address createAddress(){
        Address address = new Address();
        address.setId(1);
        address.setStreetAddress("mockStreetName");
        address.setCity(createCity());

        return address;
    }

    public static Warehouse createWarehouse(){
        Warehouse warehouse = new Warehouse();
        warehouse.setId(1);
        warehouse.setAddress(createAddress());

        return warehouse;
    }

    public static User createUser(){
        User user = new User();
        user.setId(1);
        user.setUsername("mockUsername");
        user.setPassword("mockPassword");
        user.setUserRole(UserRole.CUSTOMER);

        return user;
    }

    public static Customer createCustomer(){
        Customer customer = new Customer();
        customer.setId(1);
        customer.setFirstName("mockFirstName");
        customer.setLastName("mockLastName");
        customer.setEmail("mockEmail");
        customer.setUser(createUser());
        customer.setAddress(createAddress());

        return customer;
    }

    public static Employee createEmployee(){
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("mockFirstName");
        employee.setLastName("mockLastName");
        employee.setEmail("mockEmail");
        employee.setUser(createUser());
        employee.getUser().setUserRole(UserRole.EMPLOYEE);

        return employee;
    }

    public static ParcelCategory createParcelCategory(){
        ParcelCategory parcelCategory = new ParcelCategory();
        parcelCategory.setId(1);
        parcelCategory.setCategory("mockCategory");

        return parcelCategory;
    }



    public static Parcel createParcel(){
        Parcel parcel = new Parcel();
        parcel.setId(1);
        parcel.setCategory(createParcelCategory());
        parcel.setCustomer(createCustomer());
        parcel.setWeight(0.1);
        parcel.setWarehouse(createWarehouse());
        parcel.setShipment(null);

        return parcel;
    }

    public static Shipment createShipment(){
        Shipment shipment = new Shipment();
        shipment.setId(1);
        shipment.setDepartureDate(null);
        shipment.setArrivalDate(null);
        shipment.setWarehouseTo(createWarehouse());
        Set<Parcel> parcelSet = new HashSet<>();
        Parcel parcel1 = createParcel();
        parcel1.setId(1);
        parcel1.setWeight(10);
        Parcel parcel2 = createParcel();
        parcel2.setId(2);
        parcel2.setWeight(25);
        parcelSet.add(parcel1);
        parcelSet.add(parcel2);
        shipment.setParcelSet(parcelSet);
        return shipment;
    }

}
