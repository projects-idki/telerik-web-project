package com.telerik.deliverit.services;

import com.telerik.deliverit.models.City;
import com.telerik.deliverit.models.Country;
import com.telerik.deliverit.repositories.contracts.CityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerik.deliverit.Helpers.createCity;
import static com.telerik.deliverit.Helpers.createCountry;

@ExtendWith(MockitoExtension.class)
public class CityServiceImplTests {

    @Mock
    CityRepository cityRepository;

    @InjectMocks
    CityServiceImpl cityService;


    @Test
    public void getAllCities_Should_Call_Repository(){

        cityService.getAllCities();

        Mockito.verify(cityRepository, Mockito.times(1)).getAllCities();
    }

    @Test
    public void getCityById_Should_Call_Repository(){

        cityService.getCityById(Mockito.anyInt());

        Mockito.verify(cityRepository, Mockito.times(1)).getCityById(Mockito.anyInt());
    }

    @Test
    public void getCityByName_Should_Call_Repository(){

        cityService.getCityByName(Mockito.anyString());

        Mockito.verify(cityRepository, Mockito.times(1)).getCityByName(Mockito.anyString());
    }



    @Test
    public void getOrCreate_Should_CreateCity_When_CityDontExist(){
        City mockCity = createCity();
        Country mockCountry = createCountry();

        Mockito.when(cityRepository.contains(mockCity)).thenReturn(false);

        cityService.getOrCreate(mockCity.getName(), mockCountry);

        Mockito.verify(cityRepository, Mockito.times(1)).createCity(mockCity);
    }


    @Test
    public void getOrCreate_Should_GetCity_When_CityExist(){
        City mockCity = createCity();
        Country mockCountry = createCountry();

        Mockito.when(cityRepository.contains(mockCity)).thenReturn(true);

        cityService.getOrCreate(mockCity.getName(), mockCountry);

        Mockito.verify(cityRepository, Mockito.times(1)).getCity(mockCity);
    }

    @Test
    public void getCountryCities_Should_Call_Repository(){

        cityService.getCountryCities(Mockito.anyInt());

        Mockito.verify(cityRepository, Mockito.times(1)).getCountryCities(Mockito.anyInt());
    }

}
