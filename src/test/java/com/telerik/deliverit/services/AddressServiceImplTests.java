package com.telerik.deliverit.services;

import com.telerik.deliverit.Helpers;
import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Address;
import com.telerik.deliverit.models.City;
import com.telerik.deliverit.models.Country;
import com.telerik.deliverit.repositories.contracts.AddressRepository;
import com.telerik.deliverit.services.contracts.CityService;
import com.telerik.deliverit.services.contracts.CountryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerik.deliverit.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AddressServiceImplTests {
    @Mock
    AddressRepository addressRepository;
    @Mock
    CountryService countryService;
    @Mock
    CityService cityService;

    @InjectMocks
    AddressServiceImpl addressService;

    @Test
    public void getAllAddresses_Should_CallRepository_When_Valid(){
        addressService.getAllAddresses();

        verify(addressRepository,times(1)).getAllAddresses();
    }

    @Test
    public void getAddressById_Should_CallRepository_WhenValid(){
        addressService.getAddressById(Mockito.anyInt());

        verify(addressRepository,times(1)).getAddressById(Mockito.anyInt());
    }

    @Test
    public void getAddressByStreetName_Should_CallRepository_WhenValid(){
        addressService.getAddressByStreetName(Mockito.anyString());

        verify(addressRepository,times(1)).getAddressByStreetName(Mockito.anyString());
    }

    @Test
    public void createAddress_Should_Throws_When_AddressExist(){
        when(countryService.getOrCreate("mockCountry")).thenReturn(createCountry());
        when(cityService.getOrCreate("mockCity",createCountry())).thenReturn(createCity());
        Address address = new Address(createCity(),"mockStreet");
        when(addressRepository.contains(address)).thenReturn(true);
        when(addressRepository.getAddress(address)).thenReturn(address);

        assertThrows(DuplicateEntityException.class,
                () -> addressService.createAddress("mockStreet","mockCity","mockCountry"));
    }

    @Test
    public void createAddress_Should_CallRepository_WhenValid(){
        when(countryService.getOrCreate("mockCountry")).thenReturn(createCountry());
        when(cityService.getOrCreate("mockCity",createCountry())).thenReturn(createCity());
        Address address = new Address(createCity(),"mockStreet");
        when(addressRepository.contains(address)).thenReturn(false);

        addressService.createAddress("mockStreet","mockCity","mockCountry");

        verify(addressRepository,times(1)).createAddress(address);

    }

    @Test
    public void getOrCreate_Should_getAddress_When_Exist(){
        when(countryService.getOrCreate("mockCountry")).thenReturn(createCountry());
        when(cityService.getOrCreate("mockCity",createCountry())).thenReturn(createCity());
        Address address = new Address(createCity(),"mockStreet");
        when(addressRepository.contains(address)).thenReturn(true);
        when(addressRepository.getAddress(address)).thenReturn(address);

        Assertions.assertEquals(address,addressService.getOrCreate("mockStreet","mockCity","mockCountry"));
    }

    @Test
    public void getOrCreate_Should_createAddress_When_NotExist(){
        when(countryService.getOrCreate("mockCountry")).thenReturn(createCountry());
        when(cityService.getOrCreate("mockCity",createCountry())).thenReturn(createCity());
        Address address = new Address(createCity(),"mockStreet");
        when(addressRepository.contains(address)).thenReturn(false);

        Assertions.assertNotEquals(address,addressService.getOrCreate("mockStreet","mockCity","mockCountry"));

    }

    @Test
    public void getOrCreate_Should_Throw_When_City_Country_notCorrespond(){
        City city = createCity();
        Country country = new Country("newMockCountry");
        city.setCountry(country);
        when(countryService.getCountryById(Mockito.anyInt())).thenReturn(createCountry());
        when(cityService.getCityById(Mockito.anyInt())).thenReturn(city);

        assertThrows(EntityNotFoundException.class,
                () -> addressService.getOrCreate("mockStreet",1,1));

    }

    @Test
    public void getOrCreate_Should_getAddress_When_Exist_2(){
        when(countryService.getCountryById(1)).thenReturn(createCountry());
        when(cityService.getCityById(1)).thenReturn(createCity());
        Address address = new Address(createCity(),"mockName");
        when(addressRepository.contains(address)).thenReturn(true);
        when(addressRepository.getAddress(address)).thenReturn(address);

        Assertions.assertEquals(address,addressService.getOrCreate("mockName",1,1));

    }

    @Test
    public void getOrCreate_Should_createAddress_When_NotExist_2(){
        when(countryService.getCountryById(1)).thenReturn(createCountry());
        when(cityService.getCityById(1)).thenReturn(createCity());
        Address address = new Address(createCity(),"mockName");
        when(addressRepository.contains(address)).thenReturn(false);

        Assertions.assertEquals(address,addressService.getOrCreate("mockName",1,1));

    }

}
