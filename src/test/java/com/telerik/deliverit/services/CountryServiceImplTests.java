package com.telerik.deliverit.services;

import com.telerik.deliverit.Helpers;
import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Country;
import com.telerik.deliverit.repositories.contracts.CountryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerik.deliverit.Helpers.createCountry;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    CountryServiceImpl countryService;

    @Test
    public void getAllCountries_Should_Call_Repository_When(){

        countryService.getAllCountries();

        verify(countryRepository,times(1)).getAllCountries();
    }
    @Test
    public void getCountryById_Should_Call_Repository_When(){

        countryService.getCountryById(Mockito.anyInt());

        verify(countryRepository,times(1)).getCountryById(Mockito.anyInt());
    }

    @Test
    public void getCountryByName_Should_Call_Repository_When(){

        countryService.getCountryByName(Mockito.anyString());

        verify(countryRepository,times(1)).getCountryByName(Mockito.anyString());
    }

    @Test
    public void createCountry_Should_ThrowException_When_CountryDuplicated(){
        Country country = createCountry();
        Country newCountry = createCountry();
        newCountry.setId(2);

        when(countryRepository.getCountryByName(newCountry.getName()))
                .thenReturn(country);

        assertThrows(DuplicateEntityException.class,
                () -> countryService.createCountry(newCountry.getName()));
    }

    @Test
    public void createCountry_Should_CallRepository_When_newCountry(){
        Country country = createCountry();
        Country newCountry = createCountry();
        newCountry.setId(2);

        when(countryRepository.getCountryByName(newCountry.getName()))
                .thenThrow(EntityNotFoundException.class);

        countryService.createCountry(newCountry.getName());

        verify(countryRepository,times(1)).createCountry(newCountry.getName());
    }

    @Test
    public void getOrCreate_Should_CreateNewCountry_IfNotExist(){

        when(countryRepository.getCountryByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        countryService.getOrCreate(Mockito.anyString());

        verify(countryRepository,times(1)).createCountry(Mockito.anyString());
    }

}
