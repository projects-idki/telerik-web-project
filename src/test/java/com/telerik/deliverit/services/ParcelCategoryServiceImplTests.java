package com.telerik.deliverit.services;

import com.telerik.deliverit.repositories.contracts.ParcelCategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class ParcelCategoryServiceImplTests {

    @Mock
    ParcelCategoryRepository parcelCategoryRepository;

    @InjectMocks
    ParcelCategoryServiceImpl parcelCategoryService;

    @Test
    public void getAllCategories_Should_Call_Repository_When() {

        parcelCategoryService.getAllCategories();

        Mockito.verify(parcelCategoryRepository, times(1)).getAllCategories();
    }

    @Test
    public void getParcelCategoryById_Should_Call_Repository_When() {

        parcelCategoryService.getParcelCategoryById(Mockito.anyInt());

        Mockito.verify(parcelCategoryRepository, times(1)).getParcelCategoryById(Mockito.anyInt());
    }

}
