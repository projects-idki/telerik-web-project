package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.UnauthorizedOperationException;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.utils.UserRole;
import com.telerik.deliverit.repositories.contracts.EmployeeRepository;
import com.telerik.deliverit.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerik.deliverit.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceImplTests {

    @Mock
    EmployeeRepository employeeRepository;

    @Mock
    UserService userService;

    @InjectMocks
    EmployeeServiceImpl employeeService;

    @Test
    public void getAllEmployees_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.getAllEmployees(mockCustomer.getUser()));
    }

    @Test
    public void getAllEmployees_Should_Call_Repository() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        employeeService.getAllEmployees(mockUser);

        Mockito.verify(employeeRepository, Mockito.times(1)).getAllEmployees();
    }


    @Test
    public void getEmployeeById_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.getEmployeeById(mockCustomer.getId(), mockCustomer.getUser()));
    }

    @Test
    public void getEmployeeById_Should_Call_Repository_With_User() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        employeeService.getEmployeeById(mockUser.getId(), mockUser);

        Mockito.verify(employeeRepository, Mockito.times(1)).getEmployeeById(mockUser.getId());
    }

    @Test
    public void getEmployeeById_Should_Call_Repository_Without_User() {

        employeeService.getEmployeeById(Mockito.anyInt());

        Mockito.verify(employeeRepository, Mockito.times(1)).getEmployeeById(Mockito.anyInt());
    }

    @Test
    public void createEmployee_Should_Throw_When_User_NotAdmin() {
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Employee mockEmployee = createEmployee();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.createEmployee(mockEmployee, mockCustomer.getUser()));
    }

    @Test
    public void createEmployee_Should_Throw_When_Email_AlreadyExists() {
        Employee mockEmployee = createEmployee();
        mockEmployee.getUser().setUserRole(UserRole.ADMIN);

        Mockito.when(employeeRepository.emailExists(mockEmployee.getEmail())).thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.createEmployee(mockEmployee, mockEmployee.getUser()));
    }

    @Test
    public void createEmployee_Should_Throw_When_Username_Exists() {
        Employee mockEmployee = createEmployee();
        mockEmployee.getUser().setUserRole(UserRole.ADMIN);

        Mockito.when(employeeRepository.emailExists(mockEmployee.getEmail())).thenReturn(false);
        Mockito.when(userService.isUsernameExist(mockEmployee.getUser().getUsername())).thenReturn(true);

        assertThrows(DuplicateEntityException.class,
                () -> employeeService.createEmployee(mockEmployee, mockEmployee.getUser()));
    }

    @Test
    public void createEmployee_Should_CallRepository_When_Valid() {
        Employee mockEmployee = createEmployee();
        mockEmployee.getUser().setUserRole(UserRole.ADMIN);

        Mockito.when(employeeRepository.emailExists(mockEmployee.getEmail())).thenReturn(false);
        Mockito.when(userService.isUsernameExist(mockEmployee.getUser().getUsername())).thenReturn(false);

        employeeService.createEmployee(mockEmployee, mockEmployee.getUser());

        verify(employeeRepository, times(1)).createEmployee(mockEmployee);
    }

    @Test
    public void updateEmployee_Should_Throw_When_User_NotCreatorOrAdmin() {
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);
        Employee mockEmployee = createEmployee();
        Employee anotherMockEmployee = createEmployee();
        anotherMockEmployee.setLastName("Gosho");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.updateEmployee(mockEmployee, anotherMockEmployee, anotherMockEmployee.getUser()));
    }

    @Test
    public void updateEmployee_Should_Throw_When_EmailExists() {
        Employee mockEmployee = createEmployee();
        Employee updateMockEmployee = createEmployee();
        updateMockEmployee.setEmail("DifferentEmail");
        updateMockEmployee.getUser().setUserRole(UserRole.ADMIN);

        Mockito.when(employeeRepository.emailExists(Mockito.anyString())).thenReturn(true);

        assertThrows(DuplicateEntityException.class,
                () -> employeeService.updateEmployee(updateMockEmployee, mockEmployee, updateMockEmployee.getUser()));
    }

    @Test
    public void updateEmployee_Should_Throw_When_UsernameExists() {
        Employee mockEmployee = createEmployee();
        Employee updateMockEmployee = createEmployee();
        updateMockEmployee.getUser().setUserRole(UserRole.ADMIN);
        updateMockEmployee.getUser().setUsername("DifferentUsername");

        Mockito.when(userService.isUsernameExist(Mockito.anyString())).thenReturn(true);

        assertThrows(DuplicateEntityException.class,
                () -> employeeService.updateEmployee(updateMockEmployee, mockEmployee, updateMockEmployee.getUser()));
    }

    @Test
    public void updateEmployee_CallRepository_When_Valid() {
        Employee mockEmployee = createEmployee();
        Employee updateMockEmployee = createEmployee();

        updateMockEmployee.getUser().setUserRole(UserRole.ADMIN);

        employeeService.updateEmployee(updateMockEmployee, mockEmployee, updateMockEmployee.getUser());

        Mockito.verify(employeeRepository, times(1)).updateEmployee(updateMockEmployee);
    }

    @Test
    public void deleteEmployee_Should_Throw_When_User_NotCreatorOrAdmin() {
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);
        Employee mockEmployee = createEmployee();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.deleteEmployee(mockEmployee.getId(), mockCustomer.getUser()));
    }

    @Test
    public void deleteEmployee_CallRepository_When_Valid() {
        Employee mockEmployee = createEmployee();

        mockEmployee.getUser().setUserRole(UserRole.ADMIN);

        employeeService.deleteEmployee(mockEmployee.getId(), mockEmployee.getUser());

        Mockito.verify(employeeRepository, times(1)).deleteEmployee(mockEmployee.getId());
    }
}
