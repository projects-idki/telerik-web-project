package com.telerik.deliverit.services;

import com.telerik.deliverit.Helpers;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerik.deliverit.Helpers.createUser;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void tryGetUser_Should_Call_Repository_When(){

        userService.tryGetUser(Mockito.anyString(), Mockito.anyString());

        Mockito.verify(userRepository,times(1)).tryGetUser(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void createUser_Should_Call_Repository_When(){
        User user = createUser();

        userService.createUser(user);

        Mockito.verify(userRepository,times(1)).createUser(user);
    }

    @Test
    public void updateUser_Should_Call_Repository_When(){
        User mockUser = createUser();
        User mockNewUser = createUser();

        userService.updateUser(mockUser, mockNewUser);

        Mockito.verify(userRepository,times(1)).updateUser(mockUser, mockNewUser);
    }

    @Test
    public void deleteUser_Should_Call_Repository_When(){
        User mockUser = createUser();

        userService.deleteUser(mockUser);

        Mockito.verify(userRepository,times(1)).deleteUser(mockUser);
    }

    @Test
    public void isUsernameExist_Should_Return(){
        User mockUser = createUser();
        User anotherMockUser = createUser();
        List<User> userList = new ArrayList<>();
        userList.add(mockUser);
        userList.add(anotherMockUser);

        Mockito.when(userRepository.getAllUsers()).thenReturn(userList);

        userService.isUsernameExist(mockUser.getUsername());
    }
}
