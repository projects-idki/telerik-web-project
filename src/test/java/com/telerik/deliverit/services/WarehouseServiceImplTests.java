package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.UnauthorizedOperationException;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.models.utils.UserRole;
import com.telerik.deliverit.repositories.contracts.WarehouseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerik.deliverit.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class WarehouseServiceImplTests {

    @Mock
    WarehouseRepository warehouseRepository;

    @InjectMocks
    WarehouseServiceImpl warehouseService;

    @Test
    public void getAll_Should_Call_Repository() {

        warehouseService.getAll();

        Mockito.verify(warehouseRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getWarehouseById_Should_Call_Repository() {

        warehouseService.getWarehouseById(Mockito.anyInt());

        Mockito.verify(warehouseRepository, Mockito.times(1)).getWarehouseById(Mockito.anyInt());
    }

    @Test
    public void createWarehouse_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Warehouse mockWarehouse = createWarehouse();
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> warehouseService.createWarehouse(mockWarehouse, mockCustomer.getUser()));
    }

    @Test
    public void createWarehouse_Should_CallRepository_When_newWarehouse() {
        Warehouse mockWarehouse = createWarehouse();
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);
        warehouseService.createWarehouse(mockWarehouse, mockUser);

        Mockito.verify(warehouseRepository, Mockito.times(1)).createWarehouse(mockWarehouse);
    }

    @Test
    public void updateWarehouse_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Warehouse mockWarehouse = createWarehouse();
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> warehouseService.updateWarehouse(mockWarehouse, mockCustomer.getUser()));
    }

    @Test
    public void updateWarehouse_Should_CallRepository() {
        Warehouse mockWarehouse = createWarehouse();
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);
        warehouseService.updateWarehouse(mockWarehouse, mockUser);

        Mockito.verify(warehouseRepository, Mockito.times(1)).updateWarehouse(mockWarehouse);
    }


    @Test
    public void deleteWarehouse_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Warehouse mockWarehouse = createWarehouse();
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> warehouseService.deleteWarehouse(mockWarehouse.getId(), mockCustomer.getUser()));
    }

    @Test
    public void deleteWarehouse_Should_CallRepository() {
        Warehouse mockWarehouse = createWarehouse();
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);
        warehouseService.deleteWarehouse(mockWarehouse.getId(), mockUser);

        Mockito.verify(warehouseRepository, Mockito.times(1)).deleteWarehouse(mockWarehouse.getId());
    }


}
