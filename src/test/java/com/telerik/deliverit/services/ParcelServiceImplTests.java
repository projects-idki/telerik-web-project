package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.exceptions.UnauthorizedOperationException;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.repositories.contracts.ParcelRepository;
import com.telerik.deliverit.services.contracts.ParcelCategoryService;
import com.telerik.deliverit.services.contracts.WarehouseService;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.telerik.deliverit.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ParcelServiceImplTests {

    @Mock
    ParcelRepository parcelRepository;
    @Mock
    CustomerServiceImpl customerService;
    @Mock
    WarehouseService warehouseService;
    @Mock
    ParcelCategoryService parcelCategoryService;
    @Mock
    ValidationHelper validationHelper;

    @InjectMocks
    ParcelServiceImpl parcelService;


    @Test
    public void getCustomerParcels_Should_Throw_When_Customer_NotAuthorised() {

        when(validationHelper.isCustomerAuthorised(createUser(), createCustomer())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.getCustomerParcels(createCustomer(), createUser()));
    }

    @Test
    public void getCustomerParcels_Should_Call_Repository_When_Valid() {
        when(validationHelper.isCustomerAuthorised(createUser(), createCustomer())).thenReturn(true);

        parcelService.getCustomerParcels(createCustomer(), createUser());

        verify(parcelRepository, times(1)).getCustomerParcels(Mockito.anyInt());
    }

    @Test
    public void getAllParcels_Should_Throw_When_Customer_NotAuthorised() {

        when(validationHelper.isUserEmployee(createUser())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.getAllParcels(createUser()));
    }

    @Test
    public void getAllParcels_Should_CallRepository_When_Valid(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(true);

        parcelService.getAllParcels(createUser());

        verify(parcelRepository,times(1)).getAllParcels();
    }

    @Test
    public void createParcel_Should_Throw_When_UserUnauthorised(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.createParcel(createParcel(),createUser()));
    }

    @Test
    public void createParcel_Should_CallRepository_When_Valid(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(true);

        parcelService.createParcel(createParcel(),createUser());

        verify(parcelRepository,times(1)).createParcel(createParcel());
    }

    @Test
    public void getParcelById_should_CallRepository(){
        parcelService.getParcelById(Mockito.anyInt());

        verify(parcelRepository,times(1)).getParcelById(Mockito.anyInt());
    }

    @Test
    public void getParcelById_Should_Throw_When_UserUnouthorised(){
        User user = createUser();
        when(validationHelper.isUserEmployee(createUser())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.getParcelById(user.getId(),user));
    }

    @Test
    public void getParcelById_Should_CallRepository_WhenValid2(){
        User user = createUser();
        when(validationHelper.isUserEmployee(user)).thenReturn(true);

        parcelService.getParcelById(user.getId(),user);

        verify(parcelRepository,times(1)).getParcelById(user.getId());
    }

    @Test
    public void updateParcel_Should_Throw_When_UserInvalid(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.updateParcel(createParcel(),createUser()));
    }

    @Test
    public void updateParcel_Should_CallRepository_When_Valid(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(true);

        parcelService.updateParcel(createParcel(),createUser());

        verify(parcelRepository,times(1)).updateParcel(createParcel());
    }

    @Test
    public void deleteParcel_Should_Throw_When_UserInvalid(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.deleteParcel(createParcel(),createUser()));
    }

    @Test
    public void deleteParcel_Should_CallRepository_When_Valid(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(true);

        parcelService.deleteParcel(createParcel(),createUser());

        verify(parcelRepository,times(1)).deleteParcel(createParcel());
    }

    @Test
    public void getParcelsWithParameters_Should_Throw_When_UserInvalid(){
        User user = createUser();
        Optional<Double> doubleOptional = Optional.of(1.1);
        Optional<Integer> integerOptional = Optional.of(1);
        when(validationHelper.isUserEmployee(user)).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                ()-> parcelService.getParcelsWithParameters(doubleOptional
                        ,doubleOptional
                        ,integerOptional
                        ,integerOptional
                        ,integerOptional
                        ,user));
    }

    @Test
    public void getParcelsWithParameters_Should_Throw_When_minWeight_Above100(){
        User user = createUser();
        Optional<Double> doubleOptional = Optional.of(1.1);
        Optional<Integer> integerOptional = Optional.of(1);
        when(validationHelper.isUserEmployee(user)).thenReturn(true);

        assertThrows(IllegalArgumentException.class,
                ()-> parcelService.getParcelsWithParameters(Optional.of(101.0)
                        ,doubleOptional
                        ,integerOptional
                        ,integerOptional
                        ,integerOptional
                        ,user));
    }

    @Test
    public void getParcelsWithParameters_Should_Throw_When_maxWeight_Below0(){
        User user = createUser();
        Optional<Double> doubleOptional = Optional.of(1.1);
        Optional<Integer> integerOptional = Optional.of(1);
        when(validationHelper.isUserEmployee(user)).thenReturn(true);

        assertThrows(IllegalArgumentException.class,
                ()-> parcelService.getParcelsWithParameters(doubleOptional
                        ,Optional.of(-1.0)
                        ,integerOptional
                        ,integerOptional
                        ,integerOptional
                        ,user));
    }

    @Test
    public void getParcelsWithParameters_Should_Throw_When_CustomerNotExist(){
        User user = createUser();
        Optional<Double> doubleOptional = Optional.of(1.1);
        Optional<Integer> integerOptional = Optional.of(1);
        when(validationHelper.isUserEmployee(user)).thenReturn(true);
        when(customerService.getCustomerById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                ()-> parcelService.getParcelsWithParameters(doubleOptional
                        ,doubleOptional
                        ,integerOptional
                        ,integerOptional
                        ,integerOptional
                        ,user));
    }

    @Test
    public void getParcelsWithParameters_Should_Throw_When_WarehouseNotExist(){
        User user = createUser();
        Optional<Double> doubleOptional = Optional.of(1.1);
        Optional<Integer> integerOptional = Optional.of(1);
        when(validationHelper.isUserEmployee(user)).thenReturn(true);
        when(warehouseService.getWarehouseById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                ()-> parcelService.getParcelsWithParameters(doubleOptional
                        ,doubleOptional
                        ,integerOptional
                        ,integerOptional
                        ,integerOptional
                        ,user));
    }

    @Test
    public void getParcelsWithParameters_Should_Throw_When_CategoryNotExist(){
        User user = createUser();
        Optional<Double> doubleOptional = Optional.of(1.1);
        Optional<Integer> integerOptional = Optional.of(1);
        when(validationHelper.isUserEmployee(user)).thenReturn(true);
        when(parcelCategoryService.getParcelCategoryById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                ()-> parcelService.getParcelsWithParameters(doubleOptional
                        ,doubleOptional
                        ,integerOptional
                        ,integerOptional
                        ,integerOptional
                        ,user));
    }

    @Test
    public void getParcelsWithParameters_Should_CallRepository_When_Valid(){
        User user = createUser();
        Optional<Double> doubleOptional = Optional.of(1.1);
        Optional<Integer> integerOptional = Optional.of(1);
        when(validationHelper.isUserEmployee(user)).thenReturn(true);

        parcelService.getParcelsWithParameters(doubleOptional,doubleOptional,integerOptional,integerOptional,integerOptional,user);

        verify(parcelRepository,times(1)).getParcelsWithparameters(doubleOptional,doubleOptional,integerOptional,integerOptional,integerOptional);
    }

    @Test
    public void sortParcels_ShouldThrow_When_UserInvalid(){
        User user = createUser();
        Optional<Boolean> booleanOptional = Optional.of(true);
        when(validationHelper.isUserEmployee(user)).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.sortParcels(booleanOptional,booleanOptional,user));
    }

    @Test
    public void sortParcels_Should_CallRepository_When_Valid(){
        User user = createUser();
        Optional<Boolean> booleanOptional = Optional.of(true);
        when(validationHelper.isUserEmployee(user)).thenReturn(true);

        parcelService.sortParcels(booleanOptional,booleanOptional,user);

        verify(parcelRepository,times(1)).sortParcels(booleanOptional,booleanOptional);
    }
}
