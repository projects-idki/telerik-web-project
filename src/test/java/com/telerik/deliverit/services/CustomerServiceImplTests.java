package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.exceptions.UnauthorizedOperationException;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.utils.UserRole;
import com.telerik.deliverit.repositories.contracts.CustomerRepository;
import com.telerik.deliverit.repositories.contracts.ParcelRepository;
import com.telerik.deliverit.services.contracts.UserService;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerik.deliverit.Helpers.createCustomer;
import static com.telerik.deliverit.Helpers.createUser;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTests {

    @Mock
    CustomerRepository customerRepository;
    @Mock
    UserService userService;
    @Mock
    ValidationHelper validationHelper;
    @Mock
    ParcelRepository parcelRepository;

    @InjectMocks
    CustomerServiceImpl customerService;

    @Test
    public void getAllCustomers_Should_Throws_When_User_NotEmployee() {
        Customer customer = createCustomer();
        customer.getUser().setUserRole(UserRole.CUSTOMER);

        when(validationHelper.isUserEmployee(customer.getUser())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> customerService.getAllCustomers(customer.getUser()));
    }

    @Test
    public void getAllCustomers_Should_CallRepository_When_Valid() {

        when(validationHelper.isUserEmployee(createUser())).thenReturn(true);

        customerService.getAllCustomers(createUser());

        verify(customerRepository, times(1)).getAllCustomers();
    }

    @Test
    public void getCustomerById_Should_Throws_When_User_NotEmployee() {
        Customer customer = createCustomer();
        customer.getUser().setUserRole(UserRole.CUSTOMER);

        when(validationHelper.isUserEmployee(customer.getUser())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> customerService.getCustomerById(Mockito.anyInt(), customer.getUser()));
    }

    @Test
    public void getCustomerById_Should_CallRepository_When_Valid() {

        when(validationHelper.isUserEmployee(createUser())).thenReturn(true);

        customerService.getCustomerById(Mockito.anyInt(), createUser());

        verify(customerRepository, times(1)).getCustomerById(Mockito.anyInt());
    }

    @Test
    public void getCustomerById_noUser_Should_CallRepository_When_Valid() {

        customerService.getCustomerById(Mockito.anyInt());

        verify(customerRepository, times(1)).getCustomerById(Mockito.anyInt());
    }

    @Test
    public void createCustomer_Should_Throw_When_Email_Exists() {
        Customer customer = createCustomer();

        when(customerRepository.isEmailExist(customer.getEmail())).thenReturn(true);

        assertThrows(DuplicateEntityException.class,
                () -> customerService.createCustomer(customer));
    }

    @Test
    public void createCustomer_Should_Throw_When_Username_Exists() {
        Customer customer = createCustomer();

        when(userService.isUsernameExist(customer.getUser().getUsername())).thenReturn(true);

        assertThrows(DuplicateEntityException.class,
                () -> customerService.createCustomer(customer));
    }

    @Test
    public void createCustomer_Should_CallRepository_When_Valid() {

        when(customerRepository.isEmailExist(Mockito.anyString())).thenReturn(false);
        when(userService.isUsernameExist(Mockito.anyString())).thenReturn(false);

        customerService.createCustomer(createCustomer());

        verify(customerRepository, times(1)).saveCustomer(createCustomer());
    }

    @Test
    public void deleteCustomerById_Should_Throw_When_User_NotAuthorised() {
        Customer customer = createCustomer();
        Customer unauthorisedCustomer = createCustomer();

        when(customerService.getCustomerById(1)).thenReturn(customer);
        when(validationHelper.isCustomerAuthorised(unauthorisedCustomer.getUser(), customerService.getCustomerById(1)))
                .thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> customerService.deleteCustomerById(1, unauthorisedCustomer.getUser()));
    }

    @Test
    public void deleteCustomerById_Should_Throw_When_Customer_NotExist() {

        when(customerService.getCustomerById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> customerService.deleteCustomerById(Mockito.anyInt(), createUser()));
    }

    @Test
    public void deleteCustomer_Should_CallRepository_When_Valid() {
        Customer customer = createCustomer();

        when(parcelRepository.getCustomerParcels(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);
        when(customerService.getCustomerById(customer.getId())).thenReturn(customer);
        when(validationHelper.isCustomerAuthorised(customer.getUser(), customer)).thenReturn(true);

        customerService.deleteCustomerById(customer.getId(), customer.getUser());

        verify(customerRepository, times(1)).deleteCustomer(customer);
    }

    @Test
    public void updateCustomerById_Should_Throw_When_User_NotAuthorised() {
        Customer customer = createCustomer();
        Customer unauthorisedCustomer = createCustomer();

        when(customerService.getCustomerById(1)).thenReturn(customer);
        when(validationHelper.isCustomerAuthorised(unauthorisedCustomer.getUser(), customerService.getCustomerById(1)))
                .thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> customerService.updateCustomer(createCustomer(), customer, unauthorisedCustomer.getUser()));
    }

    @Test
    public void updateCustomer_Should_Throw_When_EmailExists() {
        Customer customer = createCustomer();
        Customer updateCustomer = createCustomer();
        updateCustomer.setEmail("DifferentEmail");
        when(validationHelper.isCustomerAuthorised(createUser(), createCustomer())).thenReturn(true);
        when(customerRepository.isEmailExist(Mockito.anyString())).thenReturn(true);

        assertThrows(DuplicateEntityException.class,
                () -> customerService.updateCustomer(updateCustomer,customer,createUser()));
    }

    @Test
    public void updateCustomer_Should_Throw_When_UsernameExists() {
        Customer customer = createCustomer();
        Customer updateCustomer = createCustomer();
        updateCustomer.getUser().setUsername("differentUsername");
        when(validationHelper.isCustomerAuthorised(createUser(), createCustomer())).thenReturn(true);
        when(userService.isUsernameExist(Mockito.anyString())).thenReturn(true);

        assertThrows(DuplicateEntityException.class,
                () -> customerService.updateCustomer(updateCustomer,customer,createUser()));
    }

    @Test
    public void updateCustomer_Should_Call_Repository_When_Valid(){
        when(validationHelper.isCustomerAuthorised(createUser(), createCustomer())).thenReturn(true);

        customerService.updateCustomer(createCustomer(),createCustomer(),createUser());

        verify(customerRepository,times(1)).updateCustomer(createCustomer());
    }

    @Test
    public void findByKeyword_Should_Throw_When_InvalidUser(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> customerService.findByKeyword(Mockito.anyString(),createUser()));
    }

    @Test
    public void findByKeyword_Should_Call_Repository_When_Valid(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(true);

        customerService.findByKeyword(Mockito.anyString(),createUser());

        verify(customerRepository,times(1)).findByKeyword(Mockito.anyString());
    }

//    @Test
//    public void findCustomerWithParameters_Should_Throw_When_UserUnauthorised(){
//        when(authenticationHelper.isUserEmployee(createUser())).thenReturn(false);
//
//        assertThrows(UnauthorizedOperationException.class,
//                () -> customerService.findCustomerWithParameters(Mockito.anyString(),
//                        Mockito.anyString(),
//                        Mockito.anyString(),
//                        createUser()));
//    }
    @Test
    public void findCustomerWithParameters_Should_Throw_When_UserUnauthorised(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(false);

        assertThrows(UnauthorizedOperationException.class,
                () -> customerService.findCustomerWithParameters("firstname",
                        "lastname",
                        "email",
                        createUser()));
    }

    @Test
    public void findCustomerWithParameters_Should_Call_Repository_When_Valid(){
        when(validationHelper.isUserEmployee(createUser())).thenReturn(true);

        customerService.findCustomerWithParameters("firstname",
                "lastname",
                "email",
                createUser());

        verify(customerRepository,times(1)).findCustomersWithParameters(Mockito.anyString(),
                Mockito.anyString(),Mockito.anyString());
    }

    @Test
    public void getCustomerAmount_Should_CallRepository_WhenValid(){

        customerService.getCustomerAmount();

        verify(customerRepository,times(1)).getAllCustomers();
    }


}
