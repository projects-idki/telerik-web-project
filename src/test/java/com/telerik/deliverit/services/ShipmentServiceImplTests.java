package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.*;
import com.telerik.deliverit.models.*;
import com.telerik.deliverit.models.utils.UserRole;
import com.telerik.deliverit.repositories.contracts.ShipmentRepository;
import com.telerik.deliverit.services.contracts.CustomerService;
import com.telerik.deliverit.services.contracts.ParcelService;
import com.telerik.deliverit.services.contracts.WarehouseService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import static com.telerik.deliverit.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ShipmentServiceImplTests {

    @Mock
    ShipmentRepository shipmentRepository;

    @Mock
    CustomerService customerService;

    @Mock
    WarehouseService warehouseService;

    @Mock
    ParcelService parcelService;

    @InjectMocks
    ShipmentServiceImpl shipmentService;


    @Test
    public void getAllShipments_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> shipmentService.getAllShipments(mockCustomer.getUser()));
    }

    @Test
    public void getAllShipments_Should_Call_Repository() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        shipmentService.getAllShipments(mockUser);

        Mockito.verify(shipmentRepository, Mockito.times(1)).getAllShipments();
    }


    @Test
    public void getShipmentById_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Shipment mockShipment = createShipment();
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> shipmentService.getShipmentById(mockShipment.getId(), mockCustomer.getUser()));
    }

    @Test
    public void getShipmentById_Should_Call_Repository_With_User() {
        Shipment mockShipment = createShipment();
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        shipmentService.getShipmentById(mockShipment.getId(), mockUser);

        Mockito.verify(shipmentRepository, Mockito.times(1)).getShipmentById(mockShipment.getId());
    }

    @Test
    public void getShipmentById_Should_Call_Repository_Without_User() {

        shipmentService.getShipmentById(Mockito.anyInt());

        Mockito.verify(shipmentRepository, Mockito.times(1)).getShipmentById(Mockito.anyInt());
    }


    @Test
    public void createShipment_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Shipment mockShipment = createShipment();
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> shipmentService.createShipment(mockShipment, mockCustomer.getUser()));
    }

    @Test
    public void createShipment_Should_CallRepository_When_newShipment() {
        Shipment mockShipment = createShipment();
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        shipmentService.createShipment(mockShipment, mockUser);

        Mockito.verify(shipmentRepository, Mockito.times(1)).createShipment(mockShipment);
    }


    @Test
    public void updateShipment_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Shipment mockShipment = createShipment();
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> shipmentService.updateShipment(mockShipment, mockCustomer.getUser()));
    }

    @Test
    public void updateShipment_Should_Throws_When_ArrivalDate_IsBefore_DepartureDate() {
        Shipment mockShipment = createShipment();
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        mockShipment.setDepartureDate(Date.valueOf("2021-03-02"));
        mockShipment.setArrivalDate(Date.valueOf("2021-03-01"));


        Assertions.assertThrows(DateTimeException.class,
                () -> shipmentService.updateShipment(mockShipment, mockUser));
    }

    @Test
    public void updateShipment_Should_CallRepository_When_newShipment() {
        Shipment mockShipment = createShipment();
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        mockShipment.setDepartureDate(Date.valueOf("2021-03-02"));
        mockShipment.setArrivalDate(Date.valueOf("2021-03-05"));

        shipmentService.updateShipment(mockShipment, mockUser);

        Mockito.verify(shipmentRepository, Mockito.times(1)).updateShipment(mockShipment);
    }


    @Test
    public void deleteShipment_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Shipment mockShipment = createShipment();
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> shipmentService.deleteShipment(mockShipment.getId(), mockCustomer.getUser()));
    }

    @Test
    public void deleteShipment_Should_CallRepository() {
        Shipment mockShipment = createShipment();
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        shipmentService.deleteShipment(mockShipment.getId(), mockUser);

        Mockito.verify(shipmentRepository, Mockito.times(1)).deleteShipment(mockShipment.getId());
    }


    @Test
    public void getAllShipmentsByCustomer_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> shipmentService.getAllShipmentsByCustomer(mockCustomer.getId(), mockCustomer.getUser()));
    }

    @Test
    public void getAllShipmentsByCustomer_Should_CallRepository() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        customerService.getCustomerById(mockCustomer.getId());

        shipmentService.getAllShipmentsByCustomer(mockCustomer.getId(), mockUser);

        Mockito.verify(shipmentRepository, Mockito.times(1)).getAllShipmentsByCustomer(mockCustomer.getId());
    }


    @Test
    public void getAllShipmentsByWarehouse_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> shipmentService.getAllShipmentsByWarehouse(mockCustomer.getId(), mockCustomer.getUser()));
    }

    @Test
    public void getAllShipmentsByWarehouse_Should_CallRepository() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Warehouse mockWarehouse = createWarehouse();

        warehouseService.getWarehouseById(mockWarehouse.getId());

        shipmentService.getAllShipmentsByWarehouse(mockWarehouse.getId(), mockUser);

        Mockito.verify(shipmentRepository, Mockito.times(1)).getAllShipmentsByWarehouse(mockWarehouse.getId());
    }

    @Test
    public void addParcelToShipment_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Shipment mockShipment = createShipment();
        Parcel mockParcel = createParcel();
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> shipmentService.addParcelToShipment(mockShipment, mockParcel, mockCustomer.getUser()));
    }

    @Test
    public void addParcelToShipment_Should_Throws_When_Parcel_AlreadyInTheShipment() {
        Shipment mockShipment = createShipment();

        Parcel mockParcel = createParcel();
        mockParcel.setId(1);

        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> shipmentService.addParcelToShipment(mockShipment, mockParcel, mockUser));
    }

    @Test
    public void addParcelToShipment_Should_Throws_When_Parcel_AlreadyInOtherShipment() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Shipment mockShipment = createShipment();

        Shipment anotherMockShipment = createShipment();
        Set<Parcel> newParcelSet = new HashSet<>();

        Parcel mockParcel = createParcel();
        mockParcel.setId(4);
        mockParcel.setShipment(anotherMockShipment);

        anotherMockShipment.setParcelSet(newParcelSet);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> shipmentService.addParcelToShipment(mockShipment, mockParcel, mockUser));
    }


    @Test
    public void addParcelToShipment_Should_Throws_When_Parcel_IsHeadingToAnotherWarehouse() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Shipment mockShipment = createShipment();
        Warehouse mockWarehouse = createWarehouse();
        mockWarehouse.setId(1);
        mockShipment.setWarehouseTo(mockWarehouse);

        Parcel mockParcel = createParcel();
        mockParcel.setId(3);
        Warehouse anotherMockWarehouse = createWarehouse();
        anotherMockWarehouse.setId(2);
        Address mockAddress = createAddress();
        mockAddress.setStreetAddress("MockStreet2");
        anotherMockWarehouse.setAddress(mockAddress);
        mockParcel.setWarehouse(anotherMockWarehouse);

        Mockito.when(parcelService.getParcelById(mockParcel.getId()))
                .thenReturn(mockParcel);

        Assertions.assertThrows(WrongWarehouseException.class,
                () -> shipmentService.addParcelToShipment(mockShipment, mockParcel, mockUser));
    }

    @Test
    public void addParcelToShipment_Should_CallRepository() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Shipment mockShipment = createShipment();

        Parcel mockParcel = createParcel();
        Warehouse mockWarehouse = createWarehouse();
        mockWarehouse.setId(1);
        mockParcel.setId(10);
        mockParcel.setWarehouse(mockWarehouse);
        mockShipment.setWarehouseTo(mockWarehouse);

        Mockito.when(parcelService.getParcelById(mockParcel.getId()))
                .thenReturn(mockParcel);

        shipmentService.addParcelToShipment(mockShipment, mockParcel, mockUser);

        Mockito.verify(shipmentRepository, Mockito.times(1)).updateShipment(mockShipment);

    }

    @Test
    public void removeParcelFromShipment_Should_Throws_When_User_NotEmployeeOrAdmin() {
        Shipment mockShipment = createShipment();
        Parcel mockParcel = createParcel();
        Customer mockCustomer = createCustomer();
        mockCustomer.getUser().setUserRole(UserRole.CUSTOMER);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> shipmentService.removeParcelFromShipment(mockShipment, mockParcel, mockCustomer.getUser()));
    }

    @Test
    public void removeParcelFromShipment_Should_Throws_When_Parcel_IsNotInTheShipment() {
        Shipment mockShipment = createShipment();

        Parcel mockParcel = createParcel();
        mockParcel.setId(6);
        mockParcel.setWeight(3);

        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> shipmentService.removeParcelFromShipment(mockShipment, mockParcel, mockUser));
    }


    @Test
    public void removeParcelFromShipment_Should_CallRepository() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Shipment mockShipment = createShipment();

        Set<Parcel> mockParcelSet = mockShipment.getParcelSet();

        Parcel mockParcel = createParcel();
        Warehouse mockWarehouse = createWarehouse();
        mockWarehouse.setId(1);
        mockParcel.setId(10);
        mockParcel.setWarehouse(mockWarehouse);

        mockShipment.setWarehouseTo(mockWarehouse);

        mockParcelSet.add(mockParcel);
        mockShipment.setParcelSet(mockParcelSet);

        shipmentService.removeParcelFromShipment(mockShipment, mockParcel, mockUser);

        Mockito.verify(shipmentRepository, Mockito.times(1)).removeParcelFromShipment(mockShipment,mockParcel);
    }

    @Test
    public void parcelIdSetToParcel_Should_Throws_When_Parcel_AlreadyInTheShipment() {
        Shipment mockShipment = createShipment();
        Set<Integer> mockParcelIdSet = new HashSet<>();

        Parcel mockParcel = createParcel();
        mockParcel.setId(1);

        mockParcelIdSet.add(1);

        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Mockito.when(parcelService.getParcelById(mockParcel.getId()))
                .thenReturn(mockParcel);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> shipmentService.parcelIdSetToParcel(mockParcelIdSet, mockShipment));
    }


    @Test
    public void parcelIdSetToParcel_Should_Throws_When_Parcel_AlreadyInOtherShipment() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Shipment mockShipment = createShipment();
        Shipment anotherMockShipment = createShipment();
        anotherMockShipment.setId(3);
        Set<Integer> mockParcelIdSet = new HashSet<>();

        Parcel mockParcel = createParcel();
        mockParcel.setId(4);
        mockParcel.setShipment(anotherMockShipment);

        mockParcelIdSet.add(4);

        Mockito.when(parcelService.getParcelById(4))
                .thenReturn(mockParcel);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> shipmentService.parcelIdSetToParcel(mockParcelIdSet, mockShipment));
    }


    @Test
    public void parcelIdSetToParcel_Should_Throws_When_Parcel_WrongWarehouse() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Shipment mockShipment = createShipment();
        Warehouse anotherMockWarehouse = createWarehouse();
        anotherMockWarehouse.getAddress().setStreetAddress("TestStreet23");
        mockShipment.setWarehouseTo(anotherMockWarehouse);

        Warehouse mockWarehouse = createWarehouse();
        mockWarehouse.setId(3);

        Set<Integer> mockParcelIdSet = new HashSet<>();

        Parcel mockParcel = createParcel();
        mockParcel.setId(4);
        mockParcel.setWarehouse(mockWarehouse);

        mockParcelIdSet.add(4);

        Mockito.when(parcelService.getParcelById(4))
                .thenReturn(mockParcel);

        Assertions.assertThrows(WrongWarehouseException.class,
                () -> shipmentService.parcelIdSetToParcel(mockParcelIdSet, mockShipment));
    }


    @Test
    public void parcelIdSetToParcel_Should_SetParcelSet() {
        User mockUser = createUser();
        mockUser.setUserRole(UserRole.EMPLOYEE);

        Shipment mockShipment = createShipment();

        Warehouse mockWarehouse = createWarehouse();
        mockWarehouse.setId(3);
        mockShipment.setWarehouseTo(mockWarehouse);

        Set<Integer> mockParcelIdSet = new HashSet<>();

        Parcel mockParcel = createParcel();
        mockParcel.setId(4);
        mockParcel.setWarehouse(mockWarehouse);

        mockParcelIdSet.add(4);

        Mockito.when(parcelService.getParcelById(4))
                .thenReturn(mockParcel);

        shipmentService.parcelIdSetToParcel(mockParcelIdSet, mockShipment);

    }
}
