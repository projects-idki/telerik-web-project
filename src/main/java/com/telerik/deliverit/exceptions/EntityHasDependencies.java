package com.telerik.deliverit.exceptions;

public class EntityHasDependencies extends RuntimeException{

    public EntityHasDependencies(String message){
        super(message);
    }
}
