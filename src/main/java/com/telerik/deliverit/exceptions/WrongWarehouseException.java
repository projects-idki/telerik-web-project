package com.telerik.deliverit.exceptions;

public class WrongWarehouseException extends RuntimeException{
    public WrongWarehouseException() {
        super("Wrong warehouse");
    }
}
