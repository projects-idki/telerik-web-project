package com.telerik.deliverit.exceptions;

public class DateTimeException extends RuntimeException{

    public DateTimeException(){
        super("Arrival date cannot be set before departure date.");
    }
}
