package com.telerik.deliverit.exceptions;

public class WrongPasswordException extends RuntimeException {

    public WrongPasswordException(){
        super("Wrong Username Password combination");
    }
}
