package com.telerik.deliverit.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
@Component
public class ExceptionHandler {

    public void handleException(Exception e) {
        switch (e.getClass().getSimpleName()) {
            case "DuplicateEntityException":
                throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
            case "EntityNotFoundException":
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            case "WrongPasswordException":
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
            case "UnauthorizedOperationException":
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
            case "IllegalArgumentException":
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
            case "DateTimeException":
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
            case "WrongWarehouseException":
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
            case "EntityHasDependencies":
                throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY,e.getMessage());
//            default:
//                throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT,"You should not see this, contact support");
        }

    }
}


