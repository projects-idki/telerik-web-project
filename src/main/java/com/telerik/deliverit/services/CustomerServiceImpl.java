package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityHasDependencies;
import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.exceptions.UnauthorizedOperationException;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.repositories.contracts.CustomerRepository;
import com.telerik.deliverit.repositories.contracts.ParcelRepository;
import com.telerik.deliverit.services.contracts.CustomerService;
import com.telerik.deliverit.services.contracts.UserService;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final UserService userService;
    private final ValidationHelper validationHelper;
    private final ParcelRepository parcelRepository;


    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, UserService userService, ValidationHelper validationHelper, ParcelRepository parcelRepository) {
        this.customerRepository = customerRepository;
        this.userService = userService;

        this.validationHelper = validationHelper;
        this.parcelRepository = parcelRepository;
    }

    @Override
    public List<Customer> getAllCustomers(User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User not authorised");
        }
        return customerRepository.getAllCustomers();
    }

    @Override
    public Customer getCustomerById(int id, User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User not authorised");
        }
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer getCustomerById(int id) {
        return customerRepository.getCustomerById(id);

    }

    @Override
    public Customer createCustomer(Customer customer) {
        if (isEmailExist(customer.getEmail())) {
            throw new DuplicateEntityException("Customer", "email", customer.getEmail());
        }
        if (userService.isUsernameExist(customer.getUser().getUsername())) {
            throw new DuplicateEntityException("User", "username", customer.getUser().getUsername());
        }
        customerRepository.saveCustomer(customer);
        return customer;

    }

    @Override
    public Customer deleteCustomerById(int id, User user) {
        if (!validationHelper.isCustomerAuthorised(user, getCustomerById(id))) {
            throw new UnauthorizedOperationException("User not authorised");
        }
        Customer customerToDelete = getCustomerById(id);

        if (parcelRepository.getCustomerParcels(id).isEmpty()) {
            customerRepository.deleteCustomer(customerToDelete);
            return customerToDelete;
        }

        throw new EntityHasDependencies("Customer has existing parcels");
    }


    @Override
    public Customer updateCustomer(Customer updateCustomer, Customer customer, User user) {
        if (!validationHelper.isCustomerAuthorised(user, customer)) {
            throw new UnauthorizedOperationException("User not authorised");
        }
        if (!updateCustomer.getEmail().equals(customer.getEmail()) && isEmailExist(updateCustomer.getEmail())) {
            throw new DuplicateEntityException("Customer", "email", updateCustomer.getEmail());
        }
        if (!updateCustomer.getUser().getUsername().equals(customer.getUser().getUsername()) && isUsernameExist(updateCustomer.getUser().getUsername())) {
            throw new DuplicateEntityException("User", "username", updateCustomer.getUser().getUsername());
        }
        return customerRepository.updateCustomer(updateCustomer);
    }

    @Override
    public List<Customer> findByKeyword(String keyword, User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User not authorised");
        }
        return customerRepository.findByKeyword(keyword);
    }

    @Override
    public int getCustomerAmount() {
        return customerRepository.getAllCustomers().size();
    }

    @Override
    public List<Customer> findCustomerWithParameters(String first_name, String last_name, String email, User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User not authorised");
        }
        return customerRepository.findCustomersWithParameters(first_name, last_name, email);
    }

    private boolean isEmailExist(String email) {
        return customerRepository.isEmailExist(email);
    }

    private boolean isUsernameExist(String username) {
        return userService.isUsernameExist(username);
    }

}
