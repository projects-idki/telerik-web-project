package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Country;
import com.telerik.deliverit.repositories.contracts.CountryRepository;
import com.telerik.deliverit.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAllCountries() {
        return countryRepository.getAllCountries();
    }

    @Override
    public Country getCountryById(int id) {
        return countryRepository.getCountryById(id);
    }

    @Override
    public Country getCountryByName(String name) {
        return countryRepository.getCountryByName(name);
    }

    @Override
    public Country createCountry(String name) {
        boolean countryExists = true;

        try {
            countryRepository.getCountryByName(name);
        } catch (EntityNotFoundException e){
            countryExists = false;
        }

        if(countryExists){
            throw new DuplicateEntityException("Country", "name", name);
        }
        return countryRepository.createCountry(name);
    }

    @Override
    public Country getOrCreate(String name) {
        Country country;
        try {
             country = countryRepository.getCountryByName(name);
        } catch (EntityNotFoundException e){
            country = createCountry(name);
        }
        return country;
    }
}
