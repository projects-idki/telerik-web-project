package com.telerik.deliverit.services.utils;

import com.telerik.deliverit.exceptions.UnauthorizedOperationException;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.utils.UserRole;
import org.springframework.stereotype.Component;

@Component
public class ValidationHelper {
    public static final String AUTHENTICATION_ERROR_MESSAGE = "User is not authorised";

    public static void verifyUserIsEmployeeOrAdmin(User user) {
        if (user.getUserRole().equals(UserRole.CUSTOMER)) {
            throw new UnauthorizedOperationException(AUTHENTICATION_ERROR_MESSAGE);
        }
    }

    public static void verifyUserIsCreatorOrAdmin(Employee employee, User user) {
        if (!employee.getUser().equals(user) || !user.getUserRole().equals(UserRole.ADMIN)) {
            throw new UnauthorizedOperationException(AUTHENTICATION_ERROR_MESSAGE);
        }
    }

    public static void verifyUserIsAdmin(User user) {
        if (!user.getUserRole().equals(UserRole.ADMIN)) {
            throw new UnauthorizedOperationException(AUTHENTICATION_ERROR_MESSAGE);
        }
    }

    public boolean isCustomerAuthorised(User user, Customer customer) {
        if(user == null){
            return false;
        }
        return customer.getUser().equals(user) || user.getUserRole().equals(UserRole.EMPLOYEE) || user.getUserRole().equals(UserRole.ADMIN);
    }

    public boolean isUserEmployee(User user) {
        if(user == null){
            return false;
        }
        return user.getUserRole().equals(UserRole.EMPLOYEE) || user.getUserRole().equals(UserRole.ADMIN);
    }

    public boolean isUserCustomer(User user){
        if(user == null){
            return false;
        }
        return user.getUserRole().equals(UserRole.CUSTOMER);
    }


}
