package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityHasDependencies;
import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.repositories.contracts.ParcelRepository;
import com.telerik.deliverit.repositories.contracts.ShipmentRepository;
import com.telerik.deliverit.repositories.contracts.WarehouseRepository;
import com.telerik.deliverit.services.contracts.WarehouseService;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository warehouseRepository;
    private final ShipmentRepository shipmentRepository;

    @Autowired
    public WarehouseServiceImpl(WarehouseRepository warehouseRepository, ShipmentRepository shipmentRepository) {
        this.warehouseRepository = warehouseRepository;
        this.shipmentRepository = shipmentRepository;
    }

    @Override
    public List<Warehouse> getAll() {
        return warehouseRepository.getAll();
    }

    @Override
    public Warehouse getWarehouseById(int id) {
        return warehouseRepository.getWarehouseById(id);
    }

    @Override
    public Shipment getNextShipment(User user, int warehouseId) {

        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        Warehouse warehouse = getWarehouseById(warehouseId);
        return shipmentRepository.getNextShipmentInWarehouse(warehouse);

    }

    @Override
    public Warehouse createWarehouse(Warehouse warehouse, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        if(getAll().contains(warehouse)){
            throw new DuplicateEntityException("Same Warehouse");
        }
        warehouseRepository.createWarehouse(warehouse);
        return warehouse;
    }

    @Override
    public Warehouse updateWarehouse(Warehouse warehouse, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        if(getAll().contains(warehouse) && !getWarehouseById(warehouse.getId()).equals(warehouse)){
            throw new DuplicateEntityException("Same Warehouse");
        }
        warehouseRepository.updateWarehouse(warehouse);
        return warehouse;
    }

    @Override
    public void deleteWarehouse(int id, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        if(!shipmentRepository.getAllShipmentsByWarehouse(id).isEmpty()){
            throw new EntityHasDependencies("Cannot delete Warehouse. Present parcel dependencies");
        }
        warehouseRepository.deleteWarehouse(id);
    }


}
