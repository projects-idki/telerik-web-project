package com.telerik.deliverit.services.mapper;

import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.UserDto;
import com.telerik.deliverit.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {
    private final UserService userService;

    public UserModelMapper(UserService userService) {
        this.userService = userService;
    }

    public User userDtoToUser(UserDto userDto){
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        return user;
    }


}
