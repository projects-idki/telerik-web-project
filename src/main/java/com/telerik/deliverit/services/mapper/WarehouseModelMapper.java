package com.telerik.deliverit.services.mapper;

import com.telerik.deliverit.models.Address;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.models.dtomodels.WarehouseDto;
import com.telerik.deliverit.services.contracts.AddressService;
import com.telerik.deliverit.services.contracts.WarehouseService;
import org.springframework.stereotype.Component;

@Component
public class WarehouseModelMapper {

    private final WarehouseService warehouseService;
    private final AddressService addressService;

    public WarehouseModelMapper(WarehouseService warehouseService, AddressService addressService) {
        this.warehouseService = warehouseService;
        this.addressService = addressService;
    }

    public Warehouse fromDto(WarehouseDto warehouseDto) {
        Warehouse warehouse = new Warehouse();
        dtoToObject(warehouseDto, warehouse);
        return warehouse;
    }

    public Warehouse fromDto(WarehouseDto warehouseDto, int id) {
        Warehouse warehouse = warehouseService.getWarehouseById(id);
        dtoToObject(warehouseDto, warehouse);
        return warehouse;
    }

    private void dtoToObject(WarehouseDto warehouseDto, Warehouse warehouse) {
        Address address = addressService.getOrCreate(warehouseDto.getStreetName(),
                warehouseDto.getCityName(),
                warehouseDto.getCountryName());
        warehouse.setAddress(address);
    }

    public WarehouseDto toDto(Warehouse warehouse){
        WarehouseDto warehouseDto = new WarehouseDto();
        warehouseDto.setCountryName(warehouse.getAddress().getCity().getCountryName());
        warehouseDto.setCityName(warehouse.getAddress().getCityName());
        warehouseDto.setStreetName(warehouse.getAddress().getStreetAddress());
        return warehouseDto;
    }
}
