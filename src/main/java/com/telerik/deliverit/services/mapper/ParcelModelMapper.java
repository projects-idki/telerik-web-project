package com.telerik.deliverit.services.mapper;

import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.ParcelCategory;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.models.dtomodels.CustomerDto;
import com.telerik.deliverit.models.dtomodels.ParcelDto;
import com.telerik.deliverit.models.dtomodels.ParcelDtoOutput;
import com.telerik.deliverit.services.contracts.CustomerService;
import com.telerik.deliverit.services.contracts.ParcelCategoryService;
import com.telerik.deliverit.services.contracts.ParcelService;
import com.telerik.deliverit.services.contracts.WarehouseService;
import org.springframework.stereotype.Component;

@Component
public class ParcelModelMapper {

    private final CustomerService customerService;
    private final WarehouseService warehouseService;
    private final ParcelCategoryService parcelCategoryService;
    private final ParcelService parcelService;

    public ParcelModelMapper(CustomerService customerService, WarehouseService warehouseService, ParcelCategoryService parcelCategoryService, ParcelService parcelService) {
        this.customerService = customerService;
        this.warehouseService = warehouseService;
        this.parcelCategoryService = parcelCategoryService;
        this.parcelService = parcelService;
    }

    public Parcel parcelDtoToParcel(ParcelDto parcelDto) {
        Customer customer = customerService.getCustomerById(parcelDto.getCustomerId());
        Warehouse warehouse = warehouseService.getWarehouseById(parcelDto.getWarehouseId());
        ParcelCategory parcelCategory = parcelCategoryService.getParcelCategoryById(parcelDto.getCategoryId());
        return new Parcel(customer, warehouse, parcelDto.getWeight(), parcelCategory);
    }

    public Parcel parcelDtoToParcel(ParcelDto parcelDto, int id) {
        Parcel parcel = parcelService.getParcelById(id);
        parcel.setCustomer(customerService.getCustomerById(parcelDto.getCustomerId()));
        parcel.setWarehouse(warehouseService.getWarehouseById(parcelDto.getWarehouseId()));
        parcel.setWeight(parcelDto.getWeight());
        parcel.setCategory(parcelCategoryService.getParcelCategoryById(parcelDto.getCategoryId()));
        return parcel;
    }


    public ParcelDtoOutput parcelToParcelDtoOutput(Parcel parcel){
        ParcelDtoOutput parcelDtoOutput = new ParcelDtoOutput();
        parcelDtoOutput.setId(parcel.getId());
        parcelDtoOutput.setName(parcel.getCustomer().getFirstName()+" " +parcel.getCustomer().getLastName());
        parcelDtoOutput.setWarehouseId(parcel.getWarehouse().getId());
        parcelDtoOutput.setCategory(parcel.getCategory().getCategory());
        parcelDtoOutput.setWeight(parcel.getWeight());
        if(parcel.getShipment() != null) {
            parcelDtoOutput.setStatus(parcel.getShipment().getStatus());
            parcelDtoOutput.setDepartureDate(parcel.getShipment().getDepartureDate().toString());
            parcelDtoOutput.setArrivalDate(parcel.getShipment().getArrivalDate().toString());
        } else{
            parcelDtoOutput.setStatus("Not Processed");
            parcelDtoOutput.setDepartureDate("No Departure Date");
            parcelDtoOutput.setArrivalDate("No Arrival Date");
        }

        return parcelDtoOutput;
    }

    public ParcelDto parcelToDto(Parcel parcel){
        ParcelDto parcelDto = new ParcelDto();
        parcelDto.setCustomerId(parcel.getCustomer().getId());
        parcelDto.setWarehouseId(parcel.getWarehouse().getId());
        parcelDto.setWeight(parcel.getWeight());
        parcelDto.setCategoryId(parcel.getCategory().getId());

        return parcelDto;
    }
}
