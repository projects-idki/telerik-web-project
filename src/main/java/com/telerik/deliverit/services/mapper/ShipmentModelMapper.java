package com.telerik.deliverit.services.mapper;

import com.telerik.deliverit.exceptions.UnauthorizedOperationException;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoCreate;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoUpdate;
import com.telerik.deliverit.services.contracts.ShipmentService;
import com.telerik.deliverit.services.contracts.WarehouseService;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ShipmentModelMapper {

    private final ShipmentService shipmentService;
    private final WarehouseService warehouseService;


    public ShipmentModelMapper(ShipmentService shipmentService, WarehouseService warehouseService) {
        this.shipmentService = shipmentService;
        this.warehouseService = warehouseService;
    }


    public Shipment shipmentDtoToShipment(ShipmentDtoCreate shipmentDtoCreate) {
        Shipment shipment = new Shipment();
        shipmentDtoToObject(shipmentDtoCreate, shipment);
        return shipment;
    }

    private void shipmentDtoToObject(ShipmentDtoCreate shipmentDto, Shipment shipment) {
        Warehouse warehouse = warehouseService.getWarehouseById(shipmentDto.getWarehouseToId());
        shipment.setWarehouseTo(warehouse);
    }

    public Shipment shipmentDtoToShipment(ShipmentDtoUpdate shipmentDto, int id) {
        Shipment shipment = shipmentService.getShipmentById(id);
        Warehouse warehouse = warehouseService.getWarehouseById(shipmentDto.getWarehouseToId());
        if(!shipment.getWarehouseTo().equals(warehouse)){
            throw new UnauthorizedOperationException("You cannot change the WarehouseTo in a Shipment with Parcels");
        }
        shipmentService.parcelIdSetToParcel(shipmentDto.getParcelIdSet(), shipment);
        shipment.setDepartureDate(shipmentDto.getDepartureDate());
        shipment.setArrivalDate(shipmentDto.getArrivalDate());
        shipment.setWarehouseTo(warehouse);
        return shipment;
    }

    public ShipmentDtoUpdate shipmentToDto(Shipment shipment){
        ShipmentDtoUpdate shipmentDtoUpdate = new ShipmentDtoUpdate();
        shipmentDtoUpdate.setArrivalDate(shipment.getArrivalDate());
        shipmentDtoUpdate.setDepartureDate(shipment.getDepartureDate());
        shipmentDtoUpdate.setParcelIdSet(shipment.getParcelSet().stream().map(Parcel::getId).collect(Collectors.toSet()));
        shipmentDtoUpdate.setWarehouseToId(shipment.getWarehouseTo().getId());
        return shipmentDtoUpdate;

    }
}
