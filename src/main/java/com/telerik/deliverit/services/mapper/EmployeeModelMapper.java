package com.telerik.deliverit.services.mapper;

import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.EmployeeDto;
import com.telerik.deliverit.models.utils.UserRole;
import com.telerik.deliverit.services.contracts.EmployeeService;
import org.springframework.stereotype.Component;

@Component
public class EmployeeModelMapper {

    private final EmployeeService employeeService;


    public EmployeeModelMapper(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public Employee employeeDtoToEmployee(EmployeeDto employeeDto) {
        User user = new User(employeeDto.getUsername(), employeeDto.getPassword(), UserRole.EMPLOYEE);
        return new Employee(employeeDto.getFirstName(), employeeDto.getLastName(), employeeDto.getEmail(), user);
    }

    public Employee employeeDtoToEmployee(EmployeeDto employeeDto, int id) {

        User user = employeeService.getEmployeeById(id).getUser();
        user.setUsername(employeeDto.getUsername());
        user.setPassword(employeeDto.getPassword());

        Employee employee = employeeService.getEmployeeById(id);
        employee.setFirstName(employeeDto.getFirstName());
        employee.setLastName(employeeDto.getLastName());
        employee.setEmail(employeeDto.getEmail());
        employee.setUser(user);

        return employee;
    }

    public EmployeeDto employeeToDto(Employee employee) {

        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setLastName(employee.getLastName());
        employeeDto.setEmail(employee.getEmail());
        employeeDto.setUsername(employee.getUser().getUsername());
        employeeDto.setPassword(employee.getUser().getPassword());

        return employeeDto;
    }
}
