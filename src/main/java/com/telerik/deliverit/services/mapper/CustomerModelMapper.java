package com.telerik.deliverit.services.mapper;

import com.telerik.deliverit.models.Address;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.CustomerDto;
import com.telerik.deliverit.models.dtomodels.CustomerDtoWithoutCredentials;
import com.telerik.deliverit.models.utils.UserRole;
import com.telerik.deliverit.services.contracts.AddressService;
import com.telerik.deliverit.services.contracts.CustomerService;
import org.springframework.stereotype.Component;

@Component
public class CustomerModelMapper {

    private final CustomerService customerService;
    private final AddressService addressService;

    public CustomerModelMapper(CustomerService customerService, AddressService addressService) {
        this.customerService = customerService;
        this.addressService = addressService;
    }

    public Customer customerDtoToCustomer(CustomerDto customerDto) {
        Address address = addressService.getOrCreate(customerDto.getStreetAddress(), customerDto.getCity(), customerDto.getCountry());
        User user = new User(customerDto.getUsername(), customerDto.getPassword(), UserRole.CUSTOMER);
        return new Customer(customerDto.getFirstName(), customerDto.getLastName(), customerDto.getEmail(), user, address);


    }

    public Customer customerDtoToCustomer(CustomerDto customerDto, int id) {
        Address address = addressService.getOrCreate(customerDto.getStreetAddress(),
                customerDto.getCity(),
                customerDto.getCountry());
        User user = customerService.getCustomerById(id).getUser();
        user.setUsername(customerDto.getUsername());
        user.setPassword(customerDto.getPassword());
        Customer customer = customerService.getCustomerById(id);
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setEmail(customerDto.getEmail());
        customer.setAddress(address);
        customer.setUser(user);
        return customer;
    }

    public Customer customerDtoToCustomer(CustomerDtoWithoutCredentials customerDto, int id) {
        Address address = addressService.getOrCreate(customerDto.getStreetAddress(),
                customerDto.getCity(),
                customerDto.getCountry());

        Customer customer = customerService.getCustomerById(id);
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setEmail(customerDto.getEmail());
        customer.setAddress(address);

        return customer;
    }


    public CustomerDtoWithoutCredentials toDto(Customer customer) {

        CustomerDtoWithoutCredentials customerDto = new CustomerDtoWithoutCredentials();

        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setEmail(customer.getEmail());
        customerDto.setStreetAddress(customer.getAddress().getStreetAddress());
        customerDto.setCity(customer.getAddress().getCity().getId());
        customerDto.setCountry(customer.getAddress().getCity().getCountry().getId());

        return customerDto;
    }

    public CustomerDto customerToDto(Customer customer){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setEmail(customer.getEmail());
        customerDto.setUsername(customer.getUser().getUsername());
        customerDto.setPassword(customer.getUser().getPassword());
        customerDto.setStreetAddress(customer.getAddress().getStreetAddress());
        customerDto.setCity(customer.getAddress().getCity().getId());
        customerDto.setCountry(customer.getAddress().getCity().getCountry().getId());
        return customerDto;
    }
}
