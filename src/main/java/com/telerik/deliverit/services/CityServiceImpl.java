package com.telerik.deliverit.services;

import com.telerik.deliverit.models.City;
import com.telerik.deliverit.models.Country;
import com.telerik.deliverit.repositories.contracts.CityRepository;
import com.telerik.deliverit.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository repository;

    @Autowired
    public CityServiceImpl(CityRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<City> getAllCities() {
        return repository.getAllCities();
    }

    @Override
    public City getCityById(int id) {
        return repository.getCityById(id);
    }

    @Override
    public City getCityByName(String name) {
        return repository.getCityByName(name);
    }


    @Override
    public City getOrCreate(String name, Country country) {
        City city = new City(name,country);
        if(repository.contains(city)){
            return repository.getCity(city);
        }
        repository.createCity(city);
        return city;
    }

    @Override
    public List<City> getCountryCities(int countryId) {
        return repository.getCountryCities(countryId);
    }

}
