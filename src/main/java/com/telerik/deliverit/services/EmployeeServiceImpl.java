package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.repositories.contracts.EmployeeRepository;
import com.telerik.deliverit.services.contracts.EmployeeService;
import com.telerik.deliverit.services.contracts.UserService;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final UserService userService;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, UserService userService) {
        this.employeeRepository = employeeRepository;
        this.userService = userService;
    }

    @Override
    public List<Employee> getAllEmployees(User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        return employeeRepository.getAllEmployees();
    }

    @Override
    public Employee getEmployeeById(int id, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        return employeeRepository.getEmployeeById(id);
    }

    @Override
    public Employee getEmployeeById(int id) {
        return employeeRepository.getEmployeeById(id);
    }

    @Override
    public Employee createEmployee(Employee employee, User user) {

        ValidationHelper.verifyUserIsAdmin(user);

        if (employeeRepository.emailExists(employee.getEmail())) {
            throw new DuplicateEntityException("Employee", "email", employee.getEmail());
        }
        if (userService.isUsernameExist(employee.getUser().getUsername())) {
            throw new DuplicateEntityException("User", "username", employee.getUser().getUsername());
        }
        employeeRepository.createEmployee(employee);
        return employee;
    }


    @Override
    public Employee updateEmployee(Employee employeeToBeUpdated, Employee currentEmployee, User user) {

        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);

        if (!employeeToBeUpdated.getEmail().equals(currentEmployee.getEmail()) && isEmailExist(employeeToBeUpdated.getEmail())) {
            throw new DuplicateEntityException("Employee", "email", employeeToBeUpdated.getEmail());
        }
        if (!employeeToBeUpdated.getUser().getUsername().equals(currentEmployee.getUser().getUsername()) && isUsernameExist(employeeToBeUpdated.getUser().getUsername())) {
            throw new DuplicateEntityException("User", "username", employeeToBeUpdated.getUser().getUsername());
        }

        return employeeRepository.updateEmployee(employeeToBeUpdated);
    }

    @Override
    public void deleteEmployee(int id, User user) {

        ValidationHelper.verifyUserIsAdmin(user);

        employeeRepository.deleteEmployee(id);
    }

    private boolean isEmailExist(String email) {
        return employeeRepository.emailExists(email);
    }

    private boolean isUsernameExist(String username) {
        return userService.isUsernameExist(username);
    }
}
