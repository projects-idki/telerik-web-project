package com.telerik.deliverit.services;

import com.telerik.deliverit.models.ParcelCategory;
import com.telerik.deliverit.repositories.contracts.ParcelCategoryRepository;
import com.telerik.deliverit.services.contracts.ParcelCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParcelCategoryServiceImpl implements ParcelCategoryService {

    private final ParcelCategoryRepository parcelCategoryRepository;

    @Autowired
    public ParcelCategoryServiceImpl(ParcelCategoryRepository parcelCategoryRepository) {
        this.parcelCategoryRepository = parcelCategoryRepository;
    }

    @Override
    public List<ParcelCategory> getAllCategories() {
        return parcelCategoryRepository.getAllCategories();
    }

    @Override
    public ParcelCategory getParcelCategoryById(int id) {
        return parcelCategoryRepository.getParcelCategoryById(id);
    }
}
