package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.DateTimeException;
import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.exceptions.WrongWarehouseException;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.repositories.contracts.ShipmentRepository;
import com.telerik.deliverit.services.contracts.CustomerService;
import com.telerik.deliverit.services.contracts.ParcelService;
import com.telerik.deliverit.services.contracts.ShipmentService;
import com.telerik.deliverit.services.contracts.WarehouseService;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ShipmentServiceImpl implements ShipmentService {

    public static final String ADD_PARCEL_TO_SHIPMENT_ERROR_MESSAGE = "Parcel with id %d is already in the shipment.";
    public static final String REMOVE_PARCEL_TO_SHIPMENT_ERROR_MESSAGE = "Parcel with id %d does not exist in the shipment.";

    private final ShipmentRepository shipmentRepository;
    private final CustomerService customerService;
    private final WarehouseService warehouseService;
    private final ParcelService parcelService;

    @Autowired
    public ShipmentServiceImpl(ShipmentRepository shipmentRepository, CustomerService customerService, WarehouseService warehouseService, ParcelService parcelService) {
        this.shipmentRepository = shipmentRepository;
        this.customerService = customerService;
        this.warehouseService = warehouseService;
        this.parcelService = parcelService;
    }

    @Override
    public List<Shipment> getAllShipments(User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        return shipmentRepository.getAllShipments();
    }

    @Override
    public Shipment getShipmentById(int id, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        return shipmentRepository.getShipmentById(id);
    }

    @Override
    public Shipment getShipmentById(int id) {
        return shipmentRepository.getShipmentById(id);
    }


    @Override
    public Shipment createShipment(Shipment shipment, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        if (shipment.getArrivalDate().before(shipment.getDepartureDate())) {
            throw new DateTimeException();
        }
        shipmentRepository.createShipment(shipment);
        return shipment;
    }

    @Override
    public Shipment updateShipment(Shipment shipment, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        if (shipment.getArrivalDate().before(shipment.getDepartureDate())) {
            throw new DateTimeException();
        }
        return shipmentRepository.updateShipment(shipment);
    }

    @Override
    public int getAmount() {
        return shipmentRepository.getAmount();
    }

    @Override
    public void deleteShipment(int id, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        shipmentRepository.deleteShipment(id);
    }

    @Override
    public void parcelIdSetToParcel(Set<Integer> parcelIdSet, Shipment shipment) {
        var newParcelSet = shipment.getParcelSet();
        for (Integer parcelId : parcelIdSet) {
            if (newParcelSet.contains(parcelService.getParcelById(parcelId))) {
                continue;
            }
            if (parcelService.getParcelById(parcelId).getShipment() != null) {
                throw new DuplicateEntityException("Shipment", "parcel", parcelId.toString());
            }
            if (!shipment.getWarehouseTo().equals(parcelService.getParcelById(parcelId).getWarehouse())) {
                throw new WrongWarehouseException();
            }
            newParcelSet.add(parcelService.getParcelById(parcelId));

        }
        shipment.setParcelSet(newParcelSet);
    }

    @Override
    public void addParcelToShipment(Shipment shipment, Parcel parcel, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        var newShipment = shipment.getParcelSet();

        if (newShipment.contains(parcel)) {
            throw new DuplicateEntityException(String.format(ADD_PARCEL_TO_SHIPMENT_ERROR_MESSAGE, parcel.getId()));
        }
        if (parcel.getShipment() != null) {
            throw new DuplicateEntityException("Shipment", "parcel", "" + parcel.getId());
        }
        if (!shipment.getWarehouseTo().equals(parcelService.getParcelById(parcel.getId()).getWarehouse())) {
            throw new WrongWarehouseException();
        }
        newShipment.add(parcel);
        shipment.setParcelSet(newShipment);
        shipmentRepository.updateShipment(shipment);
    }

    @Override
    public void removeParcelFromShipment(Shipment shipment, Parcel parcel, User user) {

        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);

        var newShipment = shipment.getParcelSet();

        if (!newShipment.contains(parcel)) {
            throw new EntityNotFoundException(String.format(REMOVE_PARCEL_TO_SHIPMENT_ERROR_MESSAGE, parcel.getId()));
        }

        newShipment.remove(parcel);
        shipment.setParcelSet(newShipment);
        shipmentRepository.removeParcelFromShipment(shipment, parcel);
    }

    @Override
    public List<Shipment> getAllShipmentsByCustomer(int customerId, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        customerService.getCustomerById(customerId);
        return shipmentRepository.getAllShipmentsByCustomer(customerId);
    }

    @Override
    public List<Shipment> getAllShipmentsByWarehouse(int warehouseId, User user) {
        ValidationHelper.verifyUserIsEmployeeOrAdmin(user);
        warehouseService.getWarehouseById(warehouseId);
        return shipmentRepository.getAllShipmentsByWarehouse(warehouseId);
    }


}
