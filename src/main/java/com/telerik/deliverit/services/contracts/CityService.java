package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.City;
import com.telerik.deliverit.models.Country;

import java.util.List;

public interface CityService {

    List<City> getAllCities();

    City getCityById(int id);

    City getCityByName(String name);

    City getOrCreate(String city, Country addressCountry);

    List<City> getCountryCities(int countryId);
}
