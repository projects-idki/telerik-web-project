package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.ParcelCategory;

import java.util.List;

public interface ParcelCategoryService {

    List<ParcelCategory> getAllCategories();

    ParcelCategory getParcelCategoryById(int id);
}
