package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;


public interface CustomerService {

    List<Customer> getAllCustomers(User user);

    Customer getCustomerById(int id);

    Customer getCustomerById(int id, User user);

    List<Customer> findCustomerWithParameters(String first_name, String last_name, String email, User user);

    Customer createCustomer(Customer customer);

    Customer deleteCustomerById(int id, User user);

    Customer updateCustomer(Customer updateCustomer, Customer customer, User user);

    List<Customer> findByKeyword(String keyword, User user);

    int getCustomerAmount();
}
