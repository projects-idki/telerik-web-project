package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;

import java.util.List;

public interface EmployeeService {

    List<Employee> getAllEmployees(User user);

    Employee getEmployeeById(int id, User user);

    Employee getEmployeeById(int id);

    Employee createEmployee(Employee employee, User user);

    Employee updateEmployee(Employee employeeToBeUpdated, Employee currentEmployee, User user);

    void deleteEmployee(int id, User user);
}
