package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.User;

import java.util.List;
import java.util.Set;

public interface ShipmentService {

    List<Shipment> getAllShipments(User user);

    Shipment getShipmentById(int id, User user);

    Shipment getShipmentById(int id);

    Shipment createShipment(Shipment shipment, User user);

    Shipment updateShipment(Shipment shipment, User user);

    int getAmount();

    void deleteShipment(int id, User user);

    void parcelIdSetToParcel(Set<Integer> parcelIdSet, Shipment shipment);

    void addParcelToShipment(Shipment shipment, Parcel parcel, User user);

    void removeParcelFromShipment(Shipment shipment, Parcel parcel, User user);

    List<Shipment> getAllShipmentsByCustomer(int customerId, User user);

    List<Shipment> getAllShipmentsByWarehouse(int warehouseId, User user);
}
