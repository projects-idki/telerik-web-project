package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.Warehouse;

import java.util.List;

public interface WarehouseService {

    List<Warehouse> getAll();

    Warehouse getWarehouseById(int id);

    Warehouse createWarehouse(Warehouse warehouse, User user);

    Warehouse updateWarehouse(Warehouse warehouse, User user);

    void deleteWarehouse(int id, User user);

    Shipment getNextShipment(User user, int warehouseId);
}
