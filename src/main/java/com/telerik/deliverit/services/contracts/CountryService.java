package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.Country;

import java.util.List;

public interface CountryService {

    List<Country> getAllCountries();

    Country getCountryById(int id);

    Country getCountryByName(String name);

    Country createCountry(String name);

    Country getOrCreate(String name);
}
