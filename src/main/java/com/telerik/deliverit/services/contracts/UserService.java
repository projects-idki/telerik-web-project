package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;

public interface UserService {

    User tryGetUser(String username, String password);

    User createUser(User user);

    User updateUser(User user, User newUser);

    User deleteUser(User user);

    boolean isUsernameExist(String username);

    Employee getEmployee(User user);

    Customer getCustomer(User user);
}
