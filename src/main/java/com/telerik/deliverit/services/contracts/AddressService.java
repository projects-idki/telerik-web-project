package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.Address;
import com.telerik.deliverit.models.City;

import java.util.List;

public interface AddressService {
    List<Address> getAllAddresses();

    Address getAddressById(int id);

    Address getAddressByStreetName(String streetName);

    Address createAddress(String address, String city, String country);

    Address getOrCreate(String street_address, String city, String country);

    Address getOrCreate(String street_address, int city, int country);

}
