package com.telerik.deliverit.services.contracts;

import com.telerik.deliverit.models.*;

import java.util.List;
import java.util.Optional;

public interface ParcelService {

    List<Parcel> getCustomerParcels(Customer customer, User user);

    List<Parcel> getAllParcels(User user);

    Parcel createParcel(Parcel parcel,User user);

    Parcel getParcelById(int id);

    Parcel getParcelById(int id,User user);

    Parcel updateParcel(Parcel parcel,User user);

    Parcel deleteParcel(Parcel parcel,User user);

    int getAmount();

    List<Parcel> getParcelsWithParameters(Optional<Double> minWeight, Optional<Double> maxWeight, Optional<Integer> customerId, Optional<Integer> warehouseId, Optional<Integer> categoryId,User user);


    List<Parcel> sortParcels(Optional<Boolean> weight, Optional<Boolean> arrivalDate, User user);

    List<Parcel> getParcelsWithNoShipment(Warehouse warehouse, User user);
}

