package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Address;
import com.telerik.deliverit.models.City;
import com.telerik.deliverit.models.Country;
import com.telerik.deliverit.repositories.contracts.AddressRepository;
import com.telerik.deliverit.services.contracts.AddressService;
import com.telerik.deliverit.services.contracts.CityService;
import com.telerik.deliverit.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository repository;
    private final CountryService countryService;
    private final CityService cityService;

    @Autowired
    public AddressServiceImpl(AddressRepository repository, CountryService countryService, CityService cityService) {
        this.repository = repository;
        this.countryService = countryService;
        this.cityService = cityService;
    }

    @Override
    public List<Address> getAllAddresses() {
        return repository.getAllAddresses();
    }

    @Override
    public Address getAddressById(int id) {
        return repository.getAddressById(id);
    }

    @Override
    public Address getAddressByStreetName(String streetName) {
        return repository.getAddressByStreetName(streetName);
    }


    @Override
    public Address createAddress(String address, String city, String country) {

        Address newAddress = getAddress(address, city, country);
        if (repository.contains(newAddress)) {
            throw new DuplicateEntityException("Address");
        }
        return repository.createAddress(newAddress);
    }

    public Address getOrCreate(String streetAddress, String city, String country) {
        Address address;
        try {
             address = createAddress(streetAddress, city, country);
        } catch (DuplicateEntityException e) {
            address = getAddress(streetAddress, city, country);
        }
        return address;
    }

    public Address getOrCreate(String streetAddress, int city,int country){
        Country addressCountry = countryService.getCountryById(country);
        City addressCity = cityService.getCityById(city);
        if(!addressCity.getCountry().equals(addressCountry)){
            throw new EntityNotFoundException(addressCountry.getName(),"city",addressCity.getName());
        }
        Address address = new Address(addressCity,streetAddress);
        if(repository.contains(address)){
            return repository.getAddress(address);
        }
        return address;
    }

    private Address getAddress(String street_address, String city, String country){
        Country addressCountry = countryService.getOrCreate(country);
        City addressCity = cityService.getOrCreate(city, addressCountry);
        Address address = new Address(addressCity,street_address);
        if(repository.contains(address)){
            return repository.getAddress(address);
        }
        return address;
    }




}
