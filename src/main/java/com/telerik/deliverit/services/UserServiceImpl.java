package com.telerik.deliverit.services;

import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.repositories.contracts.UserRepository;
import com.telerik.deliverit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User tryGetUser(String username, String password) {
        return userRepository.tryGetUser(username, password);
    }

    @Override
    public User createUser(User user) {
        return userRepository.createUser(user);
    }

    @Override
    public User updateUser(User user, User newUser) {
        return userRepository.updateUser(user, newUser);
    }

    @Override
    public User deleteUser(User user) {
        return userRepository.deleteUser(user);
    }

    @Override
    public boolean isUsernameExist(String username) {
        List<User> userList = userRepository.getAllUsers();
        List<String> usernameList = userList.stream()
                .map(User::getUsername).collect(Collectors.toList());
        return usernameList.contains(username);
    }

    @Override
    public Employee getEmployee(User user) {
        return userRepository.getEmployee(user);
    }

    @Override
    public Customer getCustomer(User user) {
            return userRepository.getCustomer(user);
    }


}
