package com.telerik.deliverit.services;

import com.telerik.deliverit.exceptions.EntityHasDependencies;
import com.telerik.deliverit.exceptions.UnauthorizedOperationException;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.repositories.contracts.ParcelRepository;
import com.telerik.deliverit.services.contracts.CustomerService;
import com.telerik.deliverit.services.contracts.ParcelCategoryService;
import com.telerik.deliverit.services.contracts.ParcelService;
import com.telerik.deliverit.services.contracts.WarehouseService;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ParcelServiceImpl implements ParcelService {
    private final ParcelRepository parcelRepository;
    private final CustomerService customerService;
    private final WarehouseService warehouseService;
    private final ParcelCategoryService parcelCategoryService;
    private final ValidationHelper validationHelper;

    @Autowired
    public ParcelServiceImpl(ParcelRepository parcelRepository,
                             CustomerService customerService,
                             WarehouseService warehouseService,
                             ParcelCategoryService parcelCategoryService, ValidationHelper validationHelper) {
        this.parcelRepository = parcelRepository;
        this.customerService = customerService;
        this.warehouseService = warehouseService;
        this.parcelCategoryService = parcelCategoryService;
        this.validationHelper = validationHelper;
    }

    @Override
    public List<Parcel> getCustomerParcels(Customer customer, User user) {
        if (!validationHelper.isCustomerAuthorised(user, customer)) {
            throw new UnauthorizedOperationException("User is not authorised");
        }
        List<Parcel> parcelList = parcelRepository.getCustomerParcels(customer.getId());
        return parcelList;
    }

    @Override
    public List<Parcel> getAllParcels(User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User is not authorised");
        }
        return parcelRepository.getAllParcels();
    }

    @Override
    public Parcel createParcel(Parcel parcel, User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User is not authorised");
        }
        return parcelRepository.createParcel(parcel);
    }

    @Override
    public Parcel getParcelById(int id) {
        return parcelRepository.getParcelById(id);
    }

    @Override
    public Parcel getParcelById(int id, User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User is not authorised");
        }
        return getParcelById(id);
    }

    @Override
    public Parcel updateParcel(Parcel parcel, User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User is not authorised");
        }
        Parcel parcelToUpdate = getParcelById(parcel.getId());
        if(parcel.getShipment() != null && !parcel.getWarehouse().equals(parcelToUpdate.getWarehouse())){
            throw new UnauthorizedOperationException("Cannot change parcel with shipment");
        }
        return parcelRepository.updateParcel(parcel);
    }

    @Override
    public Parcel deleteParcel(Parcel parcel, User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User is not authorised");
        }
        if(parcel.getShipment() != null){
            throw new EntityHasDependencies("Parcel is already a part of a shipment,delete it from the shipment");
        }
        return parcelRepository.deleteParcel(parcel);
    }

    @Override
    public int getAmount() {
        return parcelRepository.getAmount();
    }

    @Override
    public List<Parcel> getParcelsWithParameters(Optional<Double> minWeight,
                                                 Optional<Double> maxWeight,
                                                 Optional<Integer> customerId,
                                                 Optional<Integer> warehouseId,
                                                 Optional<Integer> categoryId,
                                                 User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User is not authorised");
        }
        if (minWeight.isPresent() && minWeight.get() > 100) {
            throw new IllegalArgumentException("minWeight should be under 100kg");
        }
        if (maxWeight.isPresent() && maxWeight.get() < 0) {
            throw new IllegalArgumentException("maxWeight should be over 0kg");
        }
        if (customerId.isPresent()) {
            customerService.getCustomerById(customerId.get());
        }
        if (warehouseId.isPresent()) {
            warehouseService.getWarehouseById(warehouseId.get());
        }
        if (categoryId.isPresent()) {
            parcelCategoryService.getParcelCategoryById(categoryId.get());
        }
        return parcelRepository.getParcelsWithparameters(minWeight, maxWeight, customerId, warehouseId, categoryId);
    }

    @Override
    public List<Parcel> sortParcels(Optional<Boolean> weight,
                                    Optional<Boolean> arrivalDate,
                                    User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User is not authorised");
        }
        return parcelRepository.sortParcels(weight,arrivalDate);
    }

    @Override
    public List<Parcel> getParcelsWithNoShipment(Warehouse warehouse, User user) {
        if (!validationHelper.isUserEmployee(user)) {
            throw new UnauthorizedOperationException("User is not authorised");
        }
        return parcelRepository.getParcelsWithNoShipment(warehouse);
    }

}
