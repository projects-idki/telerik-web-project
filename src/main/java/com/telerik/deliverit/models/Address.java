package com.telerik.deliverit.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;


    @Column(name = "street_address")
    private String streetAddress;

    public Address() {
    }

    public Address(int id, City city, String streetAddress) {
        this.id = id;
        this.city = city;
        this.streetAddress = streetAddress;
    }

    public Address(City city, String streetAddress) {
        this.city = city;
        this.streetAddress = streetAddress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    @JsonIgnore
    public String getCityName() {
        return getCity().getName();
    }

    @Override
    public boolean equals(Object address) {
        if (address instanceof Address) {
            Address addressToCheck = (Address) address;
            return (this.streetAddress.equals(addressToCheck.streetAddress) && this.city.equals(addressToCheck.city));
        }
        return false;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Address address = (Address) o;
//        return  Objects.equals(city.getName(), address.getCity().getName())
//                && Objects.equals(city.getCountry().getName(), address.getCity().getCountry().getName())
//                && Objects.equals(streetAddress, address.streetAddress);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(city.getName(), city.getCountry().getName(), streetAddress);
//    }
}
