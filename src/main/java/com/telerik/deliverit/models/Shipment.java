package com.telerik.deliverit.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerik.deliverit.models.utils.Status;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "shipment")
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;


    @Column(name = "departure_date")
    private Date departureDate;


    @Column(name = "arrival_date")
    private Date arrivalDate;

    @ManyToOne
    @JoinColumn(name = "warehouse_to_id")
    private Warehouse warehouseTo;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "parcels_in_shipments",
            joinColumns = @JoinColumn(name = "shipment_id"),
            inverseJoinColumns = @JoinColumn(name = "parcel_id")
    )
    private Set<Parcel> parcelSet;

    public Shipment() {
    }

    public Shipment(Date departureDate, Date arrivalDate, Warehouse warehouseTo) {
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.warehouseTo = warehouseTo;
        this.parcelSet = new HashSet<>();
    }

    public Shipment(int id, Date departureDate, Date arrivalDate, Warehouse warehouseTo) {
        this.id = id;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.warehouseTo = warehouseTo;
        this.parcelSet = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getStatus() {
        if (arrivalDate != null && departureDate != null && arrivalDate.before(Date.valueOf(LocalDate.now()))) {
            return Status.COMPLETED.toString();
        }
        if (arrivalDate != null && departureDate != null && departureDate.before(Date.valueOf(LocalDate.now())) && !arrivalDate.before(Date.valueOf(LocalDate.now()))) {
            return Status.ON_THE_WAY.toString();
        }
        return Status.PREPARING.toString();
    }

    public Warehouse getWarehouseTo() {
        return warehouseTo;
    }

    public void setWarehouseTo(Warehouse warehouseTo) {
        this.warehouseTo = warehouseTo;
    }

    public Set<Parcel> getParcelSet() {
        return parcelSet;
    }

    public void setParcelSet(Set<Parcel> parcelSet) {
        this.parcelSet = parcelSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shipment shipment = (Shipment) o;
        return id == shipment.id && Objects.equals(departureDate, shipment.departureDate) && Objects.equals(arrivalDate, shipment.arrivalDate) && Objects.equals(warehouseTo, shipment.warehouseTo) && Objects.equals(parcelSet, shipment.parcelSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, departureDate, arrivalDate, warehouseTo, parcelSet);
    }
}
