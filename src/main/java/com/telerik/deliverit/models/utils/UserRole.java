package com.telerik.deliverit.models.utils;

public enum UserRole {

    CUSTOMER,
    EMPLOYEE,
    ADMIN;


    @Override
    public String toString() {
        switch (this) {
            case CUSTOMER:
                return "Customer";
            case EMPLOYEE:
                return "Employee";
            case ADMIN:
                return "Admin";
            default:
                return "";
        }
    }
}
