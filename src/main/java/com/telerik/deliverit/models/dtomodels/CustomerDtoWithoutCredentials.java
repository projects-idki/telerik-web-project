package com.telerik.deliverit.models.dtomodels;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CustomerDtoWithoutCredentials {

    @NotNull(message = "FirstName should not be empty")
    @Size(min = 2, max = 20, message = "Customer first name should be between 2 and 20 characters.")
    private String firstName;

    @NotNull(message = "LastName should not be empty")
    @Size(min = 2, max = 20, message = "Customer last name should be between 2 and 20 characters.")
    private String lastName;

    @NotNull(message = "Email should not be empty")
    @Size(min = 2, max = 50, message = "Customer email should be between 2 and 50 characters.")
    private String email;

    @NotNull(message = "street should not be empty")
    @Size(min = 2, max = 100, message = "Customer street address should be between 2 and 100 characters.")
    private String streetAddress;

    @NotNull(message = "City should not be empty")
    private int city;

    @NotNull(message = "Country should not be empty")
    private int country;


    public CustomerDtoWithoutCredentials() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }
}
