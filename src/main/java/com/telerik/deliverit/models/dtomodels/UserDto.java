package com.telerik.deliverit.models.dtomodels;

import javax.validation.constraints.NotNull;

public class UserDto {

    @NotNull(message = "Username should not be null")
    private String username;

    @NotNull(message = "Password should not be null")
    private String password;

    public UserDto() {
    }

    public UserDto(@NotNull String username, @NotNull String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
