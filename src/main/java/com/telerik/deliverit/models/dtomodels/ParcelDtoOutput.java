package com.telerik.deliverit.models.dtomodels;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telerik.deliverit.models.utils.Status;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;

public class ParcelDtoOutput {

    int id;

    String name;

    int warehouseId;

    Double weight;

    String category;

    String status;

    String departureDate;

    String  arrivalDate;

    public ParcelDtoOutput() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
}
