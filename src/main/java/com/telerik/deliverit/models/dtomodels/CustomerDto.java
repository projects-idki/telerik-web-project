package com.telerik.deliverit.models.dtomodels;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CustomerDto {

    @NotNull(message = "FirstName should not be empty")
    @Size(min = 2, max = 20, message = "Customer first name should be between 2 and 20 characters.")
    private String firstName;

    @NotNull(message = "LastName should not be empty")
    @Size(min = 2, max = 20, message = "Customer last name should be between 2 and 20 characters.")
    private String lastName;

    @NotNull(message = "Email should not be empty")
    @Size(min = 2, max = 50, message = "Customer email should be between 2 and 50 characters.")
    private String email;

    @NotNull(message = "Username should not be empty")
    @Size(min = 2, max = 35, message = "Customer username should be between 2 and 35 characters.")
    private String username;

    @NotNull(message = "Password should not be empty")
    @Size(min = 2, max = 35, message = "Customer password should be between 2 and 35 characters.")
    private String password;

    @NotNull(message = "street should not be empty")
    @Size(min = 2, max = 100, message = "Customer street address should be between 2 and 100 characters.")
    private String streetAddress;

    @NotNull(message = "City should not be empty")
    private int city;

    @NotNull(message = "Country should not be empty")
    private int country;


    public CustomerDto() {
    }

    public CustomerDto(String firstName,
                       String lastName,
                       String email,
                       String username,
                       String password,
                       String streetAddress,
                       int city,
                       int country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.streetAddress = streetAddress;
        this.city = city;
        this.country = country;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }
}
