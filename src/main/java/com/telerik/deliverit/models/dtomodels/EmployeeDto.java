package com.telerik.deliverit.models.dtomodels;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EmployeeDto {

    @NotNull
    @Size(min = 2, max = 20, message = "Employee first name should be between 2 and 20 characters.")
    private String firstName;

    @NotNull
    @Size(min = 2, max = 20, message = "Employee last name should be between 2 and 20 characters.")
    private String lastName;

    @NotNull
    @Size(min = 2, max = 50, message = "Employee email should be between 2 and 35 characters.")
    private String email;

    @NotNull
    @Size(min = 2, max = 35, message = "Employee username should be between 2 and 35 characters.")
    private String username;

    @NotNull
    @Size(min = 2, max = 35, message = "Employee password should be between 2 and 35 characters.")
    private String password;

    public EmployeeDto() {
    }

    public EmployeeDto(String firstName, String lastName, String email, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
