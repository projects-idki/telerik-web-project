package com.telerik.deliverit.models.dtomodels;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Positive;

import java.sql.Date;
import java.util.Set;

public class ShipmentDtoUpdate {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date departureDate;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date arrivalDate;


    @Positive
    private int warehouseToId;


    private Set<Integer> parcelIdSet;

    public ShipmentDtoUpdate() {
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getWarehouseToId() {
        return warehouseToId;
    }

    public void setWarehouseToId(int warehouseToId)  {
        this.warehouseToId = warehouseToId;
    }

    public Set<Integer> getParcelIdSet() {
        return parcelIdSet;
    }

    public void setParcelIdSet(Set<Integer> parcelIdSet) {
        this.parcelIdSet = parcelIdSet;
    }
}
