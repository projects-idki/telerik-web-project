package com.telerik.deliverit.models.dtomodels;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ParcelDto {
    @NotNull
    private int customerId;
    @NotNull
    private int warehouseId;

    @NotNull
    @Min(0)
    @Max(100)
    private double weight;

    @NotNull
    private int categoryId;

    public ParcelDto() {
    }

    public ParcelDto(int customerId, int warehouseId, double weight, int categoryId) {
        this.customerId = customerId;
        this.warehouseId = warehouseId;
        this.weight = weight;
        this.categoryId = categoryId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
