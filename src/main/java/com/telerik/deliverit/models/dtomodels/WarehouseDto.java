package com.telerik.deliverit.models.dtomodels;

import org.hibernate.validator.constraints.UniqueElements;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class WarehouseDto {

    @NotNull(message = "Street address cannot be null")
    @Size(min = 2, max = 50, message = "Street address should be between 2 and 50 symbols")
    private String streetName;


    @NotNull(message = "City name cannot be null")
    @Size(min = 2, max = 20, message = "City name should be between 2 and 20 symbols")
    private String cityName;


    @NotNull(message = "Country name should not be null.")
    @Size(min = 3, max = 20, message = "Country name should be between 3 and 20 characters.")
    private String countryName;

    public WarehouseDto() {
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
