package com.telerik.deliverit.models.dtomodels;

import javax.validation.constraints.Positive;

public class ShipmentDtoAddAndRemoveParcel {

    @Positive
    private int parcelId;

    public ShipmentDtoAddAndRemoveParcel() {
    }

    public int getParcelId() {
        return parcelId;
    }

    public void setParcelId(int parcelId) {
        this.parcelId = parcelId;
    }
}
