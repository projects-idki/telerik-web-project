package com.telerik.deliverit.models.dtomodels;

import javax.validation.constraints.Positive;

public class ShipmentDtoCreate {

    @Positive
    private int warehouseToId;

    public ShipmentDtoCreate() {
    }

    public int getWarehouseToId() {
        return warehouseToId;
    }

    public void setWarehouseToId(int warehouseToId) {
        this.warehouseToId = warehouseToId;
    }
}
