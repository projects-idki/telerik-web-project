package com.telerik.deliverit.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table
public class Parcel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Warehouse warehouse;

    @NotNull
    @Column(name = "weight")
    private double weight;

    @ManyToOne
    private ParcelCategory category;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "shipment_id")
    private Shipment shipment;

    public Parcel() {
    }

    public Parcel(int id, Customer customer, Warehouse warehouse, double weight, ParcelCategory category) {
        this.id = id;
        this.customer = customer;
        this.warehouse = warehouse;
        this.weight = weight;
        this.category = category;
        this.shipment = null;
    }

    public Parcel(Customer customer, Warehouse warehouse, double weight, ParcelCategory category) {
        this.customer = customer;
        this.warehouse = warehouse;
        this.weight = weight;
        this.category = category;
        this.shipment = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public ParcelCategory getCategory() {
        return category;
    }

    public void setCategory(ParcelCategory category) {
        this.category = category;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parcel parcel = (Parcel) o;
        return id == parcel.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
