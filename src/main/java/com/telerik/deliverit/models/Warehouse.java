package com.telerik.deliverit.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "warehouse")
public class Warehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    public Warehouse() {
    }

    public Warehouse(int id, Address address) {
        this.id = id;
        this.address = address;
    }

    public Warehouse(Address address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Warehouse warehouse = (Warehouse) o;
        return  Objects.equals(warehouse.getAddress().getCity().getName(), address.getCity().getName())
                && Objects.equals(warehouse.getAddress().getCity().getCountry().getName(), address.getCity().getCountry().getName())
                && Objects.equals(warehouse.getAddress().getStreetAddress(), address.getStreetAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(address.getCity().getName(), address.getCity().getCountry().getName(), address.getStreetAddress());
    }
}
