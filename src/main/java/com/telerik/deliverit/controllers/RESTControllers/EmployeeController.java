package com.telerik.deliverit.controllers.RESTControllers;

import com.telerik.deliverit.exceptions.ExceptionHandler;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.EmployeeDto;
import com.telerik.deliverit.services.contracts.EmployeeService;
import com.telerik.deliverit.services.mapper.EmployeeModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/employee")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final AuthenticationHelper authenticationHelper;
    private final EmployeeModelMapper mapper;
    private final ExceptionHandler exceptionHandler;

    @Autowired
    public EmployeeController(EmployeeService employeeService, AuthenticationHelper authenticationHelper, EmployeeModelMapper mapper, ExceptionHandler exceptionHandler) {
        this.employeeService = employeeService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
        this.exceptionHandler = exceptionHandler;
    }


    @GetMapping
    public List<Employee> getAllEmployees(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            authenticationHelper.tryGetUser(headers);
            return employeeService.getAllEmployees(user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }


    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            authenticationHelper.tryGetUser(headers);
            return employeeService.getEmployeeById(id, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }


    @PostMapping
    public Employee createEmployee(@Valid @RequestBody EmployeeDto employeeDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Employee employee = mapper.employeeDtoToEmployee(employeeDto);
            employeeService.createEmployee(employee, user);
            return employee;
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @PutMapping("/{id}")
    public Employee updateEmployee(@PathVariable int id, @Valid @RequestBody EmployeeDto employeeDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Employee employeeCurrent = employeeService.getEmployeeById(id, user);
            Employee employeeToBeUpdated = mapper.employeeDtoToEmployee(employeeDto, id);
            employeeService.updateEmployee(employeeToBeUpdated, employeeCurrent, user);
            return employeeToBeUpdated;
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public void deleteEmployee(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            employeeService.deleteEmployee(id, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
    }

}
