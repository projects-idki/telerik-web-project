package com.telerik.deliverit.controllers.RESTControllers;

import com.telerik.deliverit.exceptions.ExceptionHandler;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.ParcelDto;
import com.telerik.deliverit.models.dtomodels.ParcelDtoOutput;
import com.telerik.deliverit.services.contracts.ParcelService;
import com.telerik.deliverit.services.mapper.ParcelModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/parcel")
public class ParcelController {

    private final ParcelService parcelService;
    private final AuthenticationHelper authenticationHelper;
    private final ParcelModelMapper mapper;
    private final ExceptionHandler exceptionHandler;

    @Autowired
    public ParcelController(ParcelService parcelService, AuthenticationHelper authenticationHelper, ParcelModelMapper mapper, ExceptionHandler exceptionHandler) {
        this.parcelService = parcelService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
        this.exceptionHandler = exceptionHandler;
    }

    @GetMapping
    public List<Parcel> getAllParcels(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return parcelService.getAllParcels(user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @PostMapping
    public Parcel createParcel(@RequestHeader HttpHeaders headers,
                               @Valid @RequestBody ParcelDto parcelDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Parcel parcel = mapper.parcelDtoToParcel(parcelDto);
            return parcelService.createParcel(parcel, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;

    }

    @PutMapping("/{id}")
    public Parcel updateParcel(@RequestHeader HttpHeaders headers,
                              @Valid @RequestBody ParcelDto parcelDto,
                               @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Parcel parcel = mapper.parcelDtoToParcel(parcelDto, id);
            return parcelService.updateParcel(parcel, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public Parcel deleteParcel(@RequestHeader HttpHeaders headers,
                               @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Parcel parcel = parcelService.getParcelById(id);
            return parcelService.deleteParcel(parcel, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @GetMapping("/filter")
    public List<Parcel> getParcelsWithParameters(@RequestHeader HttpHeaders headers,
                                                 @RequestParam(required = false) Optional<Double> minWeight,
                                                 @RequestParam(required = false) Optional<Double> maxWeight,
                                                 @RequestParam(required = false) Optional<Integer> customerId,
                                                 @RequestParam(required = false) Optional<Integer> warehouseId,
                                                 @RequestParam(required = false) Optional<Integer> categoryId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return parcelService.getParcelsWithParameters(minWeight, maxWeight, customerId, warehouseId, categoryId, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @GetMapping("/sort")
    public List<ParcelDtoOutput> sortParcels(@RequestHeader HttpHeaders headers,
                                             @RequestParam(required = false) Optional<Boolean> weight,
                                             @RequestParam(required = false) Optional<Boolean> arrivalDate) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<Parcel> parcels = parcelService.sortParcels(weight, arrivalDate, user);

            return parcels.stream()
                    .map(mapper::parcelToParcelDtoOutput)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }


}
