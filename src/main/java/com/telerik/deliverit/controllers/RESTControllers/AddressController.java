package com.telerik.deliverit.controllers.RESTControllers;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.exceptions.ExceptionHandler;
import com.telerik.deliverit.models.Address;
import com.telerik.deliverit.services.contracts.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

//@RestController
@RequestMapping("/v1/address")
public class AddressController {

    private final AddressService service;
    private final ExceptionHandler exceptionHandler;

    @Autowired
    public AddressController(AddressService service, ExceptionHandler exceptionHandler) {
        this.service = service;
        this.exceptionHandler = exceptionHandler;
    }

    @GetMapping
    public List<Address> getAllAddresses() {
        try {
            return service.getAllAddresses();
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @GetMapping("/{id}")
    public Address getAddressById(@PathVariable int id) {
        try {
            return service.getAddressById(id);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @GetMapping("/search")
    public Address getAddressByStreetName(@RequestParam String streetName) {
        try {
            return service.getAddressByStreetName(streetName);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }
}
