package com.telerik.deliverit.controllers.RESTControllers;

import com.telerik.deliverit.exceptions.ExceptionHandler;
import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.models.dtomodels.WarehouseDto;
import com.telerik.deliverit.services.contracts.WarehouseService;
import com.telerik.deliverit.services.mapper.WarehouseModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/warehouse")
public class WarehouseController {

    private final WarehouseService service;
    private final WarehouseModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ExceptionHandler exceptionHandler;


    @Autowired
    public WarehouseController(WarehouseService service, WarehouseModelMapper mapper, AuthenticationHelper authenticationHelper, ExceptionHandler exceptionHandler) {
        this.service = service;
        this.modelMapper = mapper;
        this.authenticationHelper = authenticationHelper;
        this.exceptionHandler = exceptionHandler;
    }

    @GetMapping
    public List<Warehouse> getALlWarehouses() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Warehouse getWarehouseById(@PathVariable int id) {
        try {
            return service.getWarehouseById(id);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }
    @GetMapping("/{id}/nextShipment")
    public Shipment getNextShipment(@PathVariable int id,@RequestHeader HttpHeaders headers){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            return service.getNextShipment(user, id);
        } catch (Exception e){
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @PostMapping()
    public Warehouse createWarehouse(@Valid @RequestBody WarehouseDto warehouseDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Warehouse warehouse = modelMapper.fromDto(warehouseDto);
            service.createWarehouse(warehouse, user);
            return warehouse;
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @PutMapping("/{id}")
    public Warehouse updateWarehouse(@PathVariable int id, @Valid @RequestBody WarehouseDto warehouseDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Warehouse warehouse = modelMapper.fromDto(warehouseDto, id);
            service.updateWarehouse(warehouse, user);
            return warehouse;
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public void deleteWarehouse(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            service.deleteWarehouse(id, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
    }
}
