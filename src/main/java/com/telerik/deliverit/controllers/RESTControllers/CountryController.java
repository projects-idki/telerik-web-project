package com.telerik.deliverit.controllers.RESTControllers;

import com.telerik.deliverit.exceptions.ExceptionHandler;
import com.telerik.deliverit.models.Country;
import com.telerik.deliverit.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/country")
public class CountryController {

    private final CountryService countryService;
    private final ExceptionHandler exceptionHandler;

    @Autowired
    public CountryController(CountryService countryService, ExceptionHandler exceptionHandler) {
        this.countryService = countryService;
        this.exceptionHandler = exceptionHandler;
    }

    @GetMapping
    public List<Country> getAllCountries() {
        try {
            return countryService.getAllCountries();
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @GetMapping("/{id}")
    public Country getCountryById(@PathVariable int id) {
        try {
            return countryService.getCountryById(id);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @GetMapping("/search")
    public Country getCountryByName(@RequestParam String name) {
        try {
            return countryService.getCountryByName(name);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }
}
