package com.telerik.deliverit.controllers.RESTControllers;

import com.telerik.deliverit.exceptions.ExceptionHandler;
import com.telerik.deliverit.models.City;
import com.telerik.deliverit.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/city")
public class CityController {

    private final CityService service;
    private final ExceptionHandler exceptionHandler;

    @Autowired
    public CityController(CityService service, ExceptionHandler exceptionHandler) {
        this.service = service;
        this.exceptionHandler = exceptionHandler;
    }

    @GetMapping
    public List<City> getAllCities() {
        return service.getAllCities();
    }

    @GetMapping("/{id}")
    public City getCityById(@PathVariable int id) {
        try {
            return service.getCityById(id);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @GetMapping("/search")
    public City getCityByName(@RequestParam String name) {
        try {
            return service.getCityByName(name);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @GetMapping("/country/{id}")
    public List<City> getCountryCities(@PathVariable int id) {
        return service.getCountryCities(id);
    }
}
