package com.telerik.deliverit.controllers.RESTControllers;

import com.telerik.deliverit.exceptions.ExceptionHandler;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoAddAndRemoveParcel;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoCreate;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoUpdate;
import com.telerik.deliverit.services.contracts.ParcelService;
import com.telerik.deliverit.services.contracts.ShipmentService;
import com.telerik.deliverit.services.mapper.ShipmentModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/shipment")
public class ShipmentController {

    private final ShipmentService shipmentService;
    private final ParcelService parcelService;
    private final AuthenticationHelper authenticationHelper;
    private final ShipmentModelMapper mapper;
    private final ExceptionHandler exceptionHandler;

    @Autowired
    public ShipmentController(ShipmentService shipmentService, ParcelService parcelService, AuthenticationHelper authenticationHelper, ShipmentModelMapper mapper, ExceptionHandler exceptionHandler) {
        this.shipmentService = shipmentService;
        this.parcelService = parcelService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
        this.exceptionHandler = exceptionHandler;
    }


    @GetMapping
    public List<Shipment> getAllShipments(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return shipmentService.getAllShipments(user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }


    @GetMapping("/{id}")
    public Shipment getShipmentById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return shipmentService.getShipmentById(id, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @PostMapping()
    public Shipment createShipment(@Valid @RequestBody ShipmentDtoCreate shipmentDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Shipment shipment = mapper.shipmentDtoToShipment(shipmentDto);
            shipmentService.createShipment(shipment, user);
            return shipment;
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @PutMapping("/{id}")
    public Shipment updateShipment(@PathVariable int id, @Valid @RequestBody ShipmentDtoUpdate shipmentDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Shipment shipmentToBeUpdated = mapper.shipmentDtoToShipment(shipmentDto, id);
            shipmentService.updateShipment(shipmentToBeUpdated, user);
            return shipmentToBeUpdated;
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public void deleteShipment(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            shipmentService.deleteShipment(id, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
    }

    @PostMapping("{id}/parcel")
    public void addParcelToShipment(@PathVariable int id, @Valid @RequestBody ShipmentDtoAddAndRemoveParcel shipmentDto, @RequestHeader HttpHeaders headers) {

        Shipment shipment = getShipmentById(id, headers);
        var parcelId = shipmentDto.getParcelId();

        Parcel parcel;
        try {
            User user = authenticationHelper.tryGetUser(headers);
            parcel = parcelService.getParcelById(parcelId);
            shipmentService.addParcelToShipment(shipment, parcel, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
    }

    @DeleteMapping("{id}/parcel/{parcelId}")
    public void removeParcelFromShipment(@PathVariable int id, @PathVariable int parcelId , @RequestHeader HttpHeaders headers) {
        Shipment shipment = getShipmentById(id, headers);

        Parcel parcel;
        try {
            User user = authenticationHelper.tryGetUser(headers);
            parcel = parcelService.getParcelById(parcelId);
            shipmentService.removeParcelFromShipment(shipment, parcel, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
    }

    @GetMapping("/customer/{customerId}")
    public List<Shipment> getShipmentByCustomer(@PathVariable int customerId, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return shipmentService.getAllShipmentsByCustomer(customerId, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }


    @GetMapping("/warehouse/{warehouseId}")
    public List<Shipment> getShipmentByWarehouse(@PathVariable int warehouseId, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return shipmentService.getAllShipmentsByWarehouse(warehouseId, user);
        } catch (Exception e) {
            exceptionHandler.handleException(e);
        }
        return null;
    }

    @GetMapping("/{id}/parcel-list")
    public List<Parcel> getParcelList(@PathVariable int id, @RequestHeader HttpHeaders headers){
        return new ArrayList<>(getShipmentById(id, headers).getParcelSet());
    }

}
