package com.telerik.deliverit.controllers.RESTControllers;

import com.telerik.deliverit.exceptions.ExceptionHandler;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.CustomerDto;
import com.telerik.deliverit.services.contracts.CustomerService;
import com.telerik.deliverit.services.contracts.ParcelService;
import com.telerik.deliverit.services.mapper.CustomerModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;
    private final ParcelService parcelService;
    private final AuthenticationHelper authenticationHelper;
    private final CustomerModelMapper mapper;
    private final ExceptionHandler handler;

    @Autowired
    public CustomerController(CustomerService customerService, ParcelService parcelService, AuthenticationHelper authenticationHelper, CustomerModelMapper mapper, ExceptionHandler handler) {
        this.customerService = customerService;
        this.parcelService = parcelService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
        this.handler = handler;
    }



    @GetMapping
    public List<Customer> getAllCustomers(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return customerService.getAllCustomers(user);
        } catch (Exception e) {
            handler.handleException(e);
        }
        return null;
    }

    @GetMapping("/{id}")
    public Customer getCustomerById(@PathVariable int id,
                                    @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return customerService.getCustomerById(id, user);

        } catch (Exception e) {
            handler.handleException(e);
        }
        return null;
    }

    @GetMapping("/search")
    public List<Customer> findCustomerWithParameters(@RequestParam(required = false) String first_name,
                                                     @RequestParam(required = false) String last_name,
                                                     @RequestParam(required = false) String email,
                                                     @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return customerService.findCustomerWithParameters(first_name, last_name, email, user);
        } catch (Exception e) {
            handler.handleException(e);
        }
        return null;

    }

    @GetMapping("/{id}/parcels")
    public List<Parcel> getCustomersParcels(@PathVariable int id,
                                            @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Customer customer = customerService.getCustomerById(id, user);
            return parcelService.getCustomerParcels(customer, user);
        } catch (Exception e) {
            handler.handleException(e);
        }
        return null;
    }

    @GetMapping("/find")
    public List<Customer> findByKeyword(@RequestParam String keyword, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return customerService.findByKeyword(keyword, user);
        } catch (Exception e) {
            handler.handleException(e);
        }
        return null;
    }

    @GetMapping("/amount")
    public int getCustomersAmount() {
        try {
            return customerService.getCustomerAmount();
        } catch (Exception e) {
            handler.handleException(e);
        }
        return 0;
    }


    @PostMapping
    public Customer createCustomer(@Valid @RequestBody CustomerDto newCustomer) {
        try {
            Customer customer = mapper.customerDtoToCustomer(newCustomer);
            return customerService.createCustomer(customer);

        } catch (Exception e) {
            handler.handleException(e);
        }
        return null;
    }


    @PutMapping("/{id}")
    public Customer updateCustomer(@PathVariable int id,
                                   @RequestBody @Valid CustomerDto customerDto,
                                   @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Customer customer = customerService.getCustomerById(id, user);
            Customer updateCustomer = mapper.customerDtoToCustomer(customerDto, id);
            return customerService.updateCustomer(updateCustomer, customer, user);
        } catch (Exception e) {
            handler.handleException(e);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public Customer deleteCustomerById(@PathVariable int id,
                                       @RequestHeader HttpHeaders headers) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Customer customer = customerService.getCustomerById(id, user);
            return customerService.deleteCustomerById(id, user);

        } catch (Exception e) {
            handler.handleException(e);
        }
        return null;

    }
}
