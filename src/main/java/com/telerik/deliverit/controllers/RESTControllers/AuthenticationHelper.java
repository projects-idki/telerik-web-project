package com.telerik.deliverit.controllers.RESTControllers;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.exceptions.UnauthorizedOperationException;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.utils.UserRole;
import com.telerik.deliverit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;


@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_USERNAME = "username";
    public static final String AUTHORIZATION_PASSWORD = "password";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) throws EntityNotFoundException {
        if (!headers.containsKey(AUTHORIZATION_USERNAME) || !headers.containsKey(AUTHORIZATION_PASSWORD)) {
            throw new UnauthorizedOperationException("The request status requires authentication");
        }

        String username = headers.getFirst(AUTHORIZATION_USERNAME);
        String password = headers.getFirst(AUTHORIZATION_PASSWORD);
        return userService.tryGetUser(username, password);

    }

}
