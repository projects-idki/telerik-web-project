package com.telerik.deliverit.controllers.MVCControllers;

import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.EmployeeDto;
import com.telerik.deliverit.services.contracts.EmployeeService;
import com.telerik.deliverit.services.contracts.UserService;
import com.telerik.deliverit.services.mapper.EmployeeModelMapper;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/employee")
public class EmployeeMVCController {

    private final UserService userService;
    private final ValidationHelper validationHelper;
    private final EmployeeService employeeService;
    private final EmployeeModelMapper employeeModelMapper;

    @Autowired
    public EmployeeMVCController(UserService userService, ValidationHelper validationHelper, EmployeeService employeeService, EmployeeModelMapper employeeModelMapper) {
        this.userService = userService;
        this.validationHelper = validationHelper;
        this.employeeService = employeeService;
        this.employeeModelMapper = employeeModelMapper;
    }


    @GetMapping("/{id}")
    public String getEmployeePage(@PathVariable int id, HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        Employee employee = employeeService.getEmployeeById(id);

        if (validationHelper.isUserEmployee(user)) {
            try {

                EmployeeDto employeeDto = employeeModelMapper.employeeToDto(employee);
                model.addAttribute("employeeDto", employeeDto);
                model.addAttribute("user", userService.getEmployee(user));
                return "employeePage";
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return "error-page";
            }
        }
        return "redirect:/";
    }

    @PostMapping("/{id}")
    public String updateEmployee(@PathVariable int id,
                                 HttpSession session,
                                 Model model,
                                 @Valid @ModelAttribute("employeeDto") EmployeeDto employeeDto,
                                 BindingResult errors) {

        User user = (User) session.getAttribute("currentUser");
        Employee employee = userService.getEmployee(user);

        if (validationHelper.isUserEmployee(user)) {
            try {
                model.addAttribute("user", userService.getEmployee(user));

                if (errors.hasErrors()) {
                    return "employeePage";
                }

                Employee employeeToUpdate = employeeModelMapper.employeeDtoToEmployee(employeeDto, id);
                employeeService.updateEmployee(employeeToUpdate, employee, user);
                session.setAttribute("currentUser", employeeToUpdate.getUser());

                return "redirect:/employee/" + employeeToUpdate.getId();
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return getEmployeePage(id, session, model);
            }
        }
        return "redirect:/";
    }
}
