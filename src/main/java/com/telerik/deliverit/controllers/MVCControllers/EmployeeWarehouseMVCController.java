package com.telerik.deliverit.controllers.MVCControllers;

import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.models.dtomodels.WarehouseDto;
import com.telerik.deliverit.services.contracts.*;
import com.telerik.deliverit.services.mapper.WarehouseModelMapper;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/warehouses")
public class EmployeeWarehouseMVCController {

    private final WarehouseService warehouseService;
    private final ValidationHelper validationHelper;
    private final ShipmentService shipmentService;
    private final ParcelService parcelService;
    private final CustomerService customerService;
    private final UserService userService;
    private final WarehouseModelMapper warehouseModelMapper;

    public EmployeeWarehouseMVCController(WarehouseService warehouseService, ValidationHelper validationHelper, ShipmentService shipmentService, ParcelService parcelService, CustomerService customerService, UserService userService, WarehouseModelMapper warehouseModelMapper) {
        this.warehouseService = warehouseService;
        this.validationHelper = validationHelper;
        this.shipmentService = shipmentService;
        this.parcelService = parcelService;
        this.customerService = customerService;
        this.userService = userService;
        this.warehouseModelMapper = warehouseModelMapper;
    }

    @ModelAttribute("warehouseAmount")
        public int getAmountWarehouses(){
        return warehouseService.getAll().size();
    }
    @ModelAttribute("shipmentsAmount")
        public int getAmountShipments(){
        return shipmentService.getAmount();
    }
    @ModelAttribute("customersAmount")
        public int getAmountCustomers(){
        return customerService.getCustomerAmount();
    }
    @ModelAttribute("parcelsAmount")
        public int getAmountParcels(){
        return parcelService.getAmount();
    }

    @GetMapping
    public String getEmployeeWarehouse(Model model, HttpSession session){
        User user = (User) session.getAttribute("currentUser");
        if(validationHelper.isUserEmployee(user)) {
            model.addAttribute("user",userService.getEmployee(user));
            model.addAttribute("warehouses", warehouseService.getAll());
            model.addAttribute("customers", customerService.getCustomerAmount());
            model.addAttribute("parcels", parcelService.getAmount());
            model.addAttribute("shipments", shipmentService.getAmount());
            return "warehouses";
        }
        return "redirect:/";
    }

    @GetMapping("/{id}")
    public String getWarehouseById(@PathVariable int id,HttpSession session, Model model){
        User user = (User) session.getAttribute("currentUser");
        try{
            if(validationHelper.isUserEmployee(user)){
                Warehouse warehouse = warehouseService.getWarehouseById(id);
                WarehouseDto warehouseDto = warehouseModelMapper.toDto(warehouse);
                model.addAttribute("warehouseId",warehouse.getId());
                model.addAttribute("warehouseDto",warehouseDto);
                List<Shipment> shipmentList = shipmentService.getAllShipmentsByWarehouse(id,user);
                model.addAttribute("shipments",shipmentList);
                model.addAttribute("user",userService.getEmployee(user));
                return "warehouse";
            }
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }

    @PostMapping("/{id}")
    public String updateWarehouseById(@PathVariable int id,
                                      HttpSession session,
                                      @Valid @ModelAttribute("warehouseDto") WarehouseDto warehouseDto,
                                      BindingResult error,
                                      Model model){
        User user = (User) session.getAttribute("currentUser");
        if(validationHelper.isUserEmployee(user)){
            model.addAttribute("user",userService.getEmployee(user));
            if(error.hasErrors()){
                return "warehouse";
            }
            try{
                Warehouse warehouse = warehouseModelMapper.fromDto(warehouseDto,id);
                warehouseService.updateWarehouse(warehouse,user);
                return "warehouse";
            } catch (RuntimeException e){
                model.addAttribute("error",e.getMessage());
                return "error-page";
            }
        }
        return "redirect:/";
    }


    @GetMapping("delete/{id}")
    public String deleteEmployeeWarehouse(@PathVariable int id, HttpSession session,Model model){
        User user= (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                warehouseService.deleteWarehouse(id, user);
                return "redirect:/warehouses";
            }
        } catch (RuntimeException e){
            model.addAttribute("error",e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }

    @GetMapping("/create")
    public String getCreateWarehousePage(HttpSession session, Model model){
        User user = (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                model.addAttribute("warehouseDto", new WarehouseDto());
                model.addAttribute("user",userService.getEmployee(user));
                return "warehouse-create";

            }
        }catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }

    @PostMapping("/create")
    public String createWarehouse(@Valid @ModelAttribute("warehouseDto") WarehouseDto warehouseDto,
                                  BindingResult errors,
                                  HttpSession session,
                                  Model model){
        User user = (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                model.addAttribute("user",userService.getEmployee(user));
                if(errors.hasErrors()){
                    return "warehouse-create";
                }
                Warehouse warehouse = warehouseModelMapper.fromDto(warehouseDto);
                warehouseService.createWarehouse(warehouse,user);
                return "redirect:/";
            }
        } catch (RuntimeException e){
            model.addAttribute("error",e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }
}
