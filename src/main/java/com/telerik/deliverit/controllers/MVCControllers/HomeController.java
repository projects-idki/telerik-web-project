package com.telerik.deliverit.controllers.MVCControllers;

import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.CustomerDto;
import com.telerik.deliverit.services.contracts.*;
import com.telerik.deliverit.services.mapper.CustomerModelMapper;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class HomeController {
    private final CustomerService customerService;
    private final ParcelService parcelService;
    private final ShipmentService shipmentService;
    private final WarehouseService warehouseService;
    private final CountryService countryService;
    private final CityService cityService;
    private final CustomerModelMapper customerModelMapper;
    private final ValidationHelper validationHelper;
    private final UserService userService;


    public HomeController(CustomerService customerService, ParcelService parcelService, ShipmentService shipmentService, WarehouseService warehouseService, CountryService countryService, CityService cityService, CustomerModelMapper customerModelMapper, ValidationHelper validationHelper, UserService userService) {
        this.customerService = customerService;
        this.parcelService = parcelService;
        this.shipmentService = shipmentService;
        this.warehouseService = warehouseService;
        this.countryService = countryService;
        this.cityService = cityService;
        this.customerModelMapper = customerModelMapper;
        this.validationHelper = validationHelper;
        this.userService = userService;
    }

    @ModelAttribute("warehouseAmount")
    public int getAmountWarehouses(){
        return warehouseService.getAll().size();
    }
    @ModelAttribute("shipmentsAmount")
    public int getAmountShipments(){
        return shipmentService.getAmount();
    }
    @ModelAttribute("customersAmount")
    public int getAmountCustomers(){
        return customerService.getCustomerAmount();
    }
    @ModelAttribute("parcelsAmount")
    public int getAmountParcels(){
        return parcelService.getAmount();
    }


    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        if(validationHelper.isUserEmployee(user)){
            return "redirect:/warehouses";
        }

        if(validationHelper.isUserCustomer(user)){
            Customer customer = userService.getCustomer(user);
            return "redirect:/customer/" + customer.getId();
        }
        model.addAttribute("warehouses", warehouseService.getAll());
        return "index2";
    }

    @GetMapping("/customer/create")
    public String getCustomerCreate(Model model) {
        model.addAttribute("customer", new CustomerDto());
        model.addAttribute("countries", countryService.getAllCountries());
        model.addAttribute("cities", cityService.getAllCities());
        return "customer-create";
    }

    @PostMapping("/customer/create")
    public String createCustomer(@Valid @ModelAttribute("customer") CustomerDto customerDto, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            model.addAttribute("countries",countryService.getAllCountries());
            model.addAttribute("cities",cityService.getAllCities());
            return "customer-create";
        }
        try {
            Customer customer = customerModelMapper.customerDtoToCustomer(customerDto);
            customerService.createCustomer(customer);
        } catch (RuntimeException e){
            model.addAttribute("error",e.getMessage());
            model.addAttribute("countries",countryService.getAllCountries());
            model.addAttribute("cities",cityService.getAllCities());
            return "customer-create";
        }

        return "redirect:/";
    }

}
