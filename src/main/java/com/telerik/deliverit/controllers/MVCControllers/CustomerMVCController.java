package com.telerik.deliverit.controllers.MVCControllers;

import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.CustomerDto;
import com.telerik.deliverit.models.dtomodels.SearchCustomerDto;
import com.telerik.deliverit.services.contracts.*;
import com.telerik.deliverit.services.mapper.CustomerModelMapper;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerMVCController {

    private final UserService userService;
    private final CustomerService customerService;
    private final ValidationHelper validationHelper;
    private final ParcelService parcelService;
    private final CustomerModelMapper customerModelMapper;
    private final ShipmentService shipmentService;
    private final CityService cityService;
    private final CountryService countryService;

    public CustomerMVCController(UserService userService, CustomerService customerService, ValidationHelper validationHelper, ParcelService parcelService, CustomerModelMapper customerModelMapper, ShipmentService shipmentService, CityService cityService, CountryService countryService) {
        this.userService = userService;
        this.customerService = customerService;
        this.validationHelper = validationHelper;
        this.parcelService = parcelService;
        this.customerModelMapper = customerModelMapper;
        this.shipmentService = shipmentService;
        this.cityService = cityService;
        this.countryService = countryService;
    }

    @GetMapping("/{id}")
    public String getCustomerPage(@PathVariable int id, HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        Customer customer = customerService.getCustomerById(id);
        if (validationHelper.isCustomerAuthorised(user, customer)) {
            try {
                CustomerDto customerDto = customerModelMapper.customerToDto(customer);
                model.addAttribute("customer", customerDto);
                model.addAttribute("cities",cityService.getAllCities());
                model.addAttribute("countries",countryService.getAllCountries());
                List<Parcel> parcelList = new ArrayList<>();
                parcelList.addAll(parcelService.getCustomerParcels(customer,user));
                model.addAttribute("parcels",parcelList);
                return "customerPage";
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return "error-page";
            }
        }
        return "redirect:/";
    }

    @PostMapping("/{id}")
    public String updateCustomer(@PathVariable int id,
                                 HttpSession session,
                                 Model model,
                                 @Valid @ModelAttribute("customer") CustomerDto customerDto,
                                 BindingResult errors){
        User user = (User) session.getAttribute("currentUser");
        Customer customer = userService.getCustomer(user);
        if(validationHelper.isCustomerAuthorised(user,customer)){
            try{
                model.addAttribute("user",userService.getCustomer(user));
                model.addAttribute("cities",cityService.getAllCities());
                model.addAttribute("countries",countryService.getAllCountries());
                List<Parcel> parcelList = new ArrayList<>();
                parcelList.addAll(parcelService.getCustomerParcels(customer,user));
                model.addAttribute("parcels",parcelList);
                if(errors.hasErrors()){
                    return "customerPage";
                }
                Customer customerUpdate = customerModelMapper.customerDtoToCustomer(customerDto,id);
                customerService.updateCustomer(customerUpdate,customer,user);
                session.setAttribute("currentUser",customerUpdate.getUser());
                return "redirect:/customer/" + customerUpdate.getId();
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return getCustomerPage(id,session,model);
            }
        }
        return "redirect:/";
    }


}
