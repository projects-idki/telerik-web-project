package com.telerik.deliverit.controllers.MVCControllers;

import com.telerik.deliverit.models.*;
import com.telerik.deliverit.models.dtomodels.CustomerDtoWithoutCredentials;
import com.telerik.deliverit.models.dtomodels.ParcelDto;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoCreate;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoUpdate;
import com.telerik.deliverit.services.contracts.*;
import com.telerik.deliverit.services.mapper.ParcelModelMapper;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/parcels")
public class EmployeeParcelsMVCController {
    private final WarehouseService warehouseService;
    private final ValidationHelper validationHelper;
    private final ShipmentService shipmentService;
    private final ParcelService parcelService;
    private final CustomerService customerService;
    private final UserService userService;
    private final ParcelModelMapper parcelModelMapper;
    private final ParcelCategoryService parcelCategoryService;

    public EmployeeParcelsMVCController(WarehouseService warehouseService, ValidationHelper validationHelper, ShipmentService shipmentService, ParcelService parcelService, CustomerService customerService, UserService userService, ParcelModelMapper parcelModelMapper, ParcelCategoryService parcelCategoryService) {
        this.warehouseService = warehouseService;
        this.validationHelper = validationHelper;
        this.shipmentService = shipmentService;
        this.parcelService = parcelService;
        this.customerService = customerService;
        this.userService = userService;
        this.parcelModelMapper = parcelModelMapper;
        this.parcelCategoryService = parcelCategoryService;
    }

    @ModelAttribute("warehouseAmount")
    public int getAmountWarehouses() {
        return warehouseService.getAll().size();
    }

    @ModelAttribute("shipmentsAmount")
    public int getAmountShipments() {
        return shipmentService.getAmount();
    }

    @ModelAttribute("customersAmount")
    public int getAmountCustomers() {
        return customerService.getCustomerAmount();
    }

    @ModelAttribute("parcelsAmount")
    public int getAmountParcels() {
        return parcelService.getAmount();
    }

    @GetMapping
    public String getEmployeeParcels(Model model, HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {
            model.addAttribute("user", userService.getEmployee(user));
            model.addAttribute("parcels", parcelService.getAllParcels(user));
            model.addAttribute("warehouseList", warehouseService.getAll());
            model.addAttribute("categoryList", parcelCategoryService.getAllCategories());
            return "parcels";
        }
        return "redirect:/";
    }


    @GetMapping("/{id}")
    public String getParcelById(@PathVariable int id, HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                Parcel parcel = parcelService.getParcelById(id);
                ParcelDto parcelDto = parcelModelMapper.parcelToDto(parcel);
                model.addAttribute("parcelDto", parcelDto);
                model.addAttribute("user", userService.getEmployee(user));
                model.addAttribute("customerList", customerService.getAllCustomers(user));
                model.addAttribute("warehouseList", warehouseService.getAll());
                model.addAttribute("categoryList", parcelCategoryService.getAllCategories());

                return "parcel";
            }
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }

    @PostMapping("/{id}")
    public String updateParcelById(@PathVariable int id,
                                   HttpSession session,
                                   @Valid @ModelAttribute("parcelDto") ParcelDto parcelDto,
                                   BindingResult error,
                                   Model model) {

        User user = (User) session.getAttribute("currentUser");

        if (validationHelper.isUserEmployee(user)) {

            model.addAttribute("user", userService.getEmployee(user));
            model.addAttribute("customerList", customerService.getAllCustomers(user));
            model.addAttribute("warehouseList", warehouseService.getAll());
            model.addAttribute("categoryList", parcelCategoryService.getAllCategories());

            if (error.hasErrors()) {
                return "parcel";
            }

            try {
                Parcel parcel = parcelModelMapper.parcelDtoToParcel(parcelDto, id);
                parcelService.updateParcel(parcel, user);

                return "redirect:/parcels/" + id;
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return getParcelById(id, session, model);
            }
        }
        return "redirect:/";
    }


    @GetMapping("delete/{id}")
    public String deleteEmployeeParcel(@PathVariable int id, HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                Parcel parcelToDelete = parcelService.getParcelById(id);
                parcelService.deleteParcel(parcelToDelete, user);
                return "redirect:/parcels";
            }
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }

    @GetMapping("/sort/weight")
    public String sortParcelsByWeight(HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {
            model.addAttribute("user", userService.getEmployee(user));
            model.addAttribute("parcels", parcelService.sortParcels(Optional.of(true), Optional.empty(), user));
            return "parcels";

        }
        return "redirect:/";
    }

    @GetMapping("/create")
    public String getCreateParcelPage(HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {
            try {
                ParcelDto parcelDto = new ParcelDto();
                model.addAttribute("parcelDto", parcelDto);
                model.addAttribute("user", userService.getEmployee(user));
                model.addAttribute("customerList", customerService.getAllCustomers(user));
                model.addAttribute("warehouseList", warehouseService.getAll());
                model.addAttribute("categoryList", parcelCategoryService.getAllCategories());

                return "parcel-create";
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return "error-page";
            }
        }
        return "redirect:/";
    }

    @PostMapping("/create")
    public String createParcel(@Valid @ModelAttribute("parcelDto") ParcelDto parcelDto,
                               BindingResult errors,
                               HttpSession session,
                               Model model) {
        User user = (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                model.addAttribute("user", userService.getEmployee(user));
                model.addAttribute("customerList", customerService.getAllCustomers(user));
                model.addAttribute("warehouseList", warehouseService.getAll());
                model.addAttribute("categoryList", parcelCategoryService.getAllCategories());
                if (errors.hasErrors()) {
                    return "parcel-create";
                }

                Parcel parcel = parcelModelMapper.parcelDtoToParcel(parcelDto);
                parcelService.createParcel(parcel, user);

                return "redirect:/parcels/" + parcel.getId();

            }
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }

    @PostMapping("/search")
    public String searchWithParameters(@RequestParam(value = "warehouseId", required = false) Optional<Integer> warehouseId,
                                       @RequestParam(value = "categoryId", required = false) Optional<Integer> categoryId,
                                       @RequestParam(value = "minWeight", required = false) Optional<Double> minWeight,
                                       @RequestParam(value = "maxWeight", required = false) Optional<Double> maxWeight,
                                       @RequestParam(value = "customerId", required = false) Optional<Integer> customerId,
                                       HttpSession session,
                                       Model model
    ) {

        User user = (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                if (warehouseId.isEmpty()) {
                    warehouseId = Optional.empty();
                }
                if (categoryId.isEmpty()) {
                    categoryId = Optional.empty();
                }
                if (maxWeight.isEmpty()) {
                    maxWeight = Optional.empty();
                }
                if (minWeight.isEmpty()) {
                    minWeight = Optional.empty();
                }
                if (customerId.isEmpty()) {
                    customerId = Optional.empty();
                }
                List<Parcel> parcelList = parcelService.getParcelsWithParameters(minWeight, maxWeight, customerId, warehouseId, categoryId, user);
                model.addAttribute("parcels", parcelList);
                model.addAttribute("user", userService.getEmployee(user));
                model.addAttribute("warehouseList", warehouseService.getAll());
                model.addAttribute("categoryList", parcelCategoryService.getAllCategories());

                return "parcels";
            }
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }

        return "redirect:/";
    }
}
