package com.telerik.deliverit.controllers.MVCControllers;

import com.telerik.deliverit.models.*;
import com.telerik.deliverit.models.dtomodels.CustomerDtoWithoutCredentials;
import com.telerik.deliverit.models.dtomodels.SearchCustomerDto;
import com.telerik.deliverit.models.dtomodels.WarehouseDto;
import com.telerik.deliverit.services.contracts.*;
import com.telerik.deliverit.services.mapper.CustomerModelMapper;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/customers")
public class EmployeeCustomersMVCController {

    private final WarehouseService warehouseService;
    private final ValidationHelper validationHelper;
    private final ShipmentService shipmentService;
    private final ParcelService parcelService;
    private final CustomerService customerService;
    private final UserService userService;
    private final CustomerModelMapper customerModelMapper;
    private final CountryService countryService;
    private final CityService cityService;

    public EmployeeCustomersMVCController(WarehouseService warehouseService, ValidationHelper validationHelper, ShipmentService shipmentService, ParcelService parcelService, CustomerService customerService, UserService userService, CustomerModelMapper customerModelMapper, CountryService countryService, CityService cityService) {
        this.warehouseService = warehouseService;
        this.validationHelper = validationHelper;
        this.shipmentService = shipmentService;
        this.parcelService = parcelService;
        this.customerService = customerService;
        this.userService = userService;
        this.customerModelMapper = customerModelMapper;
        this.countryService = countryService;
        this.cityService = cityService;
    }

    @ModelAttribute("warehouseAmount")
    public int getAmountWarehouses() {
        return warehouseService.getAll().size();
    }

    @ModelAttribute("shipmentsAmount")
    public int getAmountShipments() {
        return shipmentService.getAmount();
    }

    @ModelAttribute("customersAmount")
    public int getAmountCustomers() {
        return customerService.getCustomerAmount();
    }

    @ModelAttribute("parcelsAmount")
    public int getAmountParcels() {
        return parcelService.getAmount();
    }

    @GetMapping
    public String getEmployeeCustomers(Model model, HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {
            model.addAttribute("user", userService.getEmployee(user));
            model.addAttribute("customers", customerService.getAllCustomers(user));
            return "customers";
        }
        return "redirect:/";
    }


    @GetMapping("/{id}")
    public String getCustomerById(@PathVariable int id, HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                Customer customer = customerService.getCustomerById(id);
                CustomerDtoWithoutCredentials customerDto = customerModelMapper.toDto(customer);
                model.addAttribute("customerDto", customerDto);
                model.addAttribute("user", userService.getEmployee(user));
                List<Parcel> parcelList = parcelService.getCustomerParcels(customer,user);
                model.addAttribute("parcels", parcelList);
                model.addAttribute("countriesList", countryService.getAllCountries());
                model.addAttribute("cityList", cityService.getAllCities());
                return "customer";
            }
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }

    @PostMapping("/{id}")
    public String updateCustomerById(@PathVariable int id,
                                     HttpSession session,
                                     @Valid @ModelAttribute("customerDto") CustomerDtoWithoutCredentials customerDto,
                                     BindingResult error,
                                     Model model) {

        User user = (User) session.getAttribute("currentUser");


        if (validationHelper.isUserEmployee(user)) {

            model.addAttribute("user", userService.getEmployee(user));
            model.addAttribute("countriesList", countryService.getAllCountries());
            model.addAttribute("cityList", cityService.getAllCities());
            Customer customer = customerService.getCustomerById(id);
            List<Parcel> parcelList = parcelService.getCustomerParcels(customer,user);
            model.addAttribute("parcels", parcelList);

            if (error.hasErrors()) {
                return "customer";
            }

            try {
                Customer updateCustomer = customerModelMapper.customerDtoToCustomer(customerDto, id);
                customerService.updateCustomer(updateCustomer, customer, user);

                return "redirect:/customers/" + id;
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return "customer";
            }
        }
        return "redirect:/";
    }


    @GetMapping("delete/{id}")
    public String deleteEmployeeCustomer(@PathVariable int id, HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                customerService.deleteCustomerById(id, user);
                return "redirect:/customers";
            }
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }

    @PostMapping("/search")
    public String searchWithParameters(@RequestParam(value = "firstName" , required = false) String firstName,
                                       @RequestParam(value = "lastName", required = false) String lastName,
                                       @RequestParam(value = "email", required = false) String email,
                                       HttpSession session,
                                       Model model
                                       ){

        User user = (User) session.getAttribute("currentUser");
        try{
            if(validationHelper.isUserEmployee(user)){
                if(firstName.isBlank()){
                    firstName = null;
                }
                if(lastName.isBlank()){
                    lastName = null;
                }
                if(email.isBlank()){
                    email = null;
                }
                List<Customer> customerList = customerService.findCustomerWithParameters(firstName,lastName,email,user);
                model.addAttribute("customers",customerList);
                model.addAttribute("user",userService.getEmployee(user));
                return "customers";
            }
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }

        return "redirect:/";
    }

    @PostMapping("/find")
    public String findByKeyWord(@RequestParam(value = "word" , required = false) String word,
                                HttpSession session,
                                Model model){
        User user = (User) session.getAttribute("currentUser");
        try{
            if(validationHelper.isUserEmployee(user)){
                List<Customer> customerList = customerService.findByKeyword(word,user);
                model.addAttribute("customers",customerList);
                model.addAttribute("user",userService.getEmployee(user));
                return "customers";
            }
        }catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }

        return "redirect:/";
    }
}
