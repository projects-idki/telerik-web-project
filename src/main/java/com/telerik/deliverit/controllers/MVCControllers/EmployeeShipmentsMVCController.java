package com.telerik.deliverit.controllers.MVCControllers;

import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoAddAndRemoveParcel;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoCreate;
import com.telerik.deliverit.models.dtomodels.ShipmentDtoUpdate;
import com.telerik.deliverit.services.contracts.*;
import com.telerik.deliverit.services.mapper.ShipmentModelMapper;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/shipments")
public class EmployeeShipmentsMVCController {

    private final WarehouseService warehouseService;
    private final ValidationHelper validationHelper;
    private final ShipmentService shipmentService;
    private final ParcelService parcelService;
    private final CustomerService customerService;
    private final UserService userService;
    private final ShipmentModelMapper shipmentModelMapper;

    public EmployeeShipmentsMVCController(WarehouseService warehouseService, ValidationHelper validationHelper, ShipmentService shipmentService, ParcelService parcelService, CustomerService customerService, UserService userService, ShipmentModelMapper shipmentModelMapper) {
        this.warehouseService = warehouseService;
        this.validationHelper = validationHelper;
        this.shipmentService = shipmentService;
        this.parcelService = parcelService;
        this.customerService = customerService;
        this.userService = userService;
        this.shipmentModelMapper = shipmentModelMapper;
    }

    @ModelAttribute("warehouseAmount")
    public int getAmountWarehouses() {
        return warehouseService.getAll().size();
    }

    @ModelAttribute("shipmentsAmount")
    public int getAmountShipments() {
        return shipmentService.getAmount();
    }

    @ModelAttribute("customersAmount")
    public int getAmountCustomers() {
        return customerService.getCustomerAmount();
    }

    @ModelAttribute("parcelsAmount")
    public int getAmountParcels() {
        return parcelService.getAmount();
    }

    @GetMapping("/{id}")
    public String getShipmentById(@PathVariable int id, HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        try {
            if (validationHelper.isUserEmployee(user)) {
                Shipment shipment = shipmentService.getShipmentById(id);
                ShipmentDtoUpdate shipmentDtoUpdate = shipmentModelMapper.shipmentToDto(shipment);
                ShipmentDtoAddAndRemoveParcel parcelToAddOrRemove = new ShipmentDtoAddAndRemoveParcel();
                model.addAttribute("parcelToAddOrRemove", parcelToAddOrRemove);
                model.addAttribute("shipmentDtoUpdate", shipmentDtoUpdate);
                model.addAttribute("user", userService.getEmployee(user));
                model.addAttribute("parcelList", shipment.getParcelSet());
                model.addAttribute("status", shipment.getStatus());
                model.addAttribute("address", shipment.getWarehouseTo().getAddress());
                model.addAttribute("possibleParcels", parcelService.getParcelsWithNoShipment(shipment.getWarehouseTo(), user));
                model.addAttribute("shipmentId", id);
                model.addAttribute("warehouses",warehouseService.getAll());
                return "shipment";
            }
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "error-page";
        }
        return "redirect:/";
    }

    @PostMapping("/{id}")
    public String updateShipment(@PathVariable int id,
                                 HttpSession session,
                                 @Valid @ModelAttribute("shipmentDtoUpdate") ShipmentDtoUpdate shipmentDtoUpdate,
                                 BindingResult errors,
                                 Model model) {
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {
            model.addAttribute("user", userService.getEmployee(user));
            if (errors.hasErrors()) {
                return "shipment";
            }
            try {
                Shipment shipment = shipmentModelMapper.shipmentDtoToShipment(shipmentDtoUpdate, id);
                shipmentService.updateShipment(shipment, user);
                return "redirect:/shipments/" + id;
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return getShipmentById(id,session,model);
            }
        }
        return "redirect:/";
    }

    @PostMapping("/{id}/add")
    public String addParcelToShipment(@PathVariable int id,
                                      HttpSession session,
                                      @Valid @ModelAttribute("parcelToAddOrRemove") ShipmentDtoAddAndRemoveParcel parcel,
                                      BindingResult errors,
                                      Model model) {
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {

            try {
                Shipment shipment = shipmentService.getShipmentById(id);
                Parcel parcel1 = parcelService.getParcelById(parcel.getParcelId());
                shipmentService.addParcelToShipment(shipment, parcel1, user);
                return "redirect:/shipments/" + id;
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return "error-page";
            }
        }
        return "redirect:/";
    }


    @GetMapping
    public String getEmployeeShipments(Model model, HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {
            model.addAttribute("user", userService.getEmployee(user));
            model.addAttribute("warehouses", warehouseService.getAll());
            model.addAttribute("customers", customerService.getCustomerAmount());
            model.addAttribute("parcels", parcelService.getAmount());
            model.addAttribute("shipments", shipmentService.getAllShipments(user));
            return "shipments";
        }
        return "redirect:/";
    }

    @GetMapping("/parcel/remove/{id}")
    public String removeParcelFromShipment(@PathVariable int id, HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {
            try {
                Parcel parcel = parcelService.getParcelById(id);
                Shipment shipment = parcel.getShipment();
                shipmentService.removeParcelFromShipment(shipment, parcel, user);
                return "redirect:/shipments/" + shipment.getId();
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return "error-page";
            }
        }
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String deleteShipment(@PathVariable int id, HttpSession session, Model model) {
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {
            try {
                shipmentService.deleteShipment(id, user);
                return "redirect:/shipments";
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return "error-page";
            }

        }
        return "redirect:/";
    }

    @GetMapping("/create")
    public String getCreateShipmentPage(HttpSession session, Model model){
        User user = (User) session.getAttribute("currentUser");
        if (validationHelper.isUserEmployee(user)) {
            try {
                ShipmentDtoUpdate shipmentDto = new ShipmentDtoUpdate();
                List<Warehouse> warehouseList = warehouseService.getAll();
                model.addAttribute("shipmentDto",shipmentDto);
                model.addAttribute("warehouses",warehouseList);
                model.addAttribute("user",userService.getEmployee(user));
                return "shipment-create";
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return "error-page";
            }
        }
        return "redirect:/";
    }

    @PostMapping("/create")
    public String createShipment(@Valid @ModelAttribute("shipmentDto") ShipmentDtoUpdate shipmentDto,
                                 HttpSession session,
                                 BindingResult errors,
                                 Model model){
        User user = (User) session.getAttribute("currentUser");
        if(validationHelper.isUserEmployee(user)){
            if (errors.hasErrors()) {
                return "shipment-create";
            }

            model.addAttribute("user", userService.getEmployee(user));
            try{
                ShipmentDtoCreate shipmentDtoCreate = new ShipmentDtoCreate();
                shipmentDtoCreate.setWarehouseToId(shipmentDto.getWarehouseToId());
                Shipment shipment = shipmentModelMapper.shipmentDtoToShipment(shipmentDtoCreate);
                shipment.setDepartureDate(shipmentDto.getDepartureDate());
                shipment.setArrivalDate(shipmentDto.getArrivalDate());
                shipmentService.createShipment(shipment,user);
                return "redirect:/shipments/" + shipment.getId();
            } catch (RuntimeException e) {
                model.addAttribute("error", e.getMessage());
                return "error-page";
            }
        }
        return "redirect:/";
    }
}
