package com.telerik.deliverit.controllers.MVCControllers;

import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.models.dtomodels.UserDto;
import com.telerik.deliverit.services.contracts.UserService;
import com.telerik.deliverit.services.mapper.UserModelMapper;
import com.telerik.deliverit.services.utils.ValidationHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/authenticate")
public class AuthenticationController {

    private final UserService userService;
    private final UserModelMapper userModelMapper;
    private final ValidationHelper validationHelper;


    public AuthenticationController(UserService userService, UserModelMapper userModelMapper, ValidationHelper validationHelper) {
        this.userService = userService;
        this.userModelMapper = userModelMapper;
        this.validationHelper = validationHelper;
    }

    @GetMapping
    private String loginPage(Model model) {
        model.addAttribute("user", new UserDto());

        return "loginPage";
    }

    @PostMapping
    private String loginPage(@Valid @ModelAttribute("user") UserDto userDto, BindingResult errors, Model model, HttpSession session) {
        if (errors.hasErrors()) {
            return "loginPage";
        }

        try {
            User user = userService.tryGetUser(userDto.getUsername(), userDto.getPassword());
            session.setAttribute("currentUser", user);
            if (validationHelper.isUserEmployee(user)) {
                return "redirect:/warehouses";
            }
            if (validationHelper.isUserCustomer(user)){
                Customer customer = userService.getCustomer(user);
                return "redirect:/customer/" + customer.getId();
            }
            return "redirect:/";
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "loginPage";
        }
    }

    @GetMapping("/logout")
    private String logout(HttpSession session) {
            session.removeAttribute("currentUser");
        return "redirect:/";
    }
}
