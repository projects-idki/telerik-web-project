package com.telerik.deliverit.repositories;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.City;
import com.telerik.deliverit.repositories.contracts.CityRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CityRepositoryImpl implements CityRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CityRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<City> getAllCities() {
        try (Session session = sessionFactory.openSession()) {
            Query<City> query = session.createQuery("from City ", City.class);
            return query.getResultList();
        }
    }

    @Override
    public City getCityById(int id) {
        try (Session session = sessionFactory.openSession()) {
            City city = session.get(City.class, id);
            if (city == null) {
                throw new EntityNotFoundException("City", id);
            }
            return city;
        }
    }

    @Override
    public City getCityByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<City> query = session.createQuery("from City where name = :name", City.class);
            query.setParameter("name", name);
            List<City> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("City", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public City createCity(City city) {
        try (Session session = sessionFactory.openSession()) {
            session.save(city);
        }
        return city;
    }

    @Override
    public boolean contains(City city) {
        return getAllCities().contains(city);

    }

    @Override
    public City getCity(City city) {
        return getAllCities().stream()
                .filter(city1 -> city1.equals(city))
                .findFirst().orElseThrow(() -> new EntityNotFoundException("City"));
    }

    @Override
    public List<City> getCountryCities(int countryId) {
        try(Session session = sessionFactory.openSession()){
            Query<City> query = session.createQuery("FROM City WHERE country.id = :countryId", City.class);
            query.setParameter("countryId",countryId);
            return query.getResultList();
        }
    }

}
