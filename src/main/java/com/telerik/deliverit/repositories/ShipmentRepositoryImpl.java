package com.telerik.deliverit.repositories;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.repositories.contracts.ShipmentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public class ShipmentRepositoryImpl implements ShipmentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ShipmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Shipment> getAllShipments() {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment ORDER BY arrivalDate", Shipment.class);
            return query.list();
        }
    }

    @Override
    public Shipment getShipmentById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Shipment shipment = session.get(Shipment.class, id);
            if (shipment == null) {
                throw new EntityNotFoundException("Shipment", id);
            }
            return shipment;
        }
    }

    @Override
    public void createShipment(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(shipment);
            session.getTransaction().commit();
        }
    }

    @Override
    public Shipment updateShipment(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(shipment);
            for (Parcel parcel : shipment.getParcelSet()) {
                parcel.setShipment(shipment);
                session.update(parcel);
            }
            session.getTransaction().commit();
        }
        return shipment;
    }

    @Override
    public void deleteShipment(int id) {
        Shipment shipmentToDelete = getShipmentById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            for (Parcel parcel : shipmentToDelete.getParcelSet()) {
                parcel.setShipment(null);
                session.update(parcel);
            }
            session.delete(shipmentToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public int getAmount() {
        return getAllShipments().size();
    }

    @Override
    public void removeParcelFromShipment(Shipment shipment, Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            parcel.setShipment(null);
            session.update(parcel);
            session.update(shipment);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Shipment> getAllShipmentsByCustomer(int customerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment " +
                    "where id in(select shipment.id from Parcel where customer.id = :id)", Shipment.class);
            query.setParameter("id", customerId);

            return query.list();
        }
    }

    @Override
    public List<Shipment> getAllShipmentsByWarehouse(int warehouseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment where warehouseTo.id = :id", Shipment.class);
            query.setParameter("id", warehouseId);
            return query.list();
        }
    }

    @Override
    public Shipment getNextShipmentInWarehouse(Warehouse warehouse) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("FROM Shipment WHERE warehouseTo = :warehouse AND arrivalDate > current_date order by arrivalDate");
            query.setParameter("warehouse", warehouse);
            List<Shipment> shipmentList = query.getResultList();
            if (shipmentList.isEmpty()) {
                throw new EntityNotFoundException("No shipments found in warehouse");
            }
            return shipmentList.get(0);

        }
    }

}
