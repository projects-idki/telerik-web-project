package com.telerik.deliverit.repositories;

import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.repositories.contracts.CustomerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CustomerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Customer> getAllCustomers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("FROM Customer", Customer.class);
            return query.list();
        }
    }

    @Override
    public Customer getCustomerById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Customer customer = session.get(Customer.class, id);
            if (customer == null) {
                throw new EntityNotFoundException("Customer", id);
            }
            return customer;
        }

    }

    @Override
    public void saveCustomer(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(customer);
            session.getTransaction().commit();

        }

    }

    @Override
    public void deleteCustomer(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(customer);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Customer> findCustomersWithParameters(String first_name, String last_name, String email) {
        String endOfQuery = makeEndOfQuery(first_name, last_name, email);
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("FROM Customer WHERE 1=1 " + endOfQuery, Customer.class);
            if (query.list() == null) {
                throw new EntityNotFoundException("Customers");
            }
            return query.list();

        }
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(customer);
            session.getTransaction().commit();
        }
        return customer;
    }

    @Override
    public boolean isEmailExist(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("FROM Customer WHERE email = :email ", Customer.class);
            query.setParameter("email", email);
            List<Customer> customers = query.list();
            return !customers.isEmpty();
        }
    }

    @Override
    public List<Customer> findByKeyword(String keyword) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("from Customer " +
                    "where firstName like :firstName or lastName like :lastName or email like :email", Customer.class);
            query.setParameter("firstName", "%" + keyword + "%");
            query.setParameter("lastName", "%" + keyword + "%");
            query.setParameter("email", "%" + keyword + "%");
            return query.getResultList();
        }
    }

    private String makeEndOfQuery(String first_name, String last_name, String email) {
        StringBuilder sb = new StringBuilder("");
        if (first_name != null) {
            sb.append(String.format("AND first_name = '%s'", first_name));
        }
        if (last_name != null) {
            sb.append(String.format("AND last_name = '%s'", last_name));
        }
        if (email != null) {

            sb.append(String.format("AND email LIKE %s", "'%" + email + "%'"));
        }

        return sb.toString();
    }
}
