package com.telerik.deliverit.repositories;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.repositories.contracts.EmployeeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public EmployeeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Employee> getAllEmployees() {
        try (Session session = sessionFactory.openSession()) {
            Query<Employee> query = session.createQuery("FROM Employee ", Employee.class);
            return query.list();
        }
    }

    @Override
    public Employee getEmployeeById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Employee employee = session.get(Employee.class, id);
            if (employee == null) {
                throw new EntityNotFoundException("Employee", id);
            }
            return employee;
        }
    }

    @Override
    public boolean emailExists(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<Employee> query = session.createQuery("FROM Employee WHERE email = :email", Employee.class);
            query.setParameter("email", email);
            List<Employee> result = query.list();
            return !result.isEmpty();
        }
    }

    @Override
    public void createEmployee(Employee employee) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(employee);
            session.getTransaction().commit();

        }

    }

    @Override
    public Employee updateEmployee(Employee employee) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(employee);
            session.getTransaction().commit();
        }
        return employee;
    }


    @Override
    public void deleteEmployee(int id) {
        Employee employeeToDelete = getEmployeeById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(employeeToDelete);
            session.getTransaction().commit();
        }
    }
}
