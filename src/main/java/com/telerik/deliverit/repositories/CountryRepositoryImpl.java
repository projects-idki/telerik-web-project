package com.telerik.deliverit.repositories;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Country;
import com.telerik.deliverit.repositories.contracts.CountryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Country> getAllCountries() {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country ", Country.class);
            return query.list();
        }
    }

    @Override
    public Country getCountryById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            if (country == null) {
                throw new EntityNotFoundException("Country", id);
            }
            return country;
        }
    }

    @Override
    public Country getCountryByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country where name = :name", Country.class);
            query.setParameter("name", name);
            List<Country> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Country", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public Country createCountry(String  name) {
        Country country = new Country(name);
        try (Session session = sessionFactory.openSession()) {
            session.save(country);
        }
        return country;
    }

    @Override
    public Country updateCountry(Country country) {
        return null;
    }

    @Override
    public Country deleteCountryById(int id) {
        return null;
    }

    @Override
    public Country deleteCountryByName(String name) {
        return null;
    }

}



