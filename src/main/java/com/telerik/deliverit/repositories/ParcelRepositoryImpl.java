package com.telerik.deliverit.repositories;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.repositories.contracts.ParcelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ParcelRepositoryImpl implements ParcelRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Parcel> getCustomerParcels(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("FROM Parcel WHERE customer.id = :id order by shipment.arrivalDate", Parcel.class);
            query.setParameter("id", id);
            List<Parcel> parcelList = query.getResultList();
            query = session.createQuery("FROM Parcel WHERE customer.id = :id AND shipment IS null", Parcel.class);
            query.setParameter("id", id);
            parcelList.addAll(query.getResultList());
            return parcelList;
        }
    }

    @Override
    public List<Parcel> getAllParcels() {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("FROM Parcel", Parcel.class);
            return query.getResultList();
        }
    }

    @Override
    public Parcel createParcel(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(parcel);
            session.getTransaction().commit();
            return parcel;
        }
    }

    @Override
    public Parcel getParcelById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("FROM Parcel WHERE id = :id", Parcel.class);
            query.setParameter("id", id);
            List<Parcel> parcelsList = query.getResultList();
            if (parcelsList.isEmpty()) {
                throw new EntityNotFoundException("Parcel", id);
            }
            return parcelsList.get(0);
        }
    }

    @Override
    public Parcel updateParcel(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(parcel);
            session.getTransaction().commit();
            return parcel;
        }
    }

    @Override
    public Parcel deleteParcel(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(parcel);
            session.getTransaction().commit();
            return parcel;
        }
    }

    @Override
    public int getAmount() {
        return getAllParcels().size();
    }

    @Override
    public List<Parcel> getParcelsWithparameters(Optional<Double> minWeight,
                                                 Optional<Double> maxWeight,
                                                 Optional<Integer> customerId,
                                                 Optional<Integer> warehouseId,
                                                 Optional<Integer> categoryId) {
        String endOfQuery = createParameterQuery(minWeight, maxWeight, customerId, warehouseId, categoryId);
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("FROM Parcel WHERE 1=1" + endOfQuery, Parcel.class);
            return query.getResultList();
        }
    }

    @Override
    public List<Parcel> sortParcels(Optional<Boolean> weight, Optional<Boolean> arrivalDate) {
        String endOfQuery = createSortQuery(weight, arrivalDate);
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("FROM Parcel " + endOfQuery, Parcel.class);
            return query.getResultList();
        }
    }

    @Override
    public List<Parcel> getParcelsWithNoShipment(Warehouse warehouse) {
        try(Session session = sessionFactory.openSession()){
            Query<Parcel> query = session.createQuery("FROM Parcel where shipment=null and warehouse = :warehouseId", Parcel.class);
            query.setParameter("warehouseId",warehouse);
            return query.getResultList();
        }
    }

    private String createSortQuery(Optional<Boolean> weight, Optional<Boolean> arrivalDate) {
        String endOfQuery = " ORDER BY";
        if (arrivalDate.isPresent() && arrivalDate.get()) {
            endOfQuery = endOfQuery + " shipment.arrivalDate";
        }
        if (weight.isPresent() && arrivalDate.isPresent()) {
            endOfQuery = endOfQuery + " ,";
        }
        if (weight.isPresent() && weight.get()) {
            endOfQuery = endOfQuery + " weight";
        }
        return endOfQuery;
    }

    private String createParameterQuery(Optional<Double> minWeight, Optional<Double> maxWeight, Optional<Integer> customerId, Optional<Integer> warehouseId, Optional<Integer> categoryId) {
        String endOfQuery = "";
        if (minWeight.isPresent()) {
            endOfQuery = endOfQuery + " AND weight >" + minWeight.get();
        }
        if (maxWeight.isPresent()) {
            endOfQuery = endOfQuery + " AND weight <" + maxWeight.get();
        }
        if (customerId.isPresent()) {
            endOfQuery = endOfQuery + " AND customer.id = " + customerId.get();
        }
        if (warehouseId.isPresent()) {
            endOfQuery = endOfQuery + " AND warehouse.id = " + warehouseId.get();
        }
        if (categoryId.isPresent()) {
            endOfQuery = endOfQuery + " AND category.id = " + categoryId.get();
        }
        return endOfQuery;
    }
}
