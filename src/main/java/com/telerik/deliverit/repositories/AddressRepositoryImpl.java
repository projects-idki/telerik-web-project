package com.telerik.deliverit.repositories;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.Address;
import com.telerik.deliverit.models.City;
import com.telerik.deliverit.models.Country;
import com.telerik.deliverit.repositories.contracts.AddressRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AddressRepositoryImpl implements AddressRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public AddressRepositoryImpl(SessionFactory sessionFactory) {
       this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Address> getAllAddresses() {
        try (Session session = sessionFactory.openSession()) {
            Query<Address> query = session.createQuery("from Address order by city.country.name", Address.class);
            return query.list();
        }
    }

    @Override
    public Address getAddressById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Address address = session.get(Address.class, id);
            if (address == null) {
                throw new EntityNotFoundException("Address", id);
            }
            return address;
        }
    }

    @Override
    public Address getAddressByStreetName(String streetName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Address> query = session.createQuery("from Address where streetAddress = :streetName", Address.class);
            query.setParameter("streetName", streetName);
            List<Address> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Address", "street name", streetName);
            }
            return result.get(0);
        }
    }

    @Override
    public Address createAddress(Address address) {
        try (Session session = sessionFactory.openSession()) {
            session.save(address);
        }
        return address;
    }

    public boolean existsByStreetName(String streetName){
        try (Session session = sessionFactory.openSession()) {
            Query<Address> query = session.createQuery("from Address where streetAddress = :streetName", Address.class);
            query.setParameter("streetName", streetName);
            List<Address> result = query.list();
            return result.size() != 0;
        }
    }

    @Override
    public boolean contains(Address address) {
        return getAllAddresses().contains(address);
    }

    public Address getAddress(Address address){
        return getAllAddresses().stream()
                .filter(address1 -> address1.equals(address))
                .findFirst().orElseThrow(() -> new EntityNotFoundException("Address"));
    }


}
