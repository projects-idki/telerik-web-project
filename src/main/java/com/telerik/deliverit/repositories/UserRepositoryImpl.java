package com.telerik.deliverit.repositories;

import com.telerik.deliverit.exceptions.DuplicateEntityException;
import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.exceptions.WrongPasswordException;
import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;
import com.telerik.deliverit.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User tryGetUser(String username, String password) {
        User user = getUserByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("User", "username", username);
        }
        if (!user.getPassword().equals(password)) {
            throw new WrongPasswordException();
        }
        return user;

    }

    @Override
    @Transactional
    public User createUser(User user) {
        User newUser = getUserByUsername(user.getUsername());
        if (newUser != null) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    @Transactional
    public User updateUser(User user, User newUser) {
        try (Session session = sessionFactory.openSession()) {
            user.setUsername(newUser.getUsername());
            user.setPassword(newUser.getPassword());
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
        return newUser;
    }


    @Override
    @Transactional
    public User deleteUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery("FROM User ",User.class);
            return query.list();
        }
    }

    @Override
    public Employee getEmployee(User user) {
        try(Session session= sessionFactory.openSession()){
            Query<Employee> query = session.createQuery("FROM Employee where user.username = :username",Employee.class);
            query.setParameter("username",user.getUsername());
            List<Employee> employees = query.getResultList();
            if(employees.isEmpty()){
                throw new EntityNotFoundException("Employee","username",user.getUsername());
            }
            return employees.get(0);
        }
    }

    @Override
    public Customer getCustomer(User user) {
        try(Session session= sessionFactory.openSession()){
            Query<Customer> query = session.createQuery("FROM Customer where user.username = :username",Customer.class);
            query.setParameter("username",user.getUsername());
            List<Customer> customers = query.getResultList();
            if(customers.isEmpty()){
                throw new EntityNotFoundException("Customer","username",user.getUsername());
            }
            return customers.get(0);
        }
    }


    private User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("FROM User WHERE username = :username", User.class);
            query.setParameter("username", username);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User","username",username);
            }
            return query.list().get(0);
        }
    }

    private User getUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(User.class, id);
        }
    }
}
