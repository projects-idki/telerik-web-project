package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.Shipment;
import com.telerik.deliverit.models.Warehouse;
import com.telerik.deliverit.models.utils.Status;

import java.util.List;
import java.util.Optional;

public interface ShipmentRepository {

    List<Shipment> getAllShipments();

    Shipment getShipmentById(int id);

    void createShipment(Shipment shipment);

    Shipment updateShipment(Shipment shipment);

    void deleteShipment(int id);

    int getAmount();

    void removeParcelFromShipment(Shipment shipment, Parcel parcel);

    List<Shipment> getAllShipmentsByCustomer(int customerId);

    List<Shipment> getAllShipmentsByWarehouse(int warehouseId);

    Shipment getNextShipmentInWarehouse(Warehouse warehouse);
}
