package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.Employee;

import java.util.List;

public interface EmployeeRepository {

    List<Employee> getAllEmployees();

    Employee getEmployeeById(int id);

    boolean emailExists(String email);

    void createEmployee(Employee employee);

    Employee updateEmployee(Employee employee);

    void deleteEmployee(int id);
}
