package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.Address;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AddressRepository {

    List<Address> getAllAddresses();

    Address getAddressById(int id);

    Address getAddressByStreetName(String streetName);

    Address createAddress(Address address);

    boolean contains(Address address);

    boolean existsByStreetName(String streetName);

    Address getAddress(Address address);
}


