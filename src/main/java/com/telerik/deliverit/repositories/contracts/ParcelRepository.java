package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.Parcel;
import com.telerik.deliverit.models.Warehouse;

import java.util.List;
import java.util.Optional;

public interface ParcelRepository {

    List<Parcel> getCustomerParcels(int id);

    List<Parcel> getAllParcels();

    Parcel createParcel(Parcel parcel);

    Parcel getParcelById(int id);

    Parcel updateParcel(Parcel parcel);

    Parcel deleteParcel(Parcel parcel);

    int getAmount();

    List<Parcel> getParcelsWithparameters(Optional<Double> minWeight, Optional<Double> maxWeight, Optional<Integer> customerId, Optional<Integer> warehouseId, Optional<Integer> categoryId);

    List<Parcel> sortParcels(Optional<Boolean> weight, Optional<Boolean> arrivalDate);

    List<Parcel> getParcelsWithNoShipment(Warehouse warehoues);
}
