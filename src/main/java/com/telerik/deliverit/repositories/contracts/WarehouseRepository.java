package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.Warehouse;

import java.util.List;

public interface WarehouseRepository {

    List<Warehouse> getAll();

    Warehouse getWarehouseById(int id);

    Warehouse createWarehouse(Warehouse warehouse);

    Warehouse updateWarehouse(Warehouse warehouse);

    void deleteWarehouse(int id);

    //    boolean contains(Warehouse warehouse);
    Warehouse getWarehouse(Warehouse warehouse);
}
