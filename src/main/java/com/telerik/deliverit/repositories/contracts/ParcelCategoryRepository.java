package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.ParcelCategory;

import java.util.List;

public interface ParcelCategoryRepository {

    List<ParcelCategory> getAllCategories();

    ParcelCategory getParcelCategoryById(int id);
}
