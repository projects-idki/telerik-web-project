package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.City;

import java.util.List;

public interface CityRepository {

    List<City> getAllCities();

    City getCityById(int id);

    City getCityByName(String name);

    City createCity(City city);

    boolean contains(City city);

    City getCity(City city);

    List<City> getCountryCities(int countryId);
}
