package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.Country;

import java.util.List;

public interface CountryRepository {

    List<Country> getAllCountries();

    Country getCountryById(int id);

    Country getCountryByName(String name);

    Country createCountry(String name);

    Country updateCountry(Country country);

    Country deleteCountryById(int id);

    Country deleteCountryByName(String name);


}
