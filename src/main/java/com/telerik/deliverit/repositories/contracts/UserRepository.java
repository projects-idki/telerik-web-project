package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.Customer;
import com.telerik.deliverit.models.Employee;
import com.telerik.deliverit.models.User;

import java.util.List;

public interface UserRepository {

    User tryGetUser(String username, String password);

    User createUser(User user);

    User updateUser(User user, User newUser);

    User deleteUser(User user);

    List<User> getAllUsers();


    Employee getEmployee(User user);

    Customer getCustomer(User user);
}
