package com.telerik.deliverit.repositories.contracts;

import com.telerik.deliverit.models.Customer;

import java.util.List;

public interface CustomerRepository {


    List<Customer> getAllCustomers();

    Customer getCustomerById(int id);

    void saveCustomer(Customer customer);

    void deleteCustomer(Customer customerToDelete);

    List<Customer> findCustomersWithParameters(String first_name, String last_name, String email);

    Customer updateCustomer(Customer customer);

    boolean isEmailExist(String email);

    List<Customer> findByKeyword(String keyword);

}
