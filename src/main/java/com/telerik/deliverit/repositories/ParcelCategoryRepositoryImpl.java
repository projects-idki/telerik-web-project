package com.telerik.deliverit.repositories;

import com.telerik.deliverit.exceptions.EntityNotFoundException;
import com.telerik.deliverit.models.ParcelCategory;
import com.telerik.deliverit.repositories.contracts.ParcelCategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ParcelCategoryRepositoryImpl implements ParcelCategoryRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelCategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ParcelCategory> getAllCategories() {
        try(Session session = sessionFactory.openSession()){
            Query<ParcelCategory> query = session.createQuery("FROM ParcelCategory ",ParcelCategory.class);
            return query.getResultList();
        }
    }

    @Override
    public ParcelCategory getParcelCategoryById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<ParcelCategory> query = session.createQuery("FROM ParcelCategory WHERE id = :id", ParcelCategory.class);
            query.setParameter("id",id);
            List<ParcelCategory> list = query.getResultList();
            if(list.isEmpty()){
                throw new EntityNotFoundException("Parcel Category",id);
            }
            return list.get(0);
        }
    }


}
