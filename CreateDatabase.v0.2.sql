create database `deliverIT`;

use `deliverIT`;

create or replace table country
(
	id int auto_increment
		primary key,
	name varchar(20) not null,
	constraint countries_name_uindex
		unique (name)
);

create or replace table city
(
	id int auto_increment
		primary key,
	name varchar(20) not null,
	country_id int not null,
	constraint cities_countries__fk
		foreign key (country_id) references country (id)
);

create or replace table address
(
	id int auto_increment
		primary key,
	street_address text not null,
	city_id int not null,
	constraint addresses_cityes__fk
		foreign key (city_id) references city (id)
);

create or replace table parcel_category
(
	id int auto_increment
		primary key,
	category varchar(40) not null
);

create or replace table warehouse
(
    id int auto_increment
        primary key,
    address_id int not null,
    constraint warehouses_addresses_id_fk
        foreign key (address_id) references address (id)
);

create or replace table shipment
(
	id int auto_increment
		primary key,
	departure_date date null,
	arrival_date date null,
    warehouse_to_id int not null,
    constraint shipment_warehouse_to_fk
        foreign key (warehouse_to_id) references warehouse (id)
);

create or replace table user
(
	id int auto_increment
		primary key,
	username varchar(20) not null,
	password varchar(20) not null,
	role int not null,
	constraint user_username_uindex
		unique (username)
);

create or replace table customer
(
	id int auto_increment
		primary key,
	first_name varchar(20) not null,
	last_name varchar(20) not null,
	user_id int not null,
	address_id int null,
	email varchar(50) not null,
	constraint customer_email_uindex
		unique (user_id),
	constraint customer_email_uindex_2
		unique (email),
	constraint customer_user_id_fk
		foreign key (user_id) references user (id),
	constraint customers_addresses_id_fk
		foreign key (address_id) references address (id)
);

create or replace table employee
(
	id int auto_increment
		primary key,
	first_name varchar(20) null,
	last_name varchar(20) not null,
	user_id int not null,
	email varchar(50) not null,
	constraint employee_email_uindex
		unique (email),
	constraint employee_userInfo_uindex
		unique (user_id),
	constraint employee_user_id_fk
		foreign key (user_id) references user (id)
);

create or replace index user_role_id_fk
	on user (role);



create or replace table parcel
(
    id int auto_increment
        primary key,
    customer_id int not null,
    warehouse_id int not null,
    weight double not null,
    category_id int not null,
    shipment_id int null,
    constraint parcel_customers_id_fk
        foreign key (customer_id) references deliverit.customer (id),
    constraint parcel_parcel_categories_id_fk
        foreign key (category_id) references deliverit.parcel_category (id),
    constraint parcel_shipment_id_fk
        foreign key (shipment_id) references deliverit.shipment (id),
    constraint parcel_warehouses_id_fk
        foreign key (warehouse_id) references deliverit.warehouse (id)
);

create or replace table parcels_in_shipments
(
	id int auto_increment
		primary key,
	shipment_id int not null,
	parcel_id int not null,
	constraint parcels_in_shipments_parcel_id_fk
		foreign key (parcel_id) references parcel (id),
	constraint parcels_in_shipments_shipments_id_fk
		foreign key (shipment_id) references shipment (id)
);


