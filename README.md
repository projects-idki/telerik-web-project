# Telerik Web Project

DeliverIT - a web application

Swagger - https://app.swaggerhub.com/apis/kindzov/DeliverIT_1.3/1.0

Trello board - https://trello.com/b/Ld0UrcdR/telerik-web-project

**DeliverIT**

Freight Forwarding Management System

## Contents

- Team Project Assignment
-
    1. Project Description
-
    2. Functional Requirements

    - 2.1. General
    - 2.2. Public Part
    - 2.3. Private part
    - 2.4. Administrative part
-
    3. REST API
-
    4. Technical Requirements
-
    5. Database
-
    6. Backend
-
    7. Optional Requirements
-
    8. Deliverables
-
    9. Project Defense
-
    10. Expectations
-
    11. Appendix

## Team Project Assignment

## 1. Project Description

```
Your task is to develop DeliverIT - a web application that
serves the needs of a freight forwarding company.
```

```
DeliverIT's customers can place orders on international
shopping sites (like Amazon.de or eBay.com) and have their
parcels delivered to the company's warehouses.
```

```
DeliverIT has two types of users - customers and
employees. The customers can see how many parcels they
have on the way. The employees have a bit more capabilities.
They can add new parcels to the system, modify existing
ones, check how many parcels has a given warehouse
received and more.
```

## 2. Functional Requirements

### 2.1. General

- Cities

Each city has a name and a country.

- Countries

Each country has a name.

- Warehouses

A warehouse has an address.

```
Warehouses must be created/modified by employees
only.
```

- Parcels

```
Each parcel must have a customer who purchased it,
a warehouse to which it would need to be delivered,
weight and a category (the categories are predefined
and can be Electronics , Clothing , Medical , etc.).
```

```
A parcel is created by an employee. Besides creating,
employees must be able to modify parcels as well.
```

```
Customers must be able to see their past parcels as
well as the parcels they have on the way.
```

- Shipments

```
A shipment must have a departure and arrival date, a
status (the status is one of preparing , on the way ,
completed ), and a collection of all the parcels that will
be delivered with this shipment. A shipment without
any parcels cannot depart.
```

```
Employees must be able to add/remove parcels from
a shipment, as well as see how many shipments are on
the way.
```

```
Employees should be able to see which the next
shipment is to arrive.
```

```
Customers should have to ability to see the status of
the shipment that holds a given parcel of theirs.
```

### 2.2. Public Part

The public part must be accessible without authentication.

- Anonymous Users

```
Users who are not authenticated must be able to see
how many customers DeliverIT has and what and where
are the available warehouse locations.
```

```
Also, anonymous users must have the ability to
register.
```

### 2.3. Private part

Accessible only if the user is authenticated.

- Customers

```
Each customer has a first and last name, email, and
address for delivery. An address consists of a country, a
city, and a street name.
```

### 2.4. Administrative part

Available to employees only.

- Employees

```
The employees do not have an address, but they still
have a first and last name and an email.
```

## 3. REST API

```
To provide other developers with your service, you need to
develop a REST API. It should leverage HTTP as a transport
protocol and clear text JSON for the request and response
payloads.
```

```
A great API is nothing without a great documentation. The
documentation holds the information that is required to
```

successfully consume and integrate with an API. You must

Use Swagger to document yours.

The REST API provides the following capabilities:

1. Cities
    - Read operations ( _must_ )
2. Countries
    - Read operations ( _must_ )
3. Warehouses
    - Read operations ( _must_ )
    - Create, Update, Delete operations ( _should_ )
4. Shipments
    - CRUD operations ( _must_ )
    - Filter by warehouse ( _must_ )
    - Filter by customer ( _should_ )
5. Parcels
    - CRUD operations ( _must_ )
    - Filter by weight ( _must_ )
    - Filter by customer ( _must_ )
    - Filter by warehouse ( _must_ )
    - Filter by category ( _must_ )
    - Filter by multiple criteria (e.g., customer _and_ category)
      ( _should_ )
    - Sort by weight _or_ arrival date ( _should_ )
    - Sort by weight _and_ arrival date ( _could_ )
6. Customers
    - CRUD Operations ( _must_ )
    - Search by email (full or part of an email) ( _must_ )
    - Search by first/last name ( _must_ )
    - Query customer’s incoming parcels ( _should_ )
    - Search by multiple criteria ( _should_ )
    - Search all fields from one word (e.g., “john” will search in the email, first and last name fields) ( _could_ )

## 4. Technical Requirements

```
General development guidelines include, but are not
limited to:
```

- Use IntelliJ IDEA
- Following OOP principles when coding
- Following KISS, SOLID, DRY principles when coding
- Following REST API design best practices when designing the REST API (see Appendix)
- Following BDD when writing tests
- You should implement sensible Exception handling and propagation
- Try to think ahead. When developing something, think – “How hard would it be to change/modify this later?”

## 5. Database

```
The data of the application must be stored in a relational
database – MariaDB. You need to identify the core domain
objects and model their relationships accordingly. Your
repository must include two scripts – one to create the
database and one to fill it with data.
```

## 6. Backend

- JDK version 11
- Use tiered project structure (separate the application in layers)
- Use the Spring Boot framework
- Use Hibernate in the persistence (repository) layer
- The service layer (i.e., "business" functionality) _must_ have at least 80% unit test coverage

## 7. Optional Requirements

- Integrate your project with a Continuous Integration server (e.g., GitLab’s own) and configure your unit tests to run
  on each commit to your master branch


- Host your application's backend in a public hosting provider of your choice (e.g., AWS, Azure, Heroku)
- Use branches while working with Git Note: These really are **_optional_**.

## 8. Deliverables

```
Commits in the GitLab repository should give a good
overview of how the project was developed, which features
were created first and the people who contributed.
Contributions from all team members must be evident
through the git commit history! The repository must contain
the complete application source code and any scripts
(database scripts, for example).
```

```
Provide a link to a GitLab repository with the following
information in the README.md file:
```

- Link to the Trello board ( _must_ )
- Link to the Swagger documentation ( _must_ )
- Link to the hosted project (if hosted online)
- Documentation describing how to build and run the project
  (see Spring Initializr’s own) ( _must_ )
- Documentation how to create and fill the database with data
  ( _must_ )
- Images of the database relations ( _must_ )

## 9. Project Defense

```
Each team will have a defense of their project with the
trainers where they must explain the application structure,
major architectural components and selected source code
pieces demonstrating the implementation of key features.
```

```
Use a Postman collection to gather your requests for
convenience.
```

## 10. Expectations

You must understand the system you have created.

```
It is OK if your application has flaws or is missing a couple
of must's. What's not OK is if you do not know what is
working and what is not. That said, you should be aware of
any defects or incomplete functionality must be properly
documented and secured.
```

```
Some things you need to be able to explain during your
project defense:
```

- What are the most important things you have learned while working on this project?
- What are the worst "hacks" in the project, or where do you think it needs improvement?
- What more would you do if you had another week to work on the system?
- What would you do differently if you were implementing the system again?

## 11. Appendix

- Guidelines for designing good REST API
- Guidelines for URL encoding
- Always prefer constructor injection
- Git commits - an effective style guide
- How to Write a Git Commit Message


