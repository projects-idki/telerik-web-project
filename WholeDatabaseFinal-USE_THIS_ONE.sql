-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for deliverit
CREATE DATABASE IF NOT EXISTS `deliverit` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `deliverit`;

-- Dumping structure for table deliverit.address
CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street_address` text NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_cityes__fk` (`city_id`),
  CONSTRAINT `addresses_cityes__fk` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.address: ~32 rows (approximately)
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` (`id`, `street_address`, `city_id`) VALUES
	(1, 'ul. "Velbazhd" 26-28, 1517 kv. Vasil Levski', 1),
	(2, 'ul. "Zornitsa" 5-1, 9009 Sveti Ivan Rilski', 2),
	(3, 'ul. "Tetz-iztok" 7000, 7009 Eastern Industrial Zone', 3),
	(4, 'ul. "London Borough of Merton', 4),
	(5, 'ul. "Berner Str. 3-8, 12205', 5),
	(6, 'Promotion 7 Boulogne, 35 Boulevard Jean Jaurès, 92100 Boulogne-Billancourt', 6),
	(7, 'ul. "Pozitano" 78, 1309 g.k. Razsadnika - Konyovitsa', 1),
	(8, 'ul. "Sveta Troitsa, 1309', 1),
	(9, 'ul. "ul. "Ekzarh Yosif" 60-68, 1000 Sofia Center', 1),
	(10, 'ul. "ul. "Pierce O\'mahony" 53, 1612 g.k. Lagera', 1),
	(11, 'ul. "309-ta" 331, 1336 zh.k. Lyulin 3', 1),
	(12, 'ul. "Polkovnik Vladimir Serafimov", 1712 g.k. Mladost 3', 1),
	(13, 'ul. "Perushtitsa" 8A, 8001 g.k. Lazur', 2),
	(14, 'ul. "zh.k. Slaveykov 397-395, 8010 g.k. Petko R. Slaveykov', 2),
	(15, 'ul. "ul. "Tsanko Dyustabanov" 24-26, 9009 Northern Industrial Zone', 2),
	(16, 'ul. "ul. "Dubrovnik" 18, 9010 Levski', 2),
	(17, 'ul. "ul. "Antim I" 59-51, 7001 Vezhdata', 3),
	(18, 'ul. "Voyvodova" 36-54, 7002 Ruse Center', 3),
	(19, 'Onslow Square, 19F', 7),
	(20, 'Chelsea Road 193', 7),
	(21, 'Manhatton Boulevard, E 77th ST', 8),
	(22, '940 Madison Ave, NY 10021', 8),
	(23, '25-10 30th Ave, Queens, NY 11102', 8),
	(24, '22-06 31st St, Queens, NY 11105', 8),
	(25, '7313 SW 107th Ave, FL 33173', 9),
	(26, '801 Brickell Bay Dr, FL 33131', 9),
	(27, '1805 N Ridgeway Ave, IL 60647', 10),
	(28, '7435 W Talcott Ave, IL 60631', 10),
	(29, '450 March Rd, Kanata, ON K2K 3K2', 11),
	(30, '105 Schneider Rd Unit 130, Kanata, ON K2K 1Y3', 11),
	(31, '1455 Quebec St, BC V6A 3Z7', 12),
	(32, '3080 Prince Edward St, BC V5T 3N4', 12);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;

-- Dumping structure for table deliverit.city
CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cities_countries__fk` (`country_id`),
  CONSTRAINT `cities_countries__fk` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.city: ~12 rows (approximately)
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` (`id`, `name`, `country_id`) VALUES
	(1, 'Sofia', 1),
	(2, 'Varna', 1),
	(3, 'Ruse', 1),
	(4, 'London', 2),
	(5, 'Berlin', 3),
	(6, 'Paris', 4),
	(7, 'Sydney', 5),
	(8, 'New York', 6),
	(9, 'Miami', 6),
	(10, 'Chicago', 6),
	(11, 'Ottawa', 7),
	(12, 'Vancouver', 7);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;

-- Dumping structure for table deliverit.country
CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `countries_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.country: ~7 rows (approximately)
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` (`id`, `name`) VALUES
	(5, 'Australia'),
	(1, 'Bulgaria'),
	(7, 'Canada'),
	(4, 'France'),
	(3, 'Germany'),
	(2, 'United Kingdom'),
	(6, 'USA');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;

-- Dumping structure for table deliverit.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_email_uindex` (`user_id`),
  UNIQUE KEY `customer_email_uindex_2` (`email`),
  KEY `customers_addresses_id_fk` (`address_id`),
  CONSTRAINT `customer_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `customers_addresses_id_fk` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.customer: ~16 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `first_name`, `last_name`, `user_id`, `address_id`, `email`) VALUES
	(1, 'Matey', 'Todorov', 1, 7, 'matey.todorov@randommail.com'),
	(2, 'Tsveta', 'Petrovaa', 2, 8, 'TsvetaStefanovaPetrovaa@calendartalk.co.uk'),
	(3, 'Vladislav', 'Stoev', 3, 9, 'VladislavIvailovStoev@tennisprofile.co.uk'),
	(4, 'Gavril', 'Vasev', 4, 10, 'GavrilAndreevVasev@builderweekly.co.uk'),
	(5, 'Plamen', 'Bachev', 5, 11, 'PlamenStanislavovBachev@quickdiscussion.co.uk'),
	(6, 'Mira', 'Dimitrova', 6, 12, 'MiraRadomirovaDimitrova@getpopcorn.es'),
	(7, 'Vanya', 'Nikolova', 7, 13, 'VanyaVasilovaNikolova@websitecd.co.nz'),
	(8, 'Zlatko', 'Dobrev', 8, 14, 'ZlatkoMitkovDobrev@devmanagers.co.uk'),
	(9, 'Boyana', 'Adamova', 9, 15, 'BoyanaLyobenovaAdamova@designingadvice.ca'),
	(10, 'Thad', 'Castle', 10, 22, 'T54Castle@bms.ny'),
	(11, 'Tom', 'Brady', 11, 25, 'TomBrady@gmail.com'),
	(12, 'Cindy', 'Tomkins', 12, 20, 'cindy5@gmail.com'),
	(13, 'Thahit', 'Chong', 13, 28, 'Chong@motors.ch'),
	(14, 'Jordan', 'Hemmings', 14, 30, 'JHemmings@builderweekly.com'),
	(15, 'Lilly', 'Rose', 15, 31, 'Rose_Lilly@hotmail.com'),
	(16, 'Grozdana', 'Pramatarova', 16, 16, 'GrozdanaYankovaPramatarova@stlouislighting.se');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table deliverit.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_email_uindex` (`email`),
  UNIQUE KEY `employee_userInfo_uindex` (`user_id`),
  CONSTRAINT `employee_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.employee: ~11 rows (approximately)
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`id`, `first_name`, `last_name`, `user_id`, `email`) VALUES
	(1, 'Nadeja', 'Taneva', 17, 'NadejaGavrilovaTaneva@phonespa.at'),
	(2, 'Kostadin', 'Zhelev', 18, 'KostadinRadomirovZhelev@housingrate.hu'),
	(3, 'Stefan', 'Vasilev', 19, 'StefanZhivkovVasilev@symphonymagazine.co.nz'),
	(4, 'Stanimira', 'Antonova', 20, 'StanimiraStanimirovaAntonova@packagenews.fr'),
	(5, 'Ivo', 'Ilev', 21, 'IvoLyobomirovIlev@crewinformation.cz'),
	(6, 'Kalina', 'Blagoeva', 22, 'KalinaKrasimirovaBlagoeva@orthofresh.com.br'),
	(7, 'Bogomil', 'Svetkov', 23, 'BogomilLubomirovSvetkov@flexphones.co.nz'),
	(8, 'Kyle', 'Edmond', 24, 'Kyle_Edmond@deliverITsupport.co.uk'),
	(9, 'Lorena', 'Rae', 25, 'Lorena_Rae@deliverITsupport.co.uk'),
	(10, 'Adrian', 'Becker', 26, 'Adrian_Becker@deliverITsupport.co.uk'),
	(11, 'Milan', 'Kostov', 27, 'MilanStanislavovKostov@movingfee.it');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;

-- Dumping structure for table deliverit.parcel
CREATE TABLE IF NOT EXISTS `parcel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `weight` double NOT NULL,
  `category_id` int(11) NOT NULL,
  `shipment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parcel_customers_id_fk` (`customer_id`),
  KEY `parcel_parcel_categories_id_fk` (`category_id`),
  KEY `parcel_shipment_id_fk` (`shipment_id`),
  KEY `parcel_warehouses_id_fk` (`warehouse_id`),
  CONSTRAINT `parcel_customers_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  CONSTRAINT `parcel_parcel_categories_id_fk` FOREIGN KEY (`category_id`) REFERENCES `parcel_category` (`id`),
  CONSTRAINT `parcel_shipment_id_fk` FOREIGN KEY (`shipment_id`) REFERENCES `shipment` (`id`),
  CONSTRAINT `parcel_warehouses_id_fk` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.parcel: ~100 rows (approximately)
/*!40000 ALTER TABLE `parcel` DISABLE KEYS */;
INSERT INTO `parcel` (`id`, `customer_id`, `warehouse_id`, `weight`, `category_id`, `shipment_id`) VALUES
	(1, 15, 8, 2.22, 8, NULL),
	(2, 9, 2, 0.23, 12, 1),
	(3, 2, 2, 0.76, 25, 1),
	(4, 4, 9, 2.44, 17, NULL),
	(5, 1, 8, 0.09, 11, NULL),
	(6, 13, 3, 1.63, 1, 3),
	(7, 3, 9, 0.59, 5, NULL),
	(8, 12, 4, 2.94, 16, NULL),
	(9, 16, 9, 0.9, 21, NULL),
	(10, 7, 4, 4.51, 13, NULL),
	(11, 11, 2, 0.97, 8, 1),
	(12, 7, 2, 1.97, 11, 1),
	(13, 16, 2, 0.28, 21, NULL),
	(14, 6, 6, 2.16, 21, NULL),
	(15, 10, 1, 1.05, 8, NULL),
	(16, 3, 1, 2.7, 16, NULL),
	(17, 5, 4, 3.95, 15, NULL),
	(18, 9, 3, 1.28, 19, 3),
	(19, 15, 5, 2.14, 13, NULL),
	(20, 15, 8, 1.03, 30, NULL),
	(21, 4, 2, 1.82, 14, 1),
	(22, 7, 6, 0.34, 20, NULL),
	(23, 7, 5, 0.02, 11, NULL),
	(24, 10, 1, 0.5, 8, 2),
	(25, 5, 5, 1.53, 22, NULL),
	(26, 3, 1, 3.65, 29, 2),
	(27, 12, 5, 1.8, 19, NULL),
	(28, 3, 3, 3.78, 13, NULL),
	(29, 13, 9, 1.72, 8, NULL),
	(30, 7, 4, 0.69, 5, NULL),
	(31, 10, 2, 2.34, 22, 1),
	(32, 13, 9, 5.08, 28, NULL),
	(33, 7, 1, 0.97, 22, 2),
	(34, 10, 1, 1.8, 25, 2),
	(35, 13, 4, 0.39, 21, NULL),
	(36, 2, 3, 0.19, 29, 3),
	(37, 5, 4, 0.82, 13, NULL),
	(38, 10, 1, 1.2, 23, 2),
	(39, 5, 7, 1.06, 3, NULL),
	(40, 8, 9, 1.27, 14, NULL),
	(41, 10, 1, 4.65, 12, 2),
	(42, 3, 6, 0.27, 6, NULL),
	(43, 5, 9, 0.81, 15, NULL),
	(44, 14, 3, 2.28, 1, 3),
	(45, 3, 9, 0.61, 15, NULL),
	(46, 12, 6, 2.56, 25, NULL),
	(47, 4, 7, 1.27, 20, NULL),
	(48, 9, 2, 0.2, 16, 1),
	(49, 7, 7, 2.69, 5, NULL),
	(50, 2, 1, 0.18, 20, 2),
	(51, 11, 8, 0.79, 23, NULL),
	(52, 12, 3, 0.07, 4, 3),
	(53, 1, 1, 0.31, 19, 2),
	(54, 6, 4, 1.38, 21, NULL),
	(55, 7, 2, 5.46, 20, NULL),
	(56, 10, 1, 1.06, 16, 2),
	(57, 4, 5, 0.15, 26, NULL),
	(58, 11, 6, 1.34, 5, NULL),
	(59, 15, 7, 0.21, 24, NULL),
	(60, 4, 4, 1.81, 17, NULL),
	(61, 16, 5, 1.25, 20, NULL),
	(62, 1, 8, 1.32, 9, NULL),
	(63, 7, 7, 1.43, 8, NULL),
	(64, 14, 3, 0.31, 17, 3),
	(65, 6, 8, 1.07, 15, NULL),
	(66, 9, 6, 2.06, 11, NULL),
	(67, 2, 7, 0.59, 23, NULL),
	(68, 11, 1, 1.56, 14, NULL),
	(69, 10, 8, 2.35, 4, NULL),
	(70, 2, 4, 1.64, 25, NULL),
	(71, 8, 2, 0.49, 10, 1),
	(72, 3, 8, 3.12, 22, NULL),
	(73, 6, 1, 1.06, 13, 2),
	(74, 8, 1, 1.05, 12, NULL),
	(75, 16, 3, 2.35, 15, NULL),
	(76, 12, 5, 0.13, 7, NULL),
	(77, 9, 8, 0.27, 15, NULL),
	(78, 10, 3, 1.4, 22, 3),
	(79, 4, 6, 2.97, 25, NULL),
	(80, 4, 5, 1.07, 4, NULL),
	(81, 3, 3, 1.38, 21, 3),
	(82, 4, 6, 0.31, 5, NULL),
	(83, 6, 4, 1.83, 3, NULL),
	(84, 5, 4, 2.2, 17, NULL),
	(85, 8, 7, 0.07, 12, NULL),
	(86, 13, 5, 3.3, 1, NULL),
	(87, 15, 6, 0.9, 17, NULL),
	(88, 4, 9, 1.95, 6, NULL),
	(89, 4, 5, 0.47, 5, NULL),
	(90, 10, 2, 1.93, 14, 1),
	(91, 15, 1, 2.31, 13, 2),
	(92, 4, 1, 1.76, 25, 2),
	(93, 5, 2, 1.58, 22, NULL),
	(94, 7, 7, 1.94, 18, NULL),
	(95, 8, 8, 1.85, 15, NULL),
	(96, 3, 9, 0.87, 5, NULL),
	(97, 4, 6, 0.73, 21, NULL),
	(98, 11, 3, 2.02, 9, 3),
	(99, 2, 4, 0.99, 5, NULL),
	(100, 15, 7, 1.02, 14, NULL);
/*!40000 ALTER TABLE `parcel` ENABLE KEYS */;

-- Dumping structure for table deliverit.parcels_in_shipments
CREATE TABLE IF NOT EXISTS `parcels_in_shipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipment_id` int(11) NOT NULL,
  `parcel_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parcels_in_shipments_parcel_id_fk` (`parcel_id`),
  KEY `parcels_in_shipments_shipments_id_fk` (`shipment_id`),
  CONSTRAINT `parcels_in_shipments_parcel_id_fk` FOREIGN KEY (`parcel_id`) REFERENCES `parcel` (`id`),
  CONSTRAINT `parcels_in_shipments_shipments_id_fk` FOREIGN KEY (`shipment_id`) REFERENCES `shipment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.parcels_in_shipments: ~30 rows (approximately)
/*!40000 ALTER TABLE `parcels_in_shipments` DISABLE KEYS */;
INSERT INTO `parcels_in_shipments` (`id`, `shipment_id`, `parcel_id`) VALUES
	(1, 1, 2),
	(2, 1, 3),
	(3, 1, 21),
	(4, 1, 71),
	(5, 1, 90),
	(6, 1, 11),
	(7, 1, 12),
	(8, 1, 31),
	(9, 1, 48),
	(10, 2, 33),
	(11, 2, 34),
	(12, 2, 38),
	(13, 2, 24),
	(14, 2, 73),
	(15, 2, 41),
	(16, 2, 26),
	(17, 2, 91),
	(18, 2, 92),
	(19, 3, 81),
	(20, 3, 98),
	(21, 3, 18),
	(22, 3, 52),
	(23, 3, 36),
	(24, 3, 6),
	(25, 3, 44),
	(26, 3, 78),
	(27, 3, 64),
	(28, 2, 50),
	(29, 2, 53),
	(30, 2, 56);
/*!40000 ALTER TABLE `parcels_in_shipments` ENABLE KEYS */;

-- Dumping structure for table deliverit.parcel_category
CREATE TABLE IF NOT EXISTS `parcel_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.parcel_category: ~30 rows (approximately)
/*!40000 ALTER TABLE `parcel_category` DISABLE KEYS */;
INSERT INTO `parcel_category` (`id`, `category`) VALUES
	(1, 'Appliances'),
	(2, 'Apps & Games'),
	(3, 'Arts, Crafts, & Sewing'),
	(4, 'Automotive Parts & Accessories'),
	(5, 'Baby'),
	(6, 'Beauty & Personal Care'),
	(7, 'Books'),
	(8, 'CDs & Vinyl'),
	(9, 'Cell Phones & Accessories'),
	(10, 'Clothing, Shoes and Jewelry'),
	(11, 'Collectibles & Fine Art'),
	(12, 'Computers'),
	(13, 'Electronics'),
	(14, 'Garden & Outdoor'),
	(15, 'Grocery & Gourmet Food'),
	(16, 'Handmade'),
	(17, 'Health, Household & Baby Care'),
	(18, 'Humans'),
	(19, 'Home & Kitchen'),
	(20, 'Industrial & Scientific'),
	(21, 'Kindle'),
	(22, 'Luggage & Travel Gear'),
	(23, 'Movies & TV'),
	(24, 'Musical Instruments'),
	(25, 'Office Products'),
	(26, 'Pet Supplies'),
	(27, 'Sports & Outdoors'),
	(28, 'Tools & Home Improvement'),
	(29, 'Toys & Games'),
	(30, 'Video Games');
/*!40000 ALTER TABLE `parcel_category` ENABLE KEYS */;

-- Dumping structure for table deliverit.shipment
CREATE TABLE IF NOT EXISTS `shipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departure_date` date DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `warehouse_to_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipment_warehouse_to_fk` (`warehouse_to_id`),
  CONSTRAINT `shipment_warehouse_to_fk` FOREIGN KEY (`warehouse_to_id`) REFERENCES `warehouse` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.shipment: ~3 rows (approximately)
/*!40000 ALTER TABLE `shipment` DISABLE KEYS */;
INSERT INTO `shipment` (`id`, `departure_date`, `arrival_date`, `warehouse_to_id`) VALUES
	(1, '2021-02-28', '2021-03-02', 2),
	(2, '2021-02-10', '2021-03-12', 1),
	(3, '2021-03-11', '2021-03-26', 3);
/*!40000 ALTER TABLE `shipment` ENABLE KEYS */;

-- Dumping structure for table deliverit.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_uindex` (`username`),
  KEY `user_role_id_fk` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.user: ~28 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `role`) VALUES
	(1, 'matey', '1234', 0),
	(2, 'tsveta', '1234', 0),
	(3, 'vladislav', '1234', 0),
	(4, 'garril', '1234', 0),
	(5, 'plamen', '1234', 0),
	(6, 'mira', '1234', 0),
	(7, 'vanya', '1234', 0),
	(8, 'zlatko', '1234', 0),
	(9, 'Boyana', '1234', 0),
	(10, 'Thad', '1234', 0),
	(11, 'Tom', '1234', 0),
	(12, 'Cindy', '1234', 0),
	(13, 'thahit', '1234', 0),
	(14, 'jordan', '1234', 0),
	(15, 'lilly', '1234', 0),
	(16, 'nadeja', '1234', 0),
	(17, 'kostadin', '1234', 1),
	(18, 'stefan', '1234', 1),
	(19, 'stanimira', '1234', 1),
	(20, 'ivo', '1234', 1),
	(21, 'kalina', '1234', 1),
	(22, 'bogomil', '1234', 1),
	(23, 'kyle', '1234', 1),
	(24, 'lorena', '1234', 1),
	(25, 'adrian', '1234', 1),
	(26, 'milan', '1234', 1),
	(27, 'unknown', '1234', 1),
	(28, 'admin', 'admin', 2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table deliverit.warehouse
CREATE TABLE IF NOT EXISTS `warehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `warehouses_addresses_id_fk` (`address_id`),
  CONSTRAINT `warehouses_addresses_id_fk` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table deliverit.warehouse: ~9 rows (approximately)
/*!40000 ALTER TABLE `warehouse` DISABLE KEYS */;
INSERT INTO `warehouse` (`id`, `address_id`) VALUES
	(1, 1),
	(2, 2),
	(3, 3),
	(4, 4),
	(5, 5),
	(6, 6),
	(7, 10),
	(8, 15),
	(9, 30);
/*!40000 ALTER TABLE `warehouse` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
